﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeneticReaction;
using System.IO;

namespace SeedCreator
{
    class Program
    {
        static void Main(string[] args)
        {
            byte[] data = File.ReadAllBytes(@"D:\projects\ReactiveDiffusion\results\old.gen");
            var vals = Enumerable.Range(0, 14)
                .Select(x => BitConverter.ToDouble(data, x * 8))
                .ToArray();
            var g = Converter.Convert(vals);
            g.Save("result\\convert.gen");

            //var creator = new GeneticReaction.SeedCreator();
            //var result = new List<Gene>();
            //int count = 0;
            //while (true)
            //{
            //    Console.WriteLine($"Step{count} start");
            //    var res = creator.Run(30, 100, .3);
            //    foreach (var item in res)
            //    {
            //        result.Add(item);
            //        item.Save($"result_new\\{result.Count}.gen");
            //        Console.WriteLine(result.Count);
            //    }
            //    count++;
            //}

        }
    }
}
