﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Cell1D
{
    public class Line
    {
        public Vector2 From, To;
        public int ID;
        public int Prev, Next;

        public static Vector2 GetVector(float length, float angle)
        {
            return new Vector2((float)Math.Cos(angle) * length, (float)Math.Sin(angle) * length);
        }

        public float Length => (From - To).Length();
        public Vector2 Center => (From + To) * .5f;
        public Vector2 Dir => To - From;

        public virtual VertexPositionColor[] ToVertex()
        {
            return new VertexPositionColor[]
            {
                new VertexPositionColor(new Vector3(From, 0), Color.White),
                new VertexPositionColor(new Vector3(To, 0), Color.White)
            };
        }
    }

    public class ManagerBase<T> : DrawableGameComponent where T : Line, new()
    {
        protected List<T> lines;
        SpriteBatch sprite;
        Texture2D texture;
        BasicEffect effect;

        protected readonly float Dt = (1f / 8f);

        public ManagerBase(Game game) : base(game)
        {
            lines = new List<T>();
            InitLines();
        }

        VertexBuffer vbuffer;

        protected readonly int MaxLines = 4096;

        protected bool isDirty = true;

        protected override void LoadContent()
        {
            base.LoadContent();
            sprite = new SpriteBatch(GraphicsDevice);
            texture = new Texture2D(GraphicsDevice, 1, 1);
            texture.SetData(new[] { Color.White });
            effect = new BasicEffect(GraphicsDevice);
            effect.World = Matrix.Identity;
            effect.View = Matrix.Identity;
            effect.Projection = Matrix.Identity;
            effect.VertexColorEnabled = true;
            vbuffer = new VertexBuffer(GraphicsDevice, typeof(VertexPositionColor), MaxLines * 2, BufferUsage.WriteOnly);
        }

        protected void SetData()
        {
            vbuffer.SetData(lines.SelectMany(x => x.ToVertex()).ToArray());
        }

        void InitLines()
        {
            int split = 6;
            float radius = .6f;
            var unit = MathHelper.TwoPi / split;
            for (int i = 0; i < split; i++)
            {
                var line = new T()
                {
                    ID = i,
                    From = Line.GetVector(radius, unit * i),
                    To = Line.GetVector(radius, unit * (i + 1)),
                    Prev = (i - 1 + split) % split,
                    Next = (i + 1) % split,
                };
                lines.Add(line);
            }
        }

        protected virtual int MakeChild(int id)
        {
            var line = lines[id];
            var child = new T()
            {
                ID = lines.Count
            };
            var dir = line.Dir;
            dir.Normalize();
            dir = new Vector2(dir.Y, -dir.X);
            var next = line.Center + dir * line.Length * (float)Math.Sqrt(3) * .5f * .5f;
            child.From = line.To;
            child.To = next;
            line.To = next;

            child.Prev = line.ID;
            child.Next = line.Next;
            lines[line.Next].Prev = child.ID;
            line.Next = child.ID;

            lines.Add(child);
            return child.ID;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            
        }


        public override void Draw(GameTime gameTime)
        {
            if (isDirty)
            {
                SetData();
                isDirty = false;
            }
            base.Draw(gameTime);
            GraphicsDevice.RasterizerState = RasterizerState.CullNone;
            GraphicsDevice.DepthStencilState = DepthStencilState.None;
            effect.CurrentTechnique.Passes[0].Apply();
            GraphicsDevice.SetVertexBuffer(vbuffer);
            GraphicsDevice.DrawPrimitives(PrimitiveType.LineList, 0, lines.Count);
        }

    }
}
