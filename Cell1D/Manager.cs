﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Cell1D
{
    public class ActivatorLine : Line
    {
        public double[] Params = new double[2];

        public double Activator => Params[0];
        public double Inhibitor => Params[1];


        public override VertexPositionColor[] ToVertex()
        {
            var v = base.ToVertex();
            for (int i = 0; i < v.Length; i++)
            {
                v[i].Color = new Color((float)Activator, 0, (float)Inhibitor, 1);
            }
            return v;
        }
    }

    public class Manager : ManagerBase<ActivatorLine>
    {
        readonly double[] Diffusion = new[]
        {
            1d / 128d,
            1d / 8d,
        };

        readonly double ChildThreshold = 3d;

        public Manager(Game game)
            : base(game)
        {
            var rand = new Random();
            var range = .1;
            for (int i = 0; i < lines.Count; i++)
            {
                for (int k = 0; k < 2; k++)
                {
                    lines[i].Params[k] = rand.NextDouble() * range;
                }
            }
            lines[0].Params[0] = 3 * 4;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            UpdateDiffusion(0);
            UpdateDiffusion(1);
            UpdateReaction();
            UpdateGeneation();
            isDirty = true;
        }

        void UpdateDiffusion(int k)
        {
            var buf = new double[lines.Count];
            for (int i = 0; i < buf.Length; i++)
            {
                var target = lines[i];
                buf[i] = (-Diffusion[k] * 2) * target.Params[k] + Diffusion[k] * (lines[target.Prev].Params[k] + lines[target.Next].Params[k]);
            }
            for (int i = 0; i < buf.Length; i++)
            {
                lines[i].Params[k] += buf[i] * Dt;
            }
        }

        void UpdateReaction()
        {
            var buf = new double[2][];
            buf[0] = new double[lines.Count];
            buf[1] = new double[lines.Count];
            for (int k = 0; k < 2; k++)
            {
                for (int i = 0; i < buf[k].Length; i++)
                {
                    buf[k][i] = lines[i].Params[k] + Dt * Reaction(k, lines[i].Params);
                }
            }

            for (int k = 0; k < 2; k++)
            {
                for (int i = 0; i < buf[k].Length; i++)
                {
                    lines[i].Params[k] = buf[k][i];
                }
            }
        }

        void UpdateGeneation()
        {
            if(MaxLines <= lines.Count)
            {
                return;
            }
            var len = lines.Count;
            for (int i = 0; i < len; i++)
            {
                var target = lines[i];
                if(target.Params[0] > ChildThreshold)
                {
                    MakeChild(i);
                    if(MaxLines <= lines.Count)
                    {
                        break;
                    }
                }
            }
        }

        protected override int MakeChild(int id)
        {
            var child = lines[ base.MakeChild(id)];
            var self = lines[id];
            for (int i = 0; i < self.Params.Length; i++)
            {
                self.Params[i] *= .5f;
                child.Params[i] = self.Params[i];
            }
            return child.ID;
        }

        protected virtual double Reaction(int i, double[] p)
        {
            if(i == 0)
            {
                return (.3 - .4 * p[1] + p[0] * p[0] / p[1]) * .1;
            }
            if(i == 1)
            {
                return (p[0] * p[0] - p[1]) * .1;
            }
            throw new ArgumentException("", nameof(i));
        }
    }
}
