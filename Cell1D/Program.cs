﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Cell1D
{
    class Program
    {
        public static void Main(string[] args)
        {
            using (var game = new MyGame())
            {
                game.Run();

            }
        }
    }

    public class MyGame : Game
    {
        GraphicsDeviceManager graphicsManager;

        public MyGame()
        {
            graphicsManager = new GraphicsDeviceManager(this)
            {
                PreferredBackBufferWidth = 1024,
                PreferredBackBufferHeight = 1024,
            };
            manager = new Manager(this);
            Components.Add(manager);
        }

        Manager manager;

        protected override void LoadContent()
        {
            base.LoadContent();
        }

        protected override void Update(GameTime gameTime)
        {
            var input = Keyboard.GetState();
            if (input.IsKeyDown(Keys.Escape))
            {
                Components.Remove(manager);
                manager.Dispose();
                manager = new Manager(this);
                Components.Add(manager);
            }
            base.Update(gameTime);
            
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            base.Draw(gameTime);
        }
    }
}
