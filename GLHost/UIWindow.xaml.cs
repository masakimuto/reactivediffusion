﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GLHost
{
    /// <summary>
    /// UIWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class UIWindow : Window
    {
        GeneManager manager;
        bool isInitialized;

        readonly string DefaultGeneText = "\n\n";
        TextBox[] geneTexts;
        int lastTextBox;

        public UIWindow()
        {
            InitializeComponent();
            geneTexts = new[] { geneText, geneText2 };
            foreach (var t in geneTexts)
            {
                t.Text = DefaultGeneText;
                t.Tag = -1;
            }
            lastTextBox = 0;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            glRenderer.Dispose();
        }


        void NextClick(object sender, EventArgs e)
        {
            Cursor = Cursors.Wait;
            manager.Evolve(glRenderer.GetSelection());
            Cursor = Cursors.Arrow;
            undoButton.IsEnabled = true;
            //status.Content = "generation:" + manager.Generation;
            //ApplySlider(manager.genes[s.FirstOrDefault()]);
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            if (!isInitialized && glRenderer.Renderer != null)
            {
                manager = new GeneManager(glRenderer.Renderer, glRenderer, s => status.Content = s);
                glRenderer.OnItemClick += GlRenderer_OnItemClick;
                glRenderer.OnFPSUpdate += propertyControl.FpsUpdate;
                isInitialized = true;
                propertyControl.ShowGeneProperty(manager.Genes[0]);
                propertyControl.Init(glRenderer);
            }
        }

        private void GlRenderer_OnItemClick(int index, bool select)
        {
            if (select)
            {
                var gt = manager.Genes[index].ToString();
                propertyControl.ShowGeneProperty(manager.Genes[index]);
                var t = geneTexts.Select((x, i)=> new { x, i }).FirstOrDefault(x => x.x.Tag.Equals(-1));
                if(t != null)
                {
                    t.x.Text = gt;
                    lastTextBox = t.i;
                    t.x.Tag = index;
                }else
                {
                    var box = geneTexts[1 - lastTextBox];
                    box.Text = gt;
                    box.Tag = index;
                    lastTextBox = 1 - lastTextBox;
                }
            }
            else
            {
                var t = geneTexts.FirstOrDefault(x => x.Tag.Equals(index));
                if(t != null)
                {
                    t.Text = DefaultGeneText;
                    t.Tag = -1;
                }
            }


            //if (select)
            //{
            //    geneText.Text = manager.Genes[index].ToString();
            //    propertyControl.ShowGeneProperty(manager.Genes[index]);
            //}else
            //{


            //    geneText.Text = "\n\n";
            //}
        }


        void SaveClick(object sender, EventArgs e)
        {
            var selection = glRenderer.GetSelection().Select(x => new Nullable<int>(x)).FirstOrDefault().GetValueOrDefault(-1);
            if(selection == -1)
            {
                status.Content = "no item selected to save";
                return;
            }
            glRenderer.SetActive(false);
            var dialog = new Microsoft.Win32.SaveFileDialog()
            {
                AddExtension = true,
                DefaultExt = "egene",
                Filter = "extent gene file|*.egene",
                InitialDirectory = Environment.CurrentDirectory
            };
            if(dialog.ShowDialog() == true)
            {
                manager.Genes[selection].Serialize(dialog.FileName);
                glRenderer.Renderer.Output(selection, System.IO.Path.ChangeExtension(dialog.FileName, "png"));
                status.Content = $"gene{selection} saved as {dialog.FileName}";
            }
            if(stepToggleButton.IsChecked == true)
            {
                glRenderer.SetActive(true);
            }
        }

        void LoadClick(object sender, EventArgs e)
        {
            var dialog = new Microsoft.Win32.OpenFileDialog()
            {
                AddExtension = true,
                DefaultExt = "egene",
                Filter = "extent gene file|*.egene",
                Multiselect = true,
                InitialDirectory = Environment.CurrentDirectory
            };
            if(dialog.ShowDialog() == true)
            {
                manager.FromFiles(dialog.FileNames, 0);
            }
        }

        void RenderClick(object sender, EventArgs e)
        {
            var selection = glRenderer.GetSelection().Select(x => new Nullable<int>(x)).FirstOrDefault().GetValueOrDefault(-1);
            if (selection == -1)
            {
                status.Content = "no item selected to save";
                return;
            }
            glRenderer.SetActive(false);
            var dialog = new Microsoft.Win32.SaveFileDialog()
            {
                AddExtension = true,
                DefaultExt = "png",
                Filter = "image file|*.png",
                InitialDirectory = Environment.CurrentDirectory
            };
            if (dialog.ShowDialog() == true)
            {
                glRenderer.Renderer.Output(selection, dialog.FileName);
                status.Content = $"gene{selection} rendered as {dialog.FileName}";
            }
            if (stepToggleButton.IsChecked == true)
            {
                glRenderer.SetActive(true);
            }
        }

        void UndoClick(object sender, EventArgs e)
        {
            manager.Undo();
            undoButton.IsEnabled = false;
        }

        //void InvertClick(object sender, EventArgs e)
        //{
        //    glRenderer.SetSelection(Enumerable
        //        .Range(0, glRenderer.UnitCount)
        //        .Except(glRenderer.GetSelection()));
        //}


        void UnselectClick(object sender, EventArgs e)
        {
            glRenderer.SetSelection(Enumerable.Empty<int>());
            foreach (var text in geneTexts)
            {
                text.Text = DefaultGeneText;
                text.Tag = -1;
            }
        }

        void ToggleClick(object sender, EventArgs e)
        {
            var check = stepToggleButton.IsChecked == true;
            glRenderer.SetActive(check);
            (sender as System.Windows.Controls.Primitives.ToggleButton).Content = check ? "更新中" : "停止中";
            status.Content = $"{glRenderer.ElapsedStep} step, {glRenderer.ElapsedTime} second";

        }

        private void RestartClick(object sender, RoutedEventArgs e)
        {
            glRenderer.StartCount();
            glRenderer.Renderer.Restart();
            glRenderer.UpdateDisplay();
        }

        private void OpenConfigClick(object sender, RoutedEventArgs e)
        {
            Config.Instance.CreateDefaultConfigFile();
            try
            {
                System.Diagnostics.Process.Start(Config.FileName);
                status.Content = "open config file";
            }
            catch
            {
                status.Content = "failed to open config file";
            }
        }
    }

}
