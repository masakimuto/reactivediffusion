﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using OpenTK.Graphics.OpenGL;
using GeneticReaction.Expressions;

namespace GLHost
{
    public class GeneticRenderer : RendererBase
    {
        readonly int Width;
        readonly int Height;

        public GeneticRenderer(int width, int height)
        {
            Width = width;
            Height = height;
            anisX = 0.5f;
            anisY = 0.5f;
            anisZ = 0f;
            iFreq = 1f;
            iDt = 0.5f;
            Gene = new ExtentGene(null);
        }

        ~GeneticRenderer()
        {
            if (!disposed)
            {
                Dispose();
            }
        }

        int program;
        bool disposed;
        int[] texture = new int[2];
        int[] fbo = new int[2];
        int currentTexture = 0;
        int seedVbo, seedProgram;
        int ParamCount;

        float anisX, anisY, anisZ;
        float iFreq, iDt;

        public ExtentGene Gene { get; private set; }

        public int GetTexture()
        {
            return texture[currentTexture];
        }

        public void LoadContent()
        {
            ParamCount = GeneticReaction.Config.ParamCount;
            fsCode = File.ReadAllText("Shaders\\stab.frag");
            vsCode = File.ReadAllText("Shaders\\test.vert");
            seedProgram = LoadShader(File.ReadAllText("Shaders\\test.vert"), File.ReadAllText("Shaders\\seed.frag"));

            seedVbo = CreateVBO(seedProgram);

            GL.GenTextures(2, texture);
            for (int i = 0; i < 2; i++)
            {
                GL.BindTexture(TextureTarget.Texture2D, texture[i]);
                GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba32f, Width, Height, 0, PixelFormat.Rgba, PixelType.Float, new IntPtr());
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.Repeat);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.Repeat);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest);
            }

            GL.GenFramebuffers(2, fbo);
            for (int i = 0; i < 2; i++)
            {
                GL.BindFramebuffer(FramebufferTarget.Framebuffer, fbo[i]);
                GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, TextureTarget.Texture2D, texture[i], 0);
                GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
            }

        }

        public void ApplySeed()
        {
            GL.UseProgram(seedProgram);
            GL.Viewport(0, 0, Width, Height);
            GL.EnableVertexAttribArray(attrPos);
            GL.BindBuffer(BufferTarget.ArrayBuffer, seedVbo);
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, fbo[0]);
            GL.DrawArrays(PrimitiveType.Triangles, 0, 6);
            GL.Flush();
            GL.DisableVertexAttribArray(attrPos);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
            currentTexture = 0;
        }

        public void ApplyFrequency(float freq)
        {
            Gene.Frequency = freq;
            iFreq = freq;
            GL.UseProgram(program);
            GL.Uniform1(GL.GetUniformLocation(program, "iFrequency"), iFreq);
            GL.UseProgram(0);
            //ApplySeed();
        }

        public void ApplyDt(float dt)
        {
            Gene.Dt = dt;
            iDt = dt;
            GL.UseProgram(program);
            GL.Uniform1(GL.GetUniformLocation(program, "iDt"), iDt);
            GL.UseProgram(0);
        }

        string fsCode, vsCode;


        public void SetGene(GLGene gene)
        {
            if(program != 0)
            {
                GL.DeleteProgram(program);
                GL.DeleteBuffer(vbo);
            }
            string lines = fsCode;
            for (int i = 0; i < ParamCount; i++)
            {
                lines = lines.Replace("STAB" + i.ToString(), gene.root[i].ToString());
            }
            for (int i = ParamCount; i < 3; i++)
            {
                lines = lines.Replace("STAB" + i.ToString(), "0.0");
            }
            program = LoadShader(vsCode, lines);

            vbo = CreateVBO(program);

            GL.UseProgram(program);
            GL.Uniform1(GL.GetUniformLocation(program, "iFrequency"), iFreq);
            float[] dif = Enumerable.Range(0, 3).Select(x => (float)gene.GetDiffusion(x)).ToArray();
            GL.Uniform3(GL.GetUniformLocation(program, "iDiffusion"), dif[0], dif[1], dif[2]);
            GL.Uniform2(GL.GetUniformLocation(program, "iResolution"), (float)Width, (float)Height);
            
            GL.Uniform3(GL.GetUniformLocation(program, "iAnisotoropy"), anisX, anisY, anisZ);//x-diffuse,y-diffuse, angle
            //GL.Uniform1(GL.GetUniformLocation(program, "iTexture"), 0);
            GL.Uniform1(GL.GetUniformLocation(program, "iDt"), iDt);
            GL.UseProgram(0);

            ApplySeed();
            Gene.Gene = gene;
        }

        public void SetGene(ExtentGene gene)
        {
            Gene = gene;
            ApplyAnis(Gene.Anisotoropy);
            ApplyAngle(gene.Angle);
            ApplyFrequency(gene.Frequency);
            ApplyDt(gene.Dt);
            SetGene(Gene.Gene);
        }

        internal void ApplyAnis(float val)
        {
            Gene.Anisotoropy = val;
            anisX = val;
            anisY = (1.0f - val);
            GL.UseProgram(program);
            GL.Uniform3(GL.GetUniformLocation(program, "iAnisotoropy"), anisX, anisY, anisZ);
            GL.UseProgram(0);
        }

        internal void ApplyAngle(float val)
        {
            Gene.Angle = val;
            anisZ = val / 180.0f * (float)Math.PI;
            GL.UseProgram(program);
            GL.Uniform3(GL.GetUniformLocation(program, "iAnisotoropy"), anisX, anisY, anisZ);
            GL.UseProgram(0);
        }

        int count = 0;

        public override void Draw()
        {
            count++;
            GL.Viewport(0, 0, Width, Height);
            currentTexture = 1 - currentTexture;
            GL.UseProgram(program);
            GL.EnableVertexAttribArray(attrPos);
            GL.BindBuffer(BufferTarget.ArrayBuffer, vbo);
            GL.BindTexture(TextureTarget.Texture2D, texture[1 - currentTexture]);
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, fbo[currentTexture]);
            GL.Clear(ClearBufferMask.ColorBufferBit);

            GL.DrawArrays(PrimitiveType.Triangles, 0, 6);

            GL.Flush();

            GL.DisableVertexAttribArray(attrPos);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
            GL.BindTexture(TextureTarget.Texture2D, 0);
        }

        public void DrawMultiple(int times)
        {
            GL.Viewport(0, 0, Width, Height);
            GL.UseProgram(program);
            GL.EnableVertexAttribArray(attrPos);
            GL.BindBuffer(BufferTarget.ArrayBuffer, vbo);

            for (int i = 0; i < times; i++)
            {
                count++;
                currentTexture = 1 - currentTexture;
                GL.BindTexture(TextureTarget.Texture2D, texture[1 - currentTexture]);
                GL.BindFramebuffer(FramebufferTarget.Framebuffer, fbo[currentTexture]);

                GL.DrawArrays(PrimitiveType.Triangles, 0, 6);

                GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
                GL.BindTexture(TextureTarget.Texture2D, 0);
            }

            GL.DisableVertexAttribArray(attrPos);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
        }

        public override void Dispose()
        {
            if (disposed)
            {
                return;
            }
            base.Dispose();
            try
            {
                GL.DeleteProgram(program);
                GL.DeleteFramebuffers(2, fbo);
                GL.DeleteTextures(2, texture);
                GL.DeleteProgram(seedProgram);
                GL.DeleteBuffer(seedVbo);
            }
            catch (Exception)
            {
                //System.Windows.MessageBox.Show(e.ToString());
                disposed = true;
            }
            disposed = true;
        }
    }
}
