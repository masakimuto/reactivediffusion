﻿using GeneticReaction.Expressions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GLHost
{
    public enum SampleExpressionType
    {
        Random = -1,
        RandomSample = 0,
        Turing,
        GiererMeinhrdt,
        Schnakenberg,
        Meinhardt2,
        Meinhardt3,
        //GinzburgLandau,
    }

    public class GLGene
    {
        public Expression[] root
        {
            get; private set;
        }
        ExpressionBuilder[] builder;
        GrayValue[] Constants
        {
            get; set;
        }

        public Random rand
        {
            get; private set;
        }

        static int DiffusionConstantLength = 2;

        readonly float MutateRange;
        readonly float MutateRate;

        public GLGene(Random rand, Expression[] root)
        {
            this.root = root;
            this.rand = rand;
            MutateRange = Config.Instance.MutateRange;
            MutateRate = Config.Instance.MutateRate;
            Build();
        }

        public static GLGene Adam(Random rand)
        {
            var g = new GLGene(rand, new[] { SampleExpressions.Turing(0), SampleExpressions.Turing(1) });
            g.RandomizeConstants();
            return g;
        }

        public static GLGene Adam(Random rand, SampleExpressionType type)
        {
            Func<int, Expression> f;
            switch (type)
            {
                case SampleExpressionType.Random:
                    f = i => Expression.CreateRandomTree(rand);
                    break;
                case SampleExpressionType.RandomSample:
                    var r = (SampleExpressionType) rand.Next(1, 6);
                    return Adam(rand, r);

                case SampleExpressionType.Turing:
                    f = SampleExpressions.Turing;
                    break;
                case SampleExpressionType.GiererMeinhrdt:
                    f = SampleExpressions.GiererMeinhrdt;
                    break;
                case SampleExpressionType.Schnakenberg:
                    f = SampleExpressions.Schnakenberg;
                    break;
                case SampleExpressionType.Meinhardt2:
                    f = SampleExpressions.Meinhardt2;
                    break;
                case SampleExpressionType.Meinhardt3:
                    f = SampleExpressions.Meinhardt3;
                    break;
                //case SampleExpressionType.GinzburgLandau:
                //    f = SampleExpressions.GinzburgLandau;
                //    break;
                default:
                    f = SampleExpressions.Turing;
                    break;
            }
            var g = new GLGene(rand, new[] { f(0), f(1) });
            g.RandomizeConstants();
            return g;
        }

       
        
        void Build()
        {
            var dif = new double[DiffusionConstantLength];
            if(Constants != null && Constants.Length >= dif.Length)
            {
                for (int i = 0; i < dif.Length; i++)
                {
                    dif[i] = Constants[i].Value;
                }
            }
            builder = root.Select(x => x.Build()).ToArray();
            for (int i = 0; i < builder.Length; i++)
            {
                builder[i].Random = rand;
            }
            var cons = builder.SelectMany(x => Enumerable.Range(0, x.ConstantCount).Select(i => x.GetConstant(i)));
            Constants = dif.Concat(cons).Select(x=> new GrayValue(x)).ToArray();
        }

        void RandomizeConstants()
        {
            for (int i = 0; i < Constants.Length; i++)
            {
                Constants[i] =  rand.NextGray();
            }
            ApplyConstants();
        }
    
        public Func<double[], double>[] Compile()
        {
            //ApplyConstants();
            return builder.Select(x => x.Compile()).ToArray();
        }

        void ApplyConstants()
        {
            int i = 0;
            foreach (var item in builder)
            {
                item.SetConstants(Constants.Skip(DiffusionConstantLength).Skip(i).Take(item.ConstantCount).Select(x=>x.Value));
                i += item.ConstantCount;
            }
        }

        public double GetDiffusion(int i)
        {
            if(i >= 2)
            {
                return 0.0;
            }
            //if(i== 0)
            //{
            //    return Constants[i].Value * Constants[(i + 1)].Value * .1f;//diffusion of activator should be smaller than inhibitor
            //}
            return Constants[i].Value;
        }


        public GLGene Clone()
        {
            var g = new GLGene(new Random(rand.Next()), root.Select(x=>x.CloneTree()).ToArray());
            for (int i = 0; i < Constants.Length; i++)
            {
                g.Constants[i] = Constants[i];
            }
            g.ApplyConstants();

            return g;
        }

        public GLGene Mutate(Random rnd)
        {
            GLGene g = this;
            int n = 0;
            do
            {
                g = g.Clone();
                g.SetRandom(rnd);

                n++;
                var r = rnd.Next(2);
                if (r == 1 || Config.Instance.DisableGP != 0)
                {
                    g.MutateConstants();
                    g.ApplyConstants();
                }
                else
                {
                    int i = rnd.Next(builder.Length);
                    g.builder[i].Mutate();
                    g.root[i] = g.builder[i].Root;
                }
                g.Build();
            } while (rnd.NextDouble() < Math.Pow(MutateRate, n) && n < 10);
            return g;
        }

        void SetRandom(Random rand)
        {
            this.rand = rand;
            foreach (var item in builder)
            {
                item.Random = rand;
            }
        }

        //パラメタ列の交叉 or 反応式の一方をパラメタごと交換 or 各反応式内での交叉
        public GLGene Crossover(GLGene o, Random rnd)
        {
            switch (rnd.Next(3))
            {
                case 0:
                    return CrossoverExpressions(o);
                case 1:
                    return CrossoverFormula(o);
                case 2:
                    return CrossoverConstants(o);
                default:
                    return CrossoverConstants(o);
            }
        }

        GLGene CrossoverExpressions(GLGene o)
        {
            var g = Clone();
            g.SetRandom(new Random(rand.Next()));
            int ind = rand.Next(g.builder.Length);
            g.builder[ind].Crossover(o.builder[ind]);
            g.root[ind] = g.builder[ind].Root;

            g.Build();
            return g;
        }

        GLGene CrossoverFormula(GLGene o)
        {
            var g = new GLGene(new Random(rand.Next()), new[] { root[0].CloneTree(), o.root[1].CloneTree() });
            g.Constants[0] = Constants[0];
            g.Constants[1] = o.Constants[1];
            int l = builder[0].ConstantCount;
            for (int i = 0; i < l; i++)
            {
                g.Constants[2 + i] = Constants[2 + i];
            }
            int ol0 = o.builder[0].ConstantCount;
            for (int i = 0; i < o.builder[1].ConstantCount; i++)
            {
                g.Constants[2 + l + i] = o.Constants[2 + ol0 + i];
            }
            g.ApplyConstants();
            return g;
        }

        GLGene CrossoverConstants(GLGene o)
        {
            var g = Clone();
            int length = Math.Min(Constants.Length, o.Constants.Length);
            for (int i = 0; i < length; i++)
            {
                if(rand.NextDouble() < .5)
                {
                    //g.Constants[i] = o.Constants[i];
                    g.Constants[i] = CrossoverConstant(g.Constants[i], o.Constants[i]);
                }
            }
            g.ApplyConstants();
            return g;
        }

        GrayValue CrossoverConstant(GrayValue v1, GrayValue v2)
        {
            UInt32 mask = (UInt32)rand.Next();
            return new GrayValue((v1.Bits & mask) | (v2.Bits & (~mask)));
        }

        void MutateConstants()
        {
            int f = rand.Next(Constants.Length);
            for (int i = 0; i < Constants.Length; i++)
            {
                MutateValue(i, i == f);
            }
        }

        //値の位置の入れ替え
        void MutateConstantArray()
        {
            if(Constants.Length <= 3)
            {
                return;
            }
            int i1 = rand.Next(2, Constants.Length);
            int i2 = rand.Next(2, Constants.Length - 1);
            if(i2 >= i1)
            {
                i2 += 1;
            }
            var tmp = Constants[i1];
            Constants[i1] = Constants[i2];
            Constants[i2] = tmp;
        }

        void MutateValue(int i, bool force)
        {
            UInt32 mask = 0;
            if (force)
            {
                int b = rand.Next(GrayValue.UseBitNumber);
                mask = (UInt32)1 << b;
            }
            for (int j = 0; j < GrayValue.UseBitNumber; j++)
            {
                if(rand.NextDouble() < MutateRange)
                {
                    mask |= (UInt32)1 << j;
                }
            }
            Constants[i].Bits ^= mask;
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            root.Aggregate(sb, (b, x) => b.AppendLine(x.ToString()));
            sb.Append("Diffusion:");
            Enumerable.Range(0, root.Length).Aggregate(sb, (b, i) => sb.Append(GetDiffusion(i).ToString("0.000")).Append(", "));
            return sb.ToString();
            //return $"{root[0]}\n{root[1]}\ndif={Constants[0].ToString("0.000")},{Constants[1].ToString("0.000")}";
        }

        public void Serialize(string fn)
        {
            Serializer.Serialize(root, Constants.Take(DiffusionConstantLength).Select(x=>x.Value).ToArray(), fn);
        }

        public static GLGene Deserialize(string fn, Random rand)
        {
            double[] dif = new double[DiffusionConstantLength];
            var e = Serializer.Deserialize(fn, dif);
            var g = new GLGene(rand, e);
            for (int i = 0; i < dif.Length; i++)
            {
                g.Constants[i] = new GLHost.GrayValue(dif[i]);
            }
            return g;
        }

    }
}
