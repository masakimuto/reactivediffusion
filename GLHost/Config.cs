﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace GLHost
{
    public class Config
    {
        static Config instance;
        public static Config Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Config();
                }
                return instance;
            }
        }

        public int GridWidth { get; private set; }
        public int GridHeight { get; private set; }
        public int GridSize { get; private set; }
        public string InitialPath { get; private set; }
        public float InitialRandom { get; private set; }
        public int AdamType { get; private set; }
        public float MutateRate { get; private set; }
        public float MutateRange { get; private set; }
        public int DisableGP { get; private set; }

        public static readonly string FileName = "config.txt";

        public Config()
        {
            GridWidth = 8;
            GridHeight = 8;
            GridSize = 64;
            InitialPath = null;
            InitialRandom = 0.5f;
            AdamType = 0;
            MutateRange = 0.2f;
            MutateRate = 0.3f;
            DisableGP = 0;
            try
            {
                var file = File.ReadAllLines(FileName);
                var files = file.Select(x => x.Split(null)).Where(x => x.Length >= 2).ToDictionary(x => x[0], x => x[1]);
                GridWidth = GetValue(files, "width", 8);
                GridHeight = GetValue(files, "height", 8);
                GridSize = GetValue(files, "size", 64);
                InitialPath = files["path"];
                InitialRandom = GetValue(files, "random", 0.5f);
                AdamType = GetValue(files, "type", 0);
                MutateRate = GetValue(files, "mrate", 0.3f);
                MutateRange = GetValue(files, "mrange", 0.2f);
                DisableGP = GetValue(files, "disableGP", 0);
            }
            catch
            {
            }
            InitialRandom = Clamp(InitialRandom, 1.0f, 0.0f);
            MutateRate = Clamp(MutateRate, 0.95f, 0.01f);
            MutateRange = Clamp(MutateRange, 1f, 0f);
            
        }

        public void CreateDefaultConfigFile()
        {
            var str = @"
width 8
height 8
size 64
path result
random 0.5
type 0
mrate 0.3
mrange 0.2
disableGP 0
";
            try
            {
                if (File.Exists(FileName))
                {
                    return;
                }
                File.WriteAllText(FileName, str);
            }
            catch
            {

            }
        }

        T Clamp<T>(T value, T max, T min) where T : IComparable
        {
            return value.CompareTo(max) > 0 ? max : (value.CompareTo(min) < 0 ? min : value);
        }


        delegate bool TryParseFunc<T>(string input, out T output);

        static T GetValue<T>(Dictionary<string, string> dict, string key, T defaultValue, TryParseFunc<T> func)
        {
            string value;
            if(dict.TryGetValue(key, out value))
            {
                T r;
                if (func(value, out r))
                {
                    return r;
                }
            }
            return defaultValue;
        }

        static float GetValue(Dictionary<string, string> dict, string key, float defaultValue)
        {
            return GetValue(dict, key, defaultValue, float.TryParse);
        }

        static int GetValue(Dictionary<string, string> dict, string key, int defaultValue)
        {
            return GetValue(dict, key, defaultValue, int.TryParse);
        }

    }
}
