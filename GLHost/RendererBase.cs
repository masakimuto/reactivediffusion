﻿using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GLHost
{
    public abstract class RendererBase : IDisposable
    {
        struct Vector2
        {
            public float x, y;

            public Vector2(float x, float y)
            {
                this.x = x;
                this.y = y;
            }
        }

        protected int vbo;
        protected int attrPos;
        
        protected int CreateVBO(int program)
        {
            var vbo = GL.GenBuffer();

            GL.BindBuffer(BufferTarget.ArrayBuffer, vbo);
            GL.BufferData(BufferTarget.ArrayBuffer, 6 * 8, new[]
            {
                new Vector2(-1, -1),
                new Vector2(-1, 1),
                new Vector2(1, 1),
                new Vector2(-1, -1),
                new Vector2(1, 1),
                new Vector2(1, -1)
            }, BufferUsageHint.StaticDraw);

            GL.UseProgram(program);
            attrPos = GL.GetAttribLocation(program, "v_pos");
            GL.BindBuffer(BufferTarget.ArrayBuffer, vbo);
            GL.EnableVertexAttribArray(attrPos);
            GL.VertexAttribPointer(attrPos, 2, VertexAttribPointerType.Float, false, 0, 0);
            GL.UseProgram(0);

            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            return vbo;
        }

        protected void LoadShaderFile(int program, string source, ShaderType type)
        {
            var vert = GL.CreateShader(type);
            GL.ShaderSource(vert, source);
            GL.CompileShader(vert);
            int err;
            GL.GetShader(vert, ShaderParameter.CompileStatus, out err);
            if (err == 0)
            {
                throw new Exception("Compile error: " + GL.GetShaderInfoLog(vert));
            }
            GL.AttachShader(program, vert);
            GL.LinkProgram(program);
            GL.DeleteShader(vert);
        }

        protected int LoadShader(string vs, string fs)
        {
            var p = GL.CreateProgram();
            LoadShaderFile(p, vs, ShaderType.VertexShader);
            LoadShaderFile(p, fs, ShaderType.FragmentShader);
            return p;
        }

        public abstract void Draw();


        public virtual void Dispose()
        {
            if(vbo != 0)
            {
                GL.DeleteBuffer(vbo);
                vbo = 0;
            }
        }

        protected void CheckError()
        {
            var err = GL.GetError();
            if (err != ErrorCode.NoError)
            {
                Console.WriteLine(err.ToString());
            }
        }
    }
}
