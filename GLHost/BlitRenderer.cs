﻿using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GLHost
{
    public class BlitRenderer : RendererBase
    {
        int program;
        //int dummyTex;

        public void LoadContent()
        {
            program = GL.CreateProgram();
            LoadShaderFile(program, File.ReadAllText("Shaders\\test.vert"), ShaderType.VertexShader);
            LoadShaderFile(program, File.ReadAllText("Shaders\\blit.frag"), ShaderType.FragmentShader);

            CreateVBO(program);

            //dummyTex = GL.GenTexture();
            //GL.BindTexture(TextureTarget.Texture2D, dummyTex);
            //byte[] data = new byte[] { 128, 255, 0 };
            //GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgb, 1, 1, 0, PixelFormat.Rgb, PixelType.UnsignedByte, data);
            //GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.Repeat);

            GL.UseProgram(program);
            GL.Uniform1(GL.GetUniformLocation(program, "iTexture"), 0);
            GL.UseProgram(0);

        }

        public void Draw(GeneticRenderer r)
        {
            GL.UseProgram(program);
            GL.EnableVertexAttribArray(attrPos);
            GL.BindBuffer(BufferTarget.ArrayBuffer, vbo);
            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, r.GetTexture());
            //GL.BindTexture(TextureTarget.Texture2D, dummyTex);

            GL.DrawArrays(PrimitiveType.Triangles, 0, 6);

            GL.DisableVertexAttribArray(attrPos);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            GL.BindTexture(TextureTarget.Texture2D, 0);
            
        }

        public override void Draw()
        {
            throw new NotImplementedException();
        }

        public override void Dispose()
        {
            base.Dispose();
            GL.DeleteProgram(program);

        }
    }
}
