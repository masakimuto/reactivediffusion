﻿#version 400

uniform sampler2D iTexture;

in vec2 v_uv;
out vec4 fc;
void main(){
	fc = texture2D(iTexture, v_uv);
	//gl_FragColor.ba = vec2(0.5, 1.0);
}
