﻿#version 400

in vec2 v_uv;
out vec4 fc;
uniform vec2 iResolution;

//http://web.archive.org/web/20080211204527/http://lumina.sourceforge.net/Tutorials/Noise.html
float rand(vec2 co){
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

void main(){
	vec2 uv = v_uv * 2 - vec2(1);
	uv.xy -= vec2(0.3);
	fc.a = vec3(1).x;
	fc.rgb = vec3(clamp((1 - length(uv) * 30) * 100, 0, 200));
	fc.r = rand(uv.xy);
	fc.g = rand(uv.yx + vec2(1.0, 1.0));
	fc.b = rand(vec2(uv.x*uv.y, uv.x+uv.y));
	fc.b = 0;
	fc.rgb *= 0.2;
}