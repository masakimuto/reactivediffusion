﻿#version 400

in vec2 v_uv;
out vec4 fc;
uniform sampler2D iTexture;
uniform vec2 iResolution;
uniform vec3 iDiffusion;
uniform vec3 iAnisotoropy;//x-diffuse,y-diffuse, angle
uniform float iFrequency;
uniform float iDt;

uniform vec2 of[8] = { vec2(1.0, 0.0), vec2(-1.0, 0.0), vec2(0.0, 1.0), vec2(0.0, -1.0),
 vec2(1.0, 1.0), vec2(-1.0, 1.0), vec2(-1.0, -1.0), vec2(1.0, -1.0) };

vec3 diffuse(vec3 c){
	vec2 offset = vec2(1.0) / textureSize(iTexture, 0);
	/*
	vec3 s = c * -4.0;
	for(int i = 0; i < 4; i++){
		s += texture2D(iTexture, v_uv + offset * of[i]).rgb;
	}
	return s;
	*/
	
	vec2 dif = iAnisotoropy.xy;
	float angle = iAnisotoropy.z;
	float cs = cos(angle);
	float sn = sin(angle);
	vec3 a = vec3(dif.x * cs * cs + dif.y * sn * sn, (dif.y - dif.x) * cs * sn, dif.y * cs * cs + dif.x * sn * sn);
	vec3 s = vec3(0.0);
	vec3 texs[8];
	for(int i = 0; i < 8; i++){
		texs[i] = texture2D(iTexture, v_uv + offset * of[i]).rgb;
	}
	s -= a.y * texs[6];
	s += 2.0 * a.z * texs[3];
	s += a.y * texs[7];
	s += 2.0 * a.x * texs[1];
	s -= 4.0 * (a.x + a.z) * c;
	s += 2.0 * a.x * texs[0];
	s += a.y * texs[5];
	s += 2.0 * a.z * texs[2];
	s -= a.y * texs[4];
	
	s.r = (texs[0].r + texs[1].r + texs[2].r + texs[3].r - c.r * 4.0);

	return s;
	
}

vec3 reaction(vec3 c){
	vec3 r;
	float p0 = c.x;
	float p1 = c.y;
	float p2 = c.z;
	
	r.x = STAB0;
	r.y = STAB1;
	r.z = STAB2;
	
	//r.x = (-p2/((0.566+p0*pow((-(-(-p1))), 2.0))-(-(-((p2-p1)/p1-((0.005-p2)-p1/p0))))/(-((0.246/0.331+(0.488+p0))+(-(-p1)))*((p2+0.000)*0.450/p0-((0.220+p2)+p2)))))/pow((0.059-p1), 2.0);
	//r.y = p2*((-(p2+(pow((-p2)*((p0-0.606)+(-p0)), 2.0)+(pow(pow(p0*p2, 2.0), 2.0)-(-(-(0.395+0.612))))))/(-(-p2)))-(p0-pow((-p2), 2.0))/(0.833-p1));
	//r.z = p2/(pow(((-p1)-((-(p2-p0))-p2)/0.414)/pow((0.756+p2), 2.0), 2.0)/p0+((pow(pow(pow((p1-0.873), 2.0), 2.0)*p0, 2.0)+(-(-(-(-0.901/p2)))))-(-p2))*pow(0.891, 2.0)*pow(0.441, 2.0));
	
	return r;
	//return vec3(0.0);
}

void main(){
	vec3 c = texture2D(iTexture, v_uv).rgb;
	vec3 dc = iDiffusion / 8.0;
	fc.a = 1.0;	
	fc.rgb = c + (diffuse(c) * dc + reaction(c) * iFrequency * 0.1) * iDt;
	fc.rgb = clamp(fc.rgb, vec3(0.0001), vec3(128.0));
}