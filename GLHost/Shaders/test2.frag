﻿#version 400

in vec2 v_uv;
out vec4 fc;
uniform sampler2D iTexture;
uniform vec2 iResolution;
uniform vec3 iDiffusion;

uniform vec2 of[4] = { vec2(1.0, 0.0), vec2(-1.0, 0.0), vec2(0.0, 1.0), vec2(0.0, -1.0),};
//vec2(1.0, 1.0), vec2(-1.0, 1.0), vec2(-1.0, -1.0), vec2(1.0, -1.0) };

vec3 diffuse(vec3 c){
	vec2 offset = vec2(1.0) / textureSize(iTexture, 0);
	vec3 s = c * -4.0;
	for(int i = 0; i < 4; i++){
		s += texture2D(iTexture, v_uv + offset * of[i]).rgb;
	}
	return s;
}

vec3 reaction(vec3 c){
	vec3 r;
	float p0 = c.x;
	float p1 = c.y;
	float p2 = c.z;
	r.x = (-p2/((0.566+p0*pow((-(-(-p1))), 2.0))-(-(-((p2-p1)/p1-((0.005-p2)-p1/p0))))/(-((0.246/0.331+(0.488+p0))+(-(-p1)))*((p2+0.000)*0.450/p0-((0.220+p2)+p2)))))/pow((0.059-p1), 2.0);
	r.y = p2*((-(p2+(pow((-p2)*((p0-0.606)+(-p0)), 2.0)+(pow(pow(p0*p2, 2.0), 2.0)-(-(-(0.395+0.612))))))/(-(-p2)))-(p0-pow((-p2), 2.0))/(0.833-p1));
	r.z = p2/(pow(((-p1)-((-(p2-p0))-p2)/0.414)/pow((0.756+p2), 2.0), 2.0)/p0+((pow(pow(pow((p1-0.873), 2.0), 2.0)*p0, 2.0)+(-(-(-(-0.901/p2)))))-(-p2))*pow(0.891, 2.0)*pow(0.441, 2.0));
	
	return r;
	//return vec3(0.0);
}

void main(){
	float dt = 0.01;
	vec3 c = texture2D(iTexture, v_uv).rgb;
	vec3 dc = vec3(0.025, 0.201, 0.864) / 4.0;
	fc.a = 1.0;	
	fc.rgb = c;
	fc.rgb = c + (diffuse(c) * dc) * dt;
	fc.rgb = fc.rgb + reaction(fc.rgb) * dt;
	fc.rgb = clamp(fc.rgb, vec3(0.01), vec3(5000.0));
}