﻿#version 400

attribute vec2 v_pos;
out vec2 v_uv;

void main(){
	gl_Position = vec4(v_pos, 0.0, 1.0);
	v_uv = (v_pos + 1.0) * 0.5;//0..1
}