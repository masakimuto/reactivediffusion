﻿#version 400

in vec2 v_uv;
out vec4 fc;
uniform sampler2D iTexture;
uniform vec2 iResolution;
uniform vec3 iAnisotoropy;//x-diffuse,y-diffuse, angle


//uniform vec2 of[4] = { vec2(1.0, 0.0), vec2(-1.0, 0.0), vec2(0.0, 1.0), vec2(0.0, -1.0) };
uniform vec2 of[8] = { vec2(1.0, 0.0), vec2(-1.0, 0.0), vec2(0.0, 1.0), vec2(0.0, -1.0),
 vec2(1.0, 1.0), vec2(-1.0, 1.0), vec2(-1.0, -1.0), vec2(1.0, -1.0) };

/*
vec3 diffuse(vec3 c){
	vec2 offset = vec2(1.0) / iResolution;
	vec3 s = c * -4.0;
	for(int i = 0; i < 4; i++){
		s += texture2D(iTexture, v_uv + offset * of[i]).rgb;
	}
	return s;
}
*/

vec3 diffuse(vec3 c){
	vec2 offset = vec2(1.0) / textureSize(iTexture, 0);
	/*
	vec3 s = c * -4.0;
	for(int i = 0; i < 4; i++){
		s += texture2D(iTexture, v_uv + offset * of[i]).rgb;
	}
	return s;
	*/
	vec2 dif = iAnisotoropy.xy * 2;
	//dif.xy *= dif.xy;
	dif = vec2(1.0, 0.0);
	float angle = iAnisotoropy.z;
	angle = 0.4;
	float cs = cos(angle);
	float sn = sin(angle);
	vec3 a = vec3(dif.x * cs * cs + dif.y * sn * sn, (dif.y - dif.x) * cs * sn, dif.y * cs * cs + dif.x * sn * sn);
	vec3 s = vec3(0.0);
	vec3 texs[8];
	for(int i = 0; i < 8; i++){
		texs[i] = texture2D(iTexture, v_uv + offset * of[i]).rgb;
	}
	vec3 ay = vec3(a.y, 0.0, 0.0).yxz;
	vec3 ax = vec3(a.x, 1.0, 1.0).yxz;
	vec3 az = vec3(a.z, 1.0, 1.0).yxz;
	float h = 1.0 / 16.0;

	s -= ay * texs[6];
	s += 2 * az * texs[3];
	s += ay * texs[7];
	s += 2 * ax * texs[1];
	s += 2 * ax * texs[0];
	s += ay * texs[5];
	s += 2 * az * texs[2];
	s -= ay * texs[4];
	s -= (4 * (ax + az)) * c;
	s /= h * h * 2.0;
	return s;
}

vec3 reaction(vec3 c){
	vec3 r;
	//r.x = c.x * 0.06 + c.y * -0.12; //+ 0.001;
	//r.y = c.x * 0.1 * c.y * -0.05;
	float d = (c.x > c.y ? 1.0 : 0.0) * 0.03;
	r.x = d;
	r.y = d;
	r.z = 0.0;
	return r;
}

void main(){
	float dt = 0.05;
	vec3 c = texture2D(iTexture, v_uv).rgb;
	vec3 dc = vec3(1.0 / 128.0, 1.0 / 4.0, 0.0) * 0.06;
	fc.a = 1.0;
	
	fc.rgb = c + (diffuse(c) * dc - c * vec3(0.10, 0.10, 0.0) * 0.3 + reaction(c) ) * dt;
	fc.rgb = clamp(fc.rgb, vec3(0.0), vec3(10.0));
}