﻿#version 400

uniform sampler2D iTexture;
uniform float iMin;
uniform float iMax;
uniform vec4 iBgColor;
uniform vec4 iActColor;
uniform vec4 iInhColor;
uniform float iSelection;
uniform float iTime;
uniform float iFlip;//flip up-down for output 

#define BR 0.03

in vec2 v_uv;
out vec4 fc;
void main(){
	vec2 uv = vec2(v_uv.x, (1.0 - v_uv.y) * iFlip + v_uv.y * (1.0 - iFlip));
	vec3 val = texture2D(iTexture, uv).rgb;
	val = max(val - iMin, vec3(0.0)) / abs(iMax - iMin);
	val = clamp(val, vec3(0.0), vec3(1.0));
	fc = max(0.0, (1.0 - val.x - val.y)) * iBgColor + val.x * iActColor + val.y * iInhColor;
	fc = clamp(fc, vec4(0.0), vec4(1.0));

	//draw selection icon
	vec2 border = vec2(0.5, 0.5) - abs(v_uv - vec2(0.5, 0.5)) + (1.0 - iSelection);
	if(any(lessThan(border, vec2(BR, BR)))){
		fc.rgb += sin((v_uv.x + v_uv.y) * 3.1415 * 16.0 + iTime * 4.0);
	}
}
