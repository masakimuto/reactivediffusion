﻿using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GLHost
{
    public class TestRDRenderer : RendererBase
    {


        readonly int Width;
        readonly int Height;

        public TestRDRenderer(int width, int height)
        {
            Width = width;
            Height = height;
        }

        ~TestRDRenderer()
        {
            if (!disposed)
            {
                Dispose();
            }
        }

        int program;
        bool disposed;
        int[] texture = new int[2];
        int[] fbo = new int[2];
        int currentTexture = 0;
        int seedVbo, seedProgram;

        public int GetTexture()
        {
            return texture[currentTexture];
        }

        public void LoadContent()
        {
            program = LoadShader(File.ReadAllText("Shaders\\test.vert"), File.ReadAllText("Shaders\\test2.frag"));
            seedProgram = LoadShader(File.ReadAllText("Shaders\\test.vert"), File.ReadAllText("Shaders\\seed.frag"));

            vbo = CreateVBO(program);
            seedVbo = CreateVBO(seedProgram);

            GL.UseProgram(program);
            GL.Uniform2(GL.GetUniformLocation(program, "iResolution"), (float)Width, (float)Height);
            //GL.Uniform1(GL.GetUniformLocation(program, "iTexture"), 0);
            GL.UseProgram(0);

            GL.GenTextures(2, texture);
            for (int i = 0; i < 2; i++)
            {
                GL.BindTexture(TextureTarget.Texture2D, texture[i]);
                GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba32f, Width, Height, 0, PixelFormat.Rgba, PixelType.Float, new IntPtr());
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.Repeat);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.Repeat);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            }

            GL.GenFramebuffers(2, fbo);
            for (int i = 0; i < 2; i++)
            {
                GL.BindFramebuffer(FramebufferTarget.Framebuffer, fbo[i]);
                GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, TextureTarget.Texture2D, texture[i], 0);
                GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
            }

            GL.UseProgram(seedProgram);
            GL.EnableVertexAttribArray(attrPos);
            GL.BindBuffer(BufferTarget.ArrayBuffer, seedVbo);
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, fbo[0]);
            GL.DrawArrays(PrimitiveType.Triangles, 0, 6);
            GL.Flush();
            GL.DisableVertexAttribArray(attrPos);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
        }

        int count = 0;

        public override void Draw()
        {
            count++;
            currentTexture = 1 - currentTexture;
            GL.UseProgram(program);
            GL.EnableVertexAttribArray(attrPos);
            GL.BindBuffer(BufferTarget.ArrayBuffer, vbo);
            GL.BindTexture(TextureTarget.Texture2D, texture[1 - currentTexture]);
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, fbo[currentTexture]);
            GL.Clear(ClearBufferMask.ColorBufferBit);

            GL.DrawArrays(PrimitiveType.Triangles, 0, 6);

            GL.Flush();

            GL.DisableVertexAttribArray(attrPos);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
            GL.BindTexture(TextureTarget.Texture2D, 0);
        }

        public override void Dispose()
        {
            if (disposed)
            {
                return;
            }
            base.Dispose();
            try
            {
                GL.DeleteProgram(program);
                GL.DeleteFramebuffers(2, fbo);
                GL.DeleteTextures(2, texture);
                GL.DeleteProgram(seedProgram);
                GL.DeleteBuffer(seedVbo);
            }
            catch (Exception e)
            {
                //System.Windows.MessageBox.Show(e.ToString());
                disposed = true;
            }
            disposed = true;
        }


        
    }
}
