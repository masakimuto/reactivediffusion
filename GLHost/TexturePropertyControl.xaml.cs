﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GLHost
{
   

    /// <summary>
    /// Interaction logic for TexturePropertyControl.xaml
    /// </summary>
    public partial class TexturePropertyControl : UserControl
    {
        public TexturePropertyControl()
        {
            InitializeComponent();
        }

        GLRenderer glRenderer;
        bool isInitialized;

        public void Init(GLRenderer renderer)
        {
            glRenderer = renderer;
            isInitialized = true;
        }

        public void FpsUpdate(double value)
        {
            labelFPS.Content = String.Format("{0}(set)*{1:F3}fps", glRenderer.UpdateSetNumber, value);
        }

        void FreqSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            var val = (float)sliderFrequency.GetPowerValue(e.NewValue);
            SetLabelFreq(val);
            if (isInitialized && !showing)
            {
                glRenderer.Renderer.ApplyFrequency(val, glRenderer.GetSelection());
            }
        }

        void SetLabelFreq(float v) => labelFrequency.Content = "反応速度:" + FormatExponentialValue(v);

        string FormatExponentialValue(float val)
        {
            return val.ToString("0.00E+0");
        }


        private void MaxSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            var val = (float)(sender as ExponentialSlider).GetPowerValue(e.NewValue);
            SetLabelMax(val);
            if (isInitialized && !showing)
            {
                glRenderer.Renderer.ApplyMax(val, glRenderer.GetSelection());
            }
        }

        void SetLabelMax(float val) => labelMax.Content = "表示値上限:" + FormatExponentialValue(val);

        private void MinSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            var val = (float)(sender as ExponentialSlider).GetPowerValue(e.NewValue);
            SetLabelMin(val);
            if (isInitialized && !showing)
            {
                glRenderer.Renderer.ApplyMin(val, glRenderer.GetSelection());
            }
        }

        void SetLabelMin(float val) => labelMin.Content = "表示値下限:" + FormatExponentialValue(val);

        private void DtSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            var val = (float)(sender as ExponentialSlider).GetPowerValue(e.NewValue);
            SetLabelDt(val);
            if (isInitialized && !showing)
            {
                glRenderer.Renderer.ApplyDt(val);
            }
        }

        private void SetLabelDt(float val)
        {
            labelDt.Content = "時間刻み幅:" + FormatExponentialValue(val);
        }

        private void AngleSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            var val = (float)e.NewValue;
            SetLabelAngle(val);
            if (isInitialized && !showing)
            {
                glRenderer.Renderer.ApplyAngle(val, glRenderer.GetSelection());
            }
        }

        private void SetLabelAngle(float val)
        {
            labelAngle.Content = String.Format("拡散角度:{0:F3}", val);
        }

        private void AnisoSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            var val = (float)e.NewValue;
            SetLabelAnis(val);
            if (isInitialized && !showing)
            {
                glRenderer.Renderer.ApplyAnis(val, glRenderer.GetSelection());
            }
        }

        private void SetLabelAnis(float val)
        {
            labelAnis.Content = String.Format("拡散異方性:{0:F3}", val);
        }

        private void UpdateSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (isInitialized)
            {
                glRenderer.UpdateSetNumber = (int)e.NewValue;
            }
        }

        private void ColorRectClick(object sender, MouseButtonEventArgs e)
        {
            var dialog = new System.Windows.Forms.ColorDialog();
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Color col = Color.FromArgb(dialog.Color.A, dialog.Color.R, dialog.Color.G, dialog.Color.B);
                (sender as Rectangle).Fill = new SolidColorBrush(col);
                var sel = glRenderer.GetSelection();
                switch ((sender as Rectangle).Tag as string)
                {
                    case "bg":
                        glRenderer.Renderer.ApplyBgColor(col, sel);
                        break;
                    case "act":
                        glRenderer.Renderer.ApplyActColor(col, sel);
                        break;
                    case "inh":
                        glRenderer.Renderer.ApplyInhColor(col, sel);
                        break;
                    default:
                        break;
                }
            }
        }

        bool showing = false;

        public void ShowGeneProperty(ExtentGene gene)
        {
            showing = true;
            sliderFrequency.PowerValue = gene.Frequency;
            sliderMax.PowerValue = gene.ValueMax;
            sliderMin.PowerValue = gene.ValueMin;
            sliderAnis.Value = gene.Anisotoropy;
            sliderAngle.Value = gene.Angle;
            sliderDt.PowerValue = gene.Dt;
            colorPickerBg.SelectedColor = gene.BgColor;
            colorPickerAc.SelectedColor = gene.ActColor;
            colorPickerIn.SelectedColor = gene.InhColor;

            gene.GetHashCode();
            showing = false;
        }

        private void SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            if (isInitialized && !showing)
            {
                var sel = glRenderer.GetSelection();
                var col = e.NewValue.Value;
                switch ((sender as Control).Tag as string)
                {
                    case "bg":
                        glRenderer.Renderer.ApplyBgColor(col, sel);
                        break;
                    case "ac":
                        glRenderer.Renderer.ApplyActColor(col, sel);
                        break;
                    case "in":
                        glRenderer.Renderer.ApplyInhColor(col, sel);
                        break;
                    default:
                        break;
                }
            }
        }
        
    }
}
