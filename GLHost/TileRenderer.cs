﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeneticReaction.Expressions;
using OpenTK.Graphics.OpenGL;
using System.IO;
using Color = System.Windows.Media.Color;
using System.Drawing;

namespace GLHost
{
    public class TileRenderer : RendererBase
    {
        GeneticRenderer[] renderers;
        readonly int Num = 4;
        readonly int Size = 64;
        readonly int Width, Height;

        int program;
        
        public bool[] Selections { get; set; }
        int iSelection;
        float iTime;


        public TileRenderer(int size, int width, int height)
        {
            Size = size;
            Width = width;
            Height = height;
            Num = width * height;
            renderers = new GeneticRenderer[Num];
            for (int i = 0; i < renderers.Length; i++)
            {
                renderers[i] = new GeneticRenderer(size, size);
            }
            Selections = new bool[renderers.Length];
        }

        public void LoadContent()
        {
            Random rand = new Random();
            for (int i = 0; i < renderers.Length; i++)
            {
                renderers[i].LoadContent();
                //renderers[i].SetGene(Gene.Adam(rand));
            }
            program = GL.CreateProgram();
            LoadShaderFile(program, File.ReadAllText("Shaders\\test.vert"), ShaderType.VertexShader);
            LoadShaderFile(program, File.ReadAllText("Shaders\\scale.frag"), ShaderType.FragmentShader);

            vbo = CreateVBO(program);

            GL.UseProgram(program);
            GL.Uniform1(GL.GetUniformLocation(program, "iTexture"), 0);
            iSelection = GL.GetUniformLocation(program, "iSelection");
            GL.Uniform1(GL.GetUniformLocation(program, "iFlip"), 0.0f);
            GL.UseProgram(0);
            iTime = 0.0f;
        }

        public void SetGenes(ExtentGene[] genes)
        {
            for (int i = 0; i < renderers.Length; i++)
            {
                renderers[i].SetGene(genes[i]);
            }
        }

        
        public void Update(int times)
        {
            for (int i = 0; i < renderers.Length; i++)
            {
                renderers[i].DrawMultiple(times);
            }
        }

        public void Restart()
        {
            foreach (var item in renderers)
            {
                item.ApplySeed();
            }
        }

        public override void Draw()
        {
            //iTime++;
            var time = System.DateTime.Now;
            iTime = time.Millisecond * 0.001f + time.Second;
            //GL.Clear(ClearBufferMask.ColorBufferBit);
            GL.UseProgram(program);
            GL.Uniform1(GL.GetUniformLocation(program, "iTime"), iTime);
            GL.EnableVertexAttribArray(attrPos);
            GL.BindBuffer(BufferTarget.ArrayBuffer, vbo);
            GL.ActiveTexture(TextureUnit.Texture0);
            for (int i = 0; i < renderers.Length; i++)
            {
                var gene = renderers[i].Gene;
                GL.Uniform1(GL.GetUniformLocation(program, "iMin"), gene.ValueMin);
                GL.Uniform1(GL.GetUniformLocation(program, "iMax"), gene.ValueMax);
                GL.Uniform1(iSelection, Selections[i] ? 1.0f : 0.0f);

                GL.Uniform4(GL.GetUniformLocation(program, "iBgColor"), 1, gene.BgColorVector);
                GL.Uniform4(GL.GetUniformLocation(program, "iActColor"), 1, gene.ActColorVector);
                GL.Uniform4(GL.GetUniformLocation(program, "iInhColor"), 1, gene.InhColorVector);

                GL.Viewport((i / Height) * Size, (i % Height) * Size, Size, Size);
                GL.BindTexture(TextureTarget.Texture2D, renderers[i].GetTexture());
                GL.DrawArrays(PrimitiveType.Triangles, 0, 6);
            }
            GL.DisableVertexAttribArray(attrPos);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            GL.BindTexture(TextureTarget.Texture2D, 0);
        }

        public void Output(int index, string fileName)
        {
            var gene = renderers[index].Gene;

            int tex;
            tex = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, tex);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgb, Size, Size, 0, PixelFormat.Rgb, PixelType.UnsignedByte, IntPtr.Zero);

            int fbo = GL.GenFramebuffer();
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, fbo);
            GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, TextureTarget.Texture2D, tex, 0);
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);

            GL.UseProgram(program);
            GL.Uniform1(GL.GetUniformLocation(program, "iMin"), gene.ValueMin);
            GL.Uniform1(GL.GetUniformLocation(program, "iMax"), gene.ValueMax);
            GL.Uniform1(GL.GetUniformLocation(program, "iTime"), iTime);
            GL.Uniform1(GL.GetUniformLocation(program, "iFlip"), 1.0f);
            GL.Uniform4(GL.GetUniformLocation(program, "iBgColor"), 1, gene.BgColorVector);
            GL.Uniform4(GL.GetUniformLocation(program, "iActColor"), 1, gene.ActColorVector);
            GL.Uniform4(GL.GetUniformLocation(program, "iInhColor"), 1, gene.InhColorVector);

            GL.EnableVertexAttribArray(attrPos);
            GL.BindBuffer(BufferTarget.ArrayBuffer, vbo);
            GL.ActiveTexture(TextureUnit.Texture0);
            GL.Uniform1(iSelection, 0.0f);
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, fbo);
            GL.Viewport(0, 0, Size, Size);
            GL.BindTexture(TextureTarget.Texture2D, renderers[index].GetTexture());
            GL.DrawArrays(PrimitiveType.Triangles, 0, 6);
            GL.Uniform1(GL.GetUniformLocation(program, "iFlip"), 0.0f);
            GL.DisableVertexAttribArray(attrPos);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            GL.BindTexture(TextureTarget.Texture2D, 0);
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);

            var bitmap = new Bitmap(Size, Size, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            var data = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), System.Drawing.Imaging.ImageLockMode.WriteOnly, bitmap.PixelFormat);
            GL.BindTexture(TextureTarget.Texture2D, tex);
            GL.GetTexImage(TextureTarget.Texture2D, 0, PixelFormat.Bgr, PixelType.UnsignedByte, data.Scan0);
            GL.BindTexture(TextureTarget.Texture2D, 0);

            bitmap.UnlockBits(data);
            bitmap.Save(fileName, System.Drawing.Imaging.ImageFormat.Png);


            GL.DeleteTexture(tex);
            GL.DeleteFramebuffer(fbo);
        }

        public override void Dispose()
        {
            base.Dispose();
            GL.DeleteProgram(program);
            program = 0;
            for (int i = 0; i < renderers.Length; i++)
            {
                renderers[i].Dispose();
                renderers[i] = null;
            }
        }

        public void ApplyFrequency(float freq, IEnumerable<int> selection)
        {
            ApplyParam(freq, selection, (r, v) => r.ApplyFrequency(v));
        }

        public void ApplyDt(float dt)
        {
            foreach (var item in renderers)
            {
                item.ApplyDt(dt);
            }
        }


        internal void ApplyAnis(float val, IEnumerable<int> selection)
        {
            ApplyParam(val, selection, (r, v) => r.ApplyAnis(v));
        }

        internal void ApplyAngle(float val, IEnumerable<int> selection)
        {
            ApplyParam(val, selection, (r, v) => r.ApplyAngle(v));
        }

        internal void ApplyMax(float val, IEnumerable<int> selection)
        {
            ApplyParam(val, selection, (r, v) => r.Gene.ValueMax = v);
        }

        internal void ApplyMin(float val, IEnumerable<int> selection)
        {
            ApplyParam(val, selection, (r, v) => r.Gene.ValueMin = v);
        }

        internal void ApplyBgColor(Color c, IEnumerable<int> selection)
        {
            ApplyParam(c, selection, (r, v) => r.Gene.BgColor = v);
        }

        internal void ApplyActColor(Color c, IEnumerable<int> selection)
        {
            ApplyParam(c, selection, (r, v) => r.Gene.ActColor = v);
        }

        internal void ApplyInhColor(Color c, IEnumerable<int> selection)
        {
            ApplyParam(c, selection, (r, v) => r.Gene.InhColor = v);
        }


        void ApplyParam<T>(T val, IEnumerable<int> selection, Action<GeneticRenderer, T> target)
        {
            if (selection.Any())
            {
                foreach (var item in selection)
                {
                    target(renderers[item], val);
                }
            }
            else
            {
                foreach (var item in renderers)
                {
                    target(item, val);
                }
            }
        }
    }
}
