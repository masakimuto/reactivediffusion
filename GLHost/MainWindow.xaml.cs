﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OpenTK.Graphics.OpenGL;
using OpenTK.Graphics;
using System.Windows.Forms.Integration;
using System.Windows.Threading;

namespace GLHost
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();

        }



        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            glRenderer.Dispose();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //new Perf();
            //return;

        }
    }
}
