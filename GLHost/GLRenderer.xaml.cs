﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using System.Windows.Threading;

namespace GLHost
{
    /// <summary>
    /// GLRenderer.xaml の相互作用ロジック
    /// </summary>
    public partial class GLRenderer : UserControl, IDisposable
    {
        public static readonly DependencyProperty UnitSizeProperty =
            DependencyProperty.Register("UnitSize", typeof(int), typeof(GLRenderer), new PropertyMetadata(64));
        public static readonly DependencyProperty UnitHeightProperty =
            DependencyProperty.Register("UnitHeight", typeof(int), typeof(GLRenderer), new PropertyMetadata(4));
        public static readonly DependencyProperty UnitWidthProperty =
            DependencyProperty.Register("UnitWidth", typeof(int), typeof(GLRenderer), new PropertyMetadata(4));


        public event Action<int, bool> OnItemClick;

        public double Fps { get; private set; }
        public event Action<double> OnFPSUpdate;


        DispatcherTimer timer;
        public TileRenderer Renderer { get; set; }
        public int UnitSize
        {
            get { return (int)GetValue(UnitSizeProperty); }
            set { SetValue(UnitSizeProperty, value); }
        }
        public int UnitHeight
        {
            get { return (int)GetValue(UnitHeightProperty); }
            set { SetValue(UnitHeightProperty, value); }
        }
        public int UnitWidth
        {
            get { return (int)GetValue(UnitWidthProperty); }
            set { SetValue(UnitWidthProperty, value); }
        }

        public int UnitCount
        {
            get
            {
                return UnitWidth * UnitHeight;
            }
        }

        int updateSetNumber = 1;

        public int UpdateSetNumber
        {
            get { return updateSetNumber; }
            set
            {
                updateSetNumber = value < 1 ? 1 : value;
            }
        }

        bool[] selection;
        bool isActive;

        public int ElapsedStep
        {
            get;
            private set;
        }

        public double ElapsedTime
        {
            get;
            private set;
        }

        public GLRenderer()
        {
            InitializeComponent();
        }

        Size GetSize()
        {
            var ct = PresentationSource.FromVisual(this).CompositionTarget;
            var scaleX = ct.TransformToDevice.M11;
            var scaleY = ct.TransformToDevice.M22;
            return new Size(UnitSize * UnitWidth / scaleX, UnitSize * UnitHeight / scaleY);
        }


        protected override Size MeasureOverride(Size constraint)
        {
            UnitWidth = Config.Instance.GridWidth;
            UnitHeight = Config.Instance.GridHeight;
            UnitSize = Config.Instance.GridSize;
            return GetSize();
        }

        protected override Size ArrangeOverride(Size arrangeBounds)
        {
            base.ArrangeOverride(arrangeBounds);
            return arrangeBounds;
        }

        private void GLControl_Load(object sender, EventArgs e)
        {
            UpdateSetNumber = 5;
            var size = GetSize();
            glControl.Size = new System.Drawing.Size((int)size.Width, (int)size.Height);
            GL.ClearColor(Color4.CornflowerBlue);
            timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(100);
            timer.Tick += Timer_Tick;
            timer.Start();
            Renderer = new TileRenderer(UnitSize, UnitWidth, UnitHeight);
            Renderer.LoadContent();

            selection = new bool[UnitWidth * UnitHeight];

            fpsLastTime = DateTime.Now;
            isActive = true;
            StartCount();
        }

        public void StartCount()
        {
            ElapsedStep = 0;
            ElapsedTime = 0.0;
        }

        public void SetActive(bool isActive)
        {
            this.isActive = isActive;
            if (isActive)
            {
                fpsLastTime = DateTime.Now;
            }
            else
            {
                ElapsedTime += DateTime.Now.Subtract(fpsLastTime).TotalSeconds;
            }
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            glControl.Invalidate();
        }

        public void UpdateDisplay()
        {
            GL.Clear(ClearBufferMask.ColorBufferBit);
            if(Renderer != null)
            {
                Renderer.Draw();
            }
            glControl.SwapBuffers();
        }

        private void GLControl_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            GL.Clear(ClearBufferMask.ColorBufferBit);
            if(Renderer != null)
            {
                if (isActive)
                {
                    ElapsedStep += UpdateSetNumber;
                    Renderer.Update(UpdateSetNumber);
                    CalcFps();
                }
                Renderer.Draw();
            }
            glControl.SwapBuffers();
        }

        int fpsCount = 0;
        DateTime fpsLastTime;

        void CalcFps()
        {
            fpsCount += 1;
            var now = DateTime.Now;
            var diff = now.Subtract(fpsLastTime).TotalSeconds;
            
            if (diff > 5.0)
            {
                Fps = fpsCount / diff;
                fpsCount = 0;
                fpsLastTime = now;
                if(OnFPSUpdate != null)
                {
                    OnFPSUpdate(Fps);
                }
                if (isActive)
                {
                    ElapsedTime += diff;
                }
            }
        }

        private void GLControl_SizeChanged(object sender, EventArgs e)
        {
            GL.Viewport(0, 0, glControl.Width, glControl.Height);
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            
        }

        public void Dispose()
        {
            timer.Stop();
            if (Renderer != null)
            {
                Renderer.Dispose();
                Renderer = null;
            }
        }
        
        private void glControl_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            int x = (int)e.X / UnitSize;
            int y = (int)e.Y / UnitSize;
            y = UnitHeight - y - 1;//GLの座標系だとY軸が逆
            x = x >= UnitWidth ? UnitWidth - 1 : (x < 0 ? 0 : x);
            y = y >= UnitHeight ? UnitHeight - 1 : (y < 0 ? 0 : y);
            Console.WriteLine($"{x},{y}");
            int index = x * UnitHeight + y;
            selection[index] ^= true;
            Renderer.Selections[index] ^= true;
            Console.WriteLine(GetSelection().Aggregate(new StringBuilder(), (sb, i)=>sb.Append(i).Append(","), sb=>sb.ToString()));
            if(OnItemClick != null)
            {
                OnItemClick(index, selection[index]);
            }
        }

        public IEnumerable<int> GetSelection()
        {
            return selection.Select((a, i) => new { a, i }).Where(a => a.a).Select(a => a.i);
        }

        public void SetSelection(IEnumerable<int> select)
        {
            for (int i = 0; i < selection.Length; i++)
            {
                selection[i] = select.Contains(i);
                Renderer.Selections[i] = selection[i];
            }
        }
    }
}
