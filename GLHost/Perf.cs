﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using GeneticReaction.Expressions;
using OpenTK.Graphics.OpenGL;

namespace GLHost
{
    class Perf
    {
        public Perf()
        {
            int Num = 36;
            int Steps = 1000;
            var rand = new Random(0);
            var renders = new GeneticRenderer[Num];
            for (int i = 0; i < renders.Length; i++)
            {
                renders[i] = new GeneticRenderer(64, 64);
                renders[i].LoadContent();
                renders[i].SetGene(GLGene.Adam(rand));
            }
            var watch = new Stopwatch();
            watch.Start();
            foreach (var item in renders)
            {
                for (int i = 0; i < Steps; i++)
                {
                    item.Draw();
                }
                GL.Finish();
            }
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds);
            foreach (var item in renders)
            {
                item.Dispose();
            }
        }
    }
}
