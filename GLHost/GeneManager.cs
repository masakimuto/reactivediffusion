﻿using GeneticReaction.Expressions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GLHost
{
    public class GeneManager
    {
        TileRenderer tile;
        GLRenderer control;

        Random rand;
        public ExtentGene[] Genes { get; private set; }

        int Generation { get; set; }

        Action<string> statusUpdate;

        static SampleExpressionType AdamType
        {
            get { return (SampleExpressionType)Config.Instance.AdamType; }
        }

        public GeneManager(TileRenderer tile, GLRenderer control, Action<string> status)
        {
            rand = new Random();
            statusUpdate = status != null ? status : (s)=> { };
            this.tile = tile;
            this.control = control;
            Genes = new ExtentGene[control.UnitWidth * control.UnitHeight];
            oldGenes = new ExtentGene[Genes.Length];
            //Adam();
            AdamFromPool();
            tile.SetGenes(Genes);
        }

        void Adam()
        {
            var t = (int)DateTime.Now.Ticks;
            Parallel.For(0, Genes.Length, i =>
            //for (var i = 0; i < genes.Length; i++)
            {
                var r = new Random(i + t);
                Genes[i] = ExtentGene.Adam(r, AdamType);
                //genes[i] = CreateValid(() => Gene.Adam(r));
                //}
            });
            Generation = 0;
            statusUpdate("init by random");
        }

        void AdamFromPool()
        {
            try
            {
                var files = System.IO.Directory.EnumerateFiles(Config.Instance.InitialPath, "*.egene").ToArray();
                if(files.Length == 0)
                {
                    Adam();
                    return;
                }
                FromFiles(files, (int)(Config.Instance.InitialRandom * Genes.Length));
                statusUpdate("init pool from " + System.IO.Path.GetFullPath(Config.Instance.InitialPath));
            }
            catch
            {
                Adam();
            }
        }

        public void Evolve(IEnumerable<int> selected)
        {
            selected = selected.ToArray();
            Backup();
            if (!selected.Any())
            {
                AdamFromPool();
            }
            else
            {
                CloneAndCrossOver(selected.ToArray());
                if (selected.Count() == 1)
                {
                    statusUpdate("mutate from " + selected.ElementAt(0));
                }
                else
                {
                    statusUpdate("crossover and mutate");
                }
            }
            Generation++;

            tile.SetGenes(Genes);
        }

        ExtentGene[] oldGenes;

        void Backup()
        {
            for (int i = 0; i < oldGenes.Length; i++)
            {
                oldGenes[i] = Genes[i].Clone(null);
            }
        }

        public void Undo()
        {
            if(!CanUndo)
            {
                return;
            }
            for (int i = 0; i < oldGenes.Length; i++)
            {
                Genes[i] = oldGenes[i];
                oldGenes[i] = null;
            }
            Generation--;
            tile.SetGenes(Genes);
        }

        public bool CanUndo
        {
            get { return oldGenes[0] != null; }
        }

        ExtentGene CreateValid(Func<ExtentGene> f)
        {
            return f();
        }

        
        void CloneAndCrossOver(int[] parents, int num = 0)
        {
            var buf = CreateNext(num == 0 ? Genes.Length - parents.Length : num, r =>
            {
                if (parents.Length == 1/* || r.NextDouble() < .5*/)
                {
                    return CreateValid(() => Genes[parents[r.Next(parents.Length)]].Mutate(r));
                }else
                {
                    return CreateValid(() =>
                    {
                        var pair = SelectPair(parents, rand);
                        var c = Genes[pair.Item1].Crossover(Genes[pair.Item2], rand);
                        if (rand.NextDouble() < .2/*Config.MutateRate*/)
                        {
                            c = c.Mutate(rand);
                        }
                        return c;
                    });
                }
            });
            int j = 0;
            var randAdam = new Random();
            var randGenes = Enumerable.Range(0, Math.Max(0, Genes.Length - buf.Length - parents.Length))
                .Select(x => ExtentGene.Adam(randAdam, AdamType)).ToArray();
            for (int i = 0; i < Genes.Length; i++)
            {
                if (parents.Contains(i))
                {
                    continue;
                }
                if(j < buf.Length)
                {
                    Genes[i] = buf[j];
                }
                else
                {
                    Genes[i] = randGenes[j - buf.Length];
                }
                j++;
            }
        }

        ExtentGene[] CreateNext(int num, Func<Random, ExtentGene> fun)
        {
            if (num <= 0)
            {
                return new ExtentGene[0];
            }
            var buf = new ExtentGene[num];
            var t = (int)DateTime.Now.Ticks;
            Parallel.For(0, num, i =>
            {
                buf[i] = fun(new Random(t + i));
            });
            return buf;
        }

        Tuple<int, int> SelectPair(int[] parents, Random r)
        {
            int i1 = r.Next(parents.Length);
            int i2 = r.Next(parents.Length - 1);
            if(i2 >= i1)
            {
                i2 += 1;
            }
            return Tuple.Create(parents[i1], parents[i2]);
        }

        public void FromFiles(string[] files, int randomCount)
        {
            var t = (int)DateTime.Now.Ticks;
            for (int i = 0; i < files.Length; i++)
            {
                if (i >= Genes.Length)
                {
                    break;
                }
                Genes[i] = ExtentGene.Deserialize(files[i], new Random(t + i));
            }
            randomCount = randomCount < 0 ? 0 : (randomCount >= Genes.Length ? Genes.Length : randomCount);
            int n = Genes.Length - randomCount;
            CloneAndCrossOver(Enumerable.Range(0, files.Length).ToArray(), n);
            Generation = 0;
            tile.SetGenes(Genes);

        }
    }
}
