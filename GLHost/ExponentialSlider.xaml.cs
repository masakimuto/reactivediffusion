﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GLHost
{
    /// <summary>
    /// Interaction logic for ExponentialSlider.xaml
    /// </summary>
    public partial class ExponentialSlider : Slider
    {
        public static readonly DependencyProperty RootProperty = DependencyProperty.Register("Root", typeof(double), typeof(ExponentialSlider));
        //public static readonly DependencyProperty PowerValueProperty = DependencyProperty.Register("PowerValue", typeof(double), typeof(ExponentialSlider));


        public double Root
        {
            get { return (double)GetValue(RootProperty); }
            set { SetValue(RootProperty, value); }
        }

        public double PowerValue
        {
            get { return Math.Pow(Root, Value); }
            set
            {
                if(value <= 0)
                {
                    throw new ArgumentOutOfRangeException("PowerValue");
                }
                Value = Math.Log(value, Root);
            }
        }

        public ExponentialSlider()
        {
            InitializeComponent();
        }

        public double GetPowerValue(double value)
        {
            return Math.Pow(Root, Value);
        }
    }
}
