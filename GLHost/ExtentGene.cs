﻿using GeneticReaction.Expressions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Color = System.Windows.Media.Color;
using Colors = System.Windows.Media.Colors;
using System.IO;

namespace GLHost
{
    public class ExtentGene
    {
        public GLGene Gene;
        public float ValueMax, ValueMin;
        public float Frequency;
        public float Dt;
        public float Anisotoropy, Angle;
        public Color BgColor, ActColor, InhColor;

        float[] colorBuffer = new float[4];

        float ByteToFloat(byte val)
        {
            return val / 255.0f;
        }

        void SetColorBuffer(Color c)
        {
            colorBuffer[0] = ByteToFloat(c.R);
            colorBuffer[1] = ByteToFloat(c.G);
            colorBuffer[2] = ByteToFloat(c.B);
            colorBuffer[3] = ByteToFloat(c.A);
        }

        public float[] BgColorVector
        {
            get
            {
                SetColorBuffer(BgColor);
                return colorBuffer;
            }
        }

        public float[] ActColorVector
        {
            get
            {
                SetColorBuffer(ActColor);
                return colorBuffer;
            }
        }

        public float[] InhColorVector
        {
            get
            {
                SetColorBuffer(InhColor);
                return colorBuffer;
            }
        }

        public ExtentGene(GLGene gene)
        {
            Gene = gene;
            ValueMax = 1;
            ValueMin = (float)Math.Pow(2.0, -5);
            Frequency = 1;
            Dt = 0.5f;
            Anisotoropy = 0.5f;
            Angle = 0;
            BgColor = Colors.Black;
            ActColor = Colors.Red;
            InhColor = Colors.Green;
        }

        public ExtentGene Clone(GLGene g)
        {
            var val = new ExtentGene(g == null ? Gene.Clone() : g)
            {
                ValueMax = this.ValueMax,
                ValueMin = this.ValueMin,
                Frequency = this.Frequency,
                Dt = this.Dt,
                Anisotoropy = this.Anisotoropy,
                Angle = this.Angle,
                BgColor = this.BgColor,
                ActColor = this.ActColor,
                InhColor = this.InhColor
            };
            return val;
        }

        public ExtentGene Mutate(Random rand)
        {
            return Clone(Gene.Mutate(rand));
        }

        public ExtentGene Crossover(ExtentGene o, Random rand)
        {
            return Clone(Gene.Crossover(o.Gene, rand));
        }

        public static ExtentGene Adam(Random rand, SampleExpressionType type)
        {
            return new ExtentGene(GLGene.Adam(rand, type));
        }

        public void Serialize(string fileName)
        {
            Gene.Serialize(fileName);
            using(var file = File.Open(fileName, FileMode.Append, FileAccess.Write))
            {
                BinaryWriter writer = new BinaryWriter(file);
                writer.Write(ValueMax);
                writer.Write(ValueMin);
                writer.Write(Frequency);
                writer.Write(Dt);
                writer.Write(Anisotoropy);
                writer.Write(Angle);
                writer.Write(BgColor);
                writer.Write(ActColor);
                writer.Write(InhColor);
            }
        }

        public static ExtentGene Deserialize(string fileName, Random rand)
        {
            var g = GLGene.Deserialize(fileName, rand);
            var val = new ExtentGene(g);
            using (var file = File.Open(fileName, FileMode.Open, FileAccess.Read))
            {
                var reader = new BinaryReader(file);
                file.Seek(0, SeekOrigin.End);
                val.InhColor = reader.ReadBackColor();
                val.ActColor = reader.ReadBackColor();
                val.BgColor = reader.ReadBackColor();
                val.Angle = reader.ReadBackFloat();
                val.Anisotoropy = reader.ReadBackFloat();
                val.Dt = reader.ReadBackFloat();
                val.Frequency = reader.ReadBackFloat();
                val.ValueMin = reader.ReadBackFloat();
                val.ValueMax = reader.ReadBackFloat();
            }
            return val;
        }

        public override string ToString()
        {
            return Gene != null ? Gene.ToString() : "";
        }
    }



    static class WriterExtension
    {
        internal static void Write(this BinaryWriter writer, Color value)
        {
            writer.Write(value.R);
            writer.Write(value.G);
            writer.Write(value.B);
            writer.Write(value.A);
        }

        internal static float ReadBackFloat(this BinaryReader reader)
        {
            reader.BaseStream.Seek(-sizeof(float), SeekOrigin.Current);
            var val = reader.ReadSingle();
            reader.BaseStream.Seek(-sizeof(float), SeekOrigin.Current);
            return val;   
        }

        internal static Color ReadBackColor(this BinaryReader reader)
        {
            reader.BaseStream.Seek(-sizeof(byte) * 4, SeekOrigin.Current);
            var val = new Color {
                R = reader.ReadByte(),
                G = reader.ReadByte(),
                B = reader.ReadByte(),
                A = reader.ReadByte() };
            reader.BaseStream.Seek(-sizeof(byte) * 4, SeekOrigin.Current);
            return val;
        }
    }
}
