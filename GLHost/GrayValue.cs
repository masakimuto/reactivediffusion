﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GLHost
{
    public struct GrayValue
    {
        public static readonly double Range = 1.0;
        public static readonly int UseBitNumber = 16;
        public static readonly double Scale = (1 << UseBitNumber) / Range;

        UInt32 bits;
        public UInt32 Bits
        {
            get
            {
                return bits;
            }
            set
            {
                bits = value & BitMask;
            }
        }
        static readonly UInt32 BitMask = UInt32.MaxValue >> (32 - UseBitNumber);

        public double Value
        {
            get
            {
                return GrayToBinary(Bits) / Scale;
            }
            set
            {
                if(value < 0)
                {
                    throw new ArgumentOutOfRangeException(nameof(value), "value must not be smaller than 0");
                }
                Bits = BinaryToGray((UInt32)(value * Scale));
            }
        }

        public GrayValue(double value)
        {
            bits = 0;
            Value = value;
        }

        public GrayValue(UInt32 value)
        {
            bits = value & BitMask;
        }

        static UInt32 GrayToBinary(UInt32 val)
        {
            val = val & BitMask;

            val = val ^ (val >> 16);
            val = val ^ (val >> 8);
            val = val ^ (val >> 4);
            val = val ^ (val >> 2);
            val = val ^ (val >> 1);

            return val;
        }

        static UInt32 BinaryToGray(UInt32 val)
        {
            val = val & BitMask;
            return val ^ (val >> 1);
        }

        public override string ToString()
        {
            return Value.ToString();
        }

        public static GrayValue operator +(GrayValue v1, GrayValue v2)
        {
            return new GrayValue(v1.Value + v2.Value);
        }

        public static GrayValue operator -(GrayValue v1, GrayValue v2)
        {
            return new GrayValue(v1.Value - v2.Value);
        }

    }

    public static class GrayExtension
    {
        public static GrayValue NextGray(this Random rand)
        {
            return new GrayValue((UInt32)rand.Next());
        }
    }
}
