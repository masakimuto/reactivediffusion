﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticReaction
{
    public static class Config
    {
        public static double MutateRate = .3;
        public static double MutateRange = .2;
        public static int PoolSize = 36;
        public static int PoolEdge = 6;
        public static int DisplayScale = 2;
        public static int FieldSize = 64;
        public static int ParamCount = 2;
        public static double Dt = 1.0/20;
        public static int MaxNodeLength = 3;

        /// <summary>
        /// Diffusion, Create Rate, Destroy Rate, Constant Create
        /// Activate/Inhibate, Mass, Power of curve
        /// </summary>
        public static double[] ParamScales = new[]
        {
            0.5, 2.0, 1.0, 0.1,
            1.0, 5.0, 5.0,
        };

        static Config()
        {

        }
        

        public static void Load(string fn)
        {
            return;
            var texts = System.IO.File.ReadAllLines(fn);
            MutateRate = double.Parse(texts[0]);
            MutateRange = double.Parse(texts[1]);
            PoolSize = int.Parse(texts[2]);
            PoolEdge = int.Parse(texts[3]);
            DisplayScale = int.Parse(texts[4]);
            FieldSize = int.Parse(texts[5]);
            ParamCount = int.Parse(texts[6]);
            Dt = double.Parse(texts[7]);
        }

        

    }
}
