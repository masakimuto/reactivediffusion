﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace GeneticReaction
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        DispatcherTimer timer;

        Expressions.GeneticManager manager;

        readonly int PoolSize;

        RendererControl[] renderers;
        Grid[] backgrounds;
        bool[] selections;
        GeneSlider[] sliders;

        bool init = false;

        public MainWindow()
        {
            var args = Environment.GetCommandLineArgs();
            if (args.Length >= 2)
            {
                Config.Load(args[1]);
            }

            PoolSize = Config.PoolSize;
            InitializeComponent();
            CreateGrid();
            renderers = new RendererControl[PoolSize];
            backgrounds = new Grid[PoolSize];
            selections = new bool[PoolSize];
            sliders = new GeneSlider[new Gene().Items.Length];
            geneText.Text = new string('\n', Config.ParamCount);
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            if (init)
            {
                return;
            }
            init = true;
            Cursor = Cursors.Wait;
            for (int i = 0; i < PoolSize; i++)
            {
                itemGrid.Children.Add(CreateRenderer(i));
            }
            manager = new Expressions.GeneticManager();
            timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(50);
            timer.Tick += Timer_Tick;
            timer.Start();
            Cursor = null;

        }
        

        void CreateSliders()
        {
            for (int i = 0; i < Config.ParamCount; i++)
            {
                genes.ColumnDefinitions.Add(new ColumnDefinition());
            }
            var panels = new List<StackPanel>();
            for (int i = 0; i < Config.ParamCount; i++)
            {
                var p = new StackPanel()
                {
                    Orientation = Orientation.Vertical,
                    Width = 260,
                };
                genes.Children.Add(p);
                p.SetValue(Grid.ColumnProperty, i);
                panels.Add(p);

            }
            var g = new Gene();
            for (int i = 0; i < sliders.Length; i++)
            {
                var s = new GeneSlider(g.GetMaximum(i));
                (s.FindName("number") as Label).Content = g.IndexToName(i);
                panels[i / g.SeriesLength()].Children.Add(s);
                sliders[i] = s;
            }
        }

        void CreateGrid()
        {
            var e = Config.PoolEdge;
            itemGrid.Width = e * Config.FieldSize * Config.DisplayScale;
            itemGrid.Height = e * Config.FieldSize * Config.DisplayScale;
            for (int i = 0; i < e; i++)
            {
                itemGrid.RowDefinitions.Add(new RowDefinition());
                itemGrid.ColumnDefinitions.Add(new ColumnDefinition());
            }
        }

        UIElement CreateRenderer(int id)
        {
            var form = new SelectionItem();
            form.SetValue(Grid.RowProperty, id % Config.PoolEdge);
            form.SetValue(Grid.ColumnProperty, id / Config.PoolEdge);

            renderers[id] = form.FindName("renderer") as RendererControl;
            renderers[id].Click += SelectItem;
            renderers[id].Tag = id;
            backgrounds[id] = form.FindName("bg") as Grid;
            return form;
        }

        private void SelectItem(object sender, EventArgs e)
        {
            var renderer = sender as RendererControl;
            var id = (int)renderer.Tag;
            selections[id] ^= true;
            backgrounds[id].Background = selections[id] ? Brushes.Yellow : Brushes.Gray;

            geneText.Text = manager.GetRunner(id).gene.ToString();
        }


        private void Timer_Tick(object sender, EventArgs e)
        {
            manager.Step(1);
            UpdateRender();
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            timer.Stop();
        }

        void UpdateRender()
        {
            for (int i = 0; i < PoolSize; i++)
            {
                renderers[i].Draw(manager.GetRunner(i).GetVisual());
                //if (manager.GetRunner(i).IsNan)
                //{
                //    manager.ResetGene(i);
                //}
            }

        }

        void StepClick(object sender, EventArgs e)
        {
            Cursor = Cursors.Wait;
            //new Perf();
            Cursor = Cursors.Arrow;
            //return;


            manager.Step(100);
            UpdateRender();
            status.Content = "step " + manager.StepCount;
        }

        void NextClick(object sender, EventArgs e)
        {
            Cursor = Cursors.Wait;
            var s = selections.Select((x, i) => Tuple.Create(x, i)).Where(x => x.Item1).Select(x => x.Item2).ToArray();
            manager.Update(s);
            manager.Step(1);
            UpdateRender();
            Cursor = Cursors.Arrow;
            status.Content = "generation:" +  manager.Generation;
            //ApplySlider(manager.genes[s.FirstOrDefault()]);
        }

        void UndoClick(object sender, EventArgs e)
        {
            manager.Undo();
            manager.Step(1);
            UpdateRender();
            status.Content = "undo";
        }

        int GetSelected()
        {
            var index = selections.Select((x, i) => new { x, i }).FirstOrDefault(x => x.x)?.i;
            if (!index.HasValue)
            {
                status.Content = "please select save target";
                System.Media.SystemSounds.Beep.Play();
                return -1;
            }
            return index.Value;
        }

        void TuneClick(object sender, EventArgs e)
        {
            for (int i = 0; i < PoolSize; i++)
            {
                manager.GetRunner(i).UpdateStatics();
            }
            UpdateRender();
        }

        void SaveClick(object sender, EventArgs e)
        {
            var index = GetSelected();
            if (index == -1)
            {
                return;
            }
            var dialog = new Microsoft.Win32.SaveFileDialog()
            {
                AddExtension = true,
                DefaultExt = "gen",
                Filter = "gene file|*.gen"
            };
            var res = dialog.ShowDialog();
            if (res.HasValue && res.Value)
            {
                manager.GetRunner(index).gene.Serialize(dialog.FileName);
                //manager.genes[index.Value].Save(dialog.FileName);

                status.Content = $"gene{index} save as {dialog.FileName}";
            }
        }

        void RenderClick(object sender, EventArgs e)
        {
            var index = GetSelected();
            if (index == -1)
            {
                return;
            }
            bool run = false;
            if (timer.IsEnabled)
            {
                timer.Stop();
                run = true;
            }
            var dialog = new Microsoft.Win32.SaveFileDialog()
            {
                AddExtension = true,
                DefaultExt = "png",
                Filter = "image file|*.png"
            };
            var res = dialog.ShowDialog();
            if (res.HasValue && res.Value)
            {
                renderers[index].SaveRender(dialog.FileName);
                status.Content = $"gene{index} render as {dialog.FileName}";
            }
            if (run)
            {
                timer.Start();
            }
        }


        void ToggleClick(object sender, EventArgs e)
        {
            var button = sender as System.Windows.Controls.Primitives.ToggleButton;
            if (button.IsChecked.HasValue && button.IsChecked.Value)
            {
                timer.Start();
            }
            else
            {
                timer.Stop();
            }
        }

        void ShowClick(object sender, EventArgs e)
        {
            status.Content = manager.StepCount;


#if DEBUG
            System.Diagnostics.Debugger.Break();
#endif
        }

        private void Window_PreviewDragOver(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop, true))
            {
                e.Effects = DragDropEffects.Copy;
            }
            else
            {
                e.Effects = DragDropEffects.None;
            }
            e.Handled = true;
        }

        private void Window_Drop(object sender, DragEventArgs e)
        {
            var files = e.Data.GetData(DataFormats.FileDrop) as string[];
            if (files != null)
            {
                status.Content = "load from files";

                manager.FromFiles(files);
                //ApplySlider(manager.genes[0]);
            }
            UpdateRender();
        }

        void ApplySlider(Gene g)
        {
            return;
            for (int i = 0; i < g.Items.Length; i++)
            {
                sliders[i].value.Value = g.Items[i];
            }
        }

        void RestartClick(object sender, RoutedEventArgs e)
        {
            manager.CreateRunner();
            UpdateRender();
            status.Content = "restart";
        }

        void UnselectClick(object sender, RoutedEventArgs e)
        {
            var selected = selections.Select((x, i) => new { x, i }).Where(y => y.x).Select(y => y.i).ToArray();
            foreach (var item in selected)
            {
                SelectItem(renderers[item], e);
            }
        }

       
    }
}
