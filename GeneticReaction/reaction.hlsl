﻿struct VIN {
	float4 pos : SV_Position;
	float2 tex : TEXCOORD0;
};

sampler TextureSampler : register(s0);
float2 PixelSize;
float4 Diffusion;
float4 NoiseScale;

void vert(inout VIN vin) {

}

float4 frag(VIN vin) : COLOR0{
	return tex2D(TextureSampler, vin.tex);
}

float4 fragInit(VIN vin) : COLOR0{
	return float4(0,0,0,0);
}

float4 fragMap(VIN vin) : COLOR0{
	return float4(0,0,0,0);
}

technique t{
	pass p{
		VertexShader = compile vs_4_0 vert();
		PixelShader = compile ps_4_0 frag();
	}

	pass map{
		VertexShader = compile vs_4_0 vert();
		PixelShader = compile ps_4_0 fragMap();
	}
}

technique init {
	pass p {
		VertexShader = compile vs_4_0 vert();
		PixelShader = compile ps_4_0 fragInit();
	}
}
