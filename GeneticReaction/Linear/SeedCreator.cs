﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticReaction
{
    public class SeedCreator
    {

        public SeedCreator()
        {
        }

        public Gene[] Run(int max, int step = 200, double threshold = .2)
        {
            ConcurrentBag<Gene> results = new ConcurrentBag<Gene>();

            Parallel.For(0, 8, i =>
            {
                var rand = new Random(i + DateTime.Now.Second);
                for (int j = 0; j < max; j++)
                {
                    var gene = Gene.Adam(rand);
                    if (Evaluater.Eval(gene, step, threshold))
                    {
                        results.Add(gene);
                    }
                }
            });

            return results.ToArray();
        }
    }
}
