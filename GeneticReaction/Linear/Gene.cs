﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticReaction
{
    public class Gene
    {
        static readonly Random random = new Random();
        readonly int ParamCount;

        //CreateA, AtoA Dir, AtoA Act, AtoA Power, BtoA Inh, BtoA Power, DestroyA, Constant Inc
        //CreateB, AtoB Act, BtoB Inh, DestoryB, Constant Inc
        public double[] Items { get; private set; }

        /// <summary>
        /// Diffusion, Create Rate, Destroy Rate, Constant Create
        /// Activate or Inhibate, Half Mass, Power of Curve
        /// </summary>
        /// <returns></returns>
        public int SeriesLength()
        {
            return 4 + ParamCount * 3;
        }

        public double GetDiffusion(int id) => GetScaledValue(id * SeriesLength());
        public double GetCreationRate(int id) => GetScaledValue(id * SeriesLength() + 1);
        public double GetDestroyRate(int id) => GetScaledValue(id * SeriesLength() + 2);
        public double GetConstantCreate(int id) => GetScaledValue(id * SeriesLength() + 3);


        /// <summary>
        /// true means (from) works to (id) as activater
        /// </summary>
        /// <param name="id"></param>
        /// <param name="from"></param>
        /// <returns></returns>
        public bool GetParamMode(int id, int from) => GetScaledValue(id * SeriesLength() + 4 + from * 3) > .5;

        public double GetHalfMass(int id, int from) => GetScaledValue(id * SeriesLength() + 4 + from * 3 + 1);
        public double GetPowerOfCurve(int id, int from) => GetScaledValue(id * SeriesLength() + 4 + from * 3 + 2);

        public double GetMaximum(int i)
        {
            var j = i % SeriesLength();
            if(j <= 3)
            {
                return Config.ParamScales[j];
            }
            else
            {
                return Config.ParamScales[4 + (j-4) % 3];
            }
        }

        public string IndexToName(int i)
        {
            var j = i % SeriesLength();
            var k = i / SeriesLength();
            var b = new StringBuilder();
            b.Append($"{k}.");
            if (j == 0) b.Append("dif");
            if (j == 1) b.Append("create");
            if (j == 2) b.Append("destory");
            if (j == 3) b.Append("constant");
            if(j >= 4)
            {
                var s = (j - 4) % 3;
                var l = (j - 4) / 3;
                if (s == 0) b.Append("mode");
                if (s == 1) b.Append("half");
                if (s == 2) b.Append("power");
                b.Append($" from {l}");

            }
            return b.ToString();
        }

        double GetScaledValue(int i)
        {
            return Items[i] * GetMaximum(i);
        }


        public static Gene Adam()
        {
            return Adam(random);
        }

        public static Gene Adam(Random rand)
        {
            var g = new Gene();
            for (int i = 0; i < g.Items.Length; i++)
            {
                g.Items[i] = rand.NextDouble();
            }
            return g;
        }

        public Gene()
        {
            ParamCount = Config.ParamCount;
            Items = new double[ParamCount * SeriesLength()];
        }

        public Gene Clone()
        {
            var g = new Gene();
            for (int i = 0; i < Items.Length; i++)
            {
                g.Items[i] = Items[i];
            }
            return g;
        }

        public Gene Mutate()
        {
            var g = Clone();
            var target = random.Next(0, g.Items.Length);
            g.Mutate(target);
            for (int i = 0; i < g.Items.Length; i++)
            {
                if(random.NextDouble() < Config.MutateRate)
                {
                    Mutate(i);
                }

            }
          
            return g;
        }

        void Mutate(int target)
        {
            var size = (random.NextDouble() - .5) * 2 * Config.MutateRange;
            if (Items[target] == 0)
            {
                Items[target] += Math.Abs(size);
            }
            else if (Items[target] == 1)
            {
                Items[target] -= Math.Abs(size);
            }
            else
            {
                Items[target] = Math.Max(0.0, Math.Min(1.0, Items[target] + size));
            }
        }

        public Gene CrossOver(Gene another)
        {
            var p = random.Next(1, Items.Length - 1);
            var g = Clone();
            for (int i = 0; i < g.Items.Length; i++)
            {
                g.Items[i] = (i < p) ? Items[i] : another.Items[i];
            }
            return g;
        }

        public void Save(string fileName)
        {
            Directory.CreateDirectory(Path.GetDirectoryName(fileName));
            using (var file = File.Open(fileName, FileMode.Create, FileAccess.Write))
            {
                foreach (var item in Items)
                {
                    file.Write(BitConverter.GetBytes(item), 0, sizeof(double));
                }
            }
        }

        public static Gene Load(string fileName)
        {
            Gene g = new Gene();
            try
            {
                using (var file = File.OpenRead(fileName))
                {
                    var buffer = new byte[sizeof(double)];
                    for (int i = 0; i < g.Items.Length; i++)
                    {
                        file.Read(buffer, 0, buffer.Length);
                        g.Items[i] = BitConverter.ToDouble(buffer, 0);
                    }

                }
                return g;
            }
            catch(Exception e)
            {
                Console.WriteLine(e.ToString());
                Console.WriteLine($"invalid gene file {fileName}");
                return null;
            }
        }

    }
}
