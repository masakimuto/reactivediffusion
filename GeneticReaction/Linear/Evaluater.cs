﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticReaction
{
    public class Evaluater
    {
        public static bool Eval(Gene gene, int step, double threshold)
        {
            var runner = new ReactionRunner(gene);
            bool result = false;
            for (int j = 0; j < 5; j++)
            {
                for (int i = 0; i < step; i++)
                {
                    runner.Update();
                }
                result = false;
                var c = runner.CellToColor(runner.Cells[0]);
                foreach (var item in runner.Cells)
                {
                    if (EvalDiff(c, runner.CellToColor(item), threshold))
                    {
                        result = true;
                    }
                }
                if (!result)
                {
                    return false;
                }
            }
            return true;
        }

        static bool EvalDiff(Color c1, Color c2, double threshold)
        {
            var v = c1.ToVector3() - c2.ToVector3();
            return Math.Abs(v.X) > threshold || Math.Abs(v.Y) > threshold || Math.Abs(v.Z) > threshold;
        }

    }
}
