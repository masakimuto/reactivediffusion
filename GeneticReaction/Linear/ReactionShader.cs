﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace GeneticReaction
{
    public class ReactionShader : IDisposable
    {
        Effect effect;
        readonly GraphicsDevice device;
        RenderTarget2D[] buffers;
        int current = 0;
        int last;

        VertexBuffer vertex;
        IndexBuffer index;

        public ReactionShader(GraphicsDevice device)
        {
            this.device = device;
            effect = new Effect(device, System.IO.File.ReadAllBytes("reaction.bin"));
            buffers = new[] { CreateBuffer(), CreateBuffer() };
            vertex = new VertexBuffer(device, typeof(VertexPositionTexture), 4, BufferUsage.WriteOnly);
            index = new IndexBuffer(device, IndexElementSize.SixteenBits, 6, BufferUsage.WriteOnly);
            vertex.SetData(new[]
            {
                new VertexPositionTexture(new Vector3(-1, 1, 0), Vector2.Zero),
                new VertexPositionTexture(new Vector3(1, 1, 0), Vector2.UnitX),
                new VertexPositionTexture(new Vector3(1, -1, 0), Vector2.One),
                new VertexPositionTexture(new Vector3(-1, -1, 0), Vector2.UnitY)
            });
            index.SetData(new short[] { 0, 1, 2, 0, 2, 3 });

            last = 1;
            current = 0;
            SetParams();
            InitNoise();
            last = current;
            current = 1 - current;
        }

        void InitNoise()
        {
            SetStates();
            //render to buffer[last]
            effect.CurrentTechnique = effect.Techniques[1];
            effect.CurrentTechnique.Passes[0].Apply();
            device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, 4, 0, 2);
        }

        void SetParams()
        {
            effect.Parameters["PixelSize"].SetValue(new Vector2(1f / Config.FieldSize));
            effect.Parameters["Diffusion"].SetValue(new[] { 1f / 256, 1f / 8f });
        }

        RenderTarget2D CreateBuffer()
        {
            return new RenderTarget2D(device, Config.FieldSize, Config.FieldSize, false, SurfaceFormat.Vector4, DepthFormat.None, 0, RenderTargetUsage.PreserveContents);
        }

        void SetStates()
        {
            device.RasterizerState = RasterizerState.CullNone;
            device.SamplerStates[0] = SamplerState.PointWrap;
            device.SetRenderTarget(buffers[last]);
            device.SetVertexBuffer(vertex);
            device.Indices = index;

        }

        public void Update()
        {
            device.Textures[0] = buffers[current];
            SetStates();
            
            effect.CurrentTechnique.Passes[0].Apply();
            
            device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, 4, 0, 2);

            last = current;
            current = 1 - current;
            
        }

        public void Dispose()
        {
            effect.Dispose();
            buffers[0].Dispose();
            buffers[1].Dispose();
            vertex.Dispose();
            index.Dispose();
        }
    }
}
