﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GeneticReaction
{
    /// <summary>
    /// GeneSlider.xaml の相互作用ロジック
    /// </summary>
    public partial class GeneSlider : UserControl
    {
        double MaxValue { get; set; }

        public GeneSlider(double maxValue)
        {
            MaxValue = maxValue;
            InitializeComponent();
        }

        private void value_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            scaled.Content = (e.NewValue * MaxValue).ToString("0.0000");
        }
    }

    class ValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
