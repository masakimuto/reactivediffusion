﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticReaction
{
    public class ReactionRunner
    {
        public readonly Cell[] Cells;
        readonly int Size;
        readonly int Size2;
        readonly Gene gene;

        double[][] buffer;


        public ReactionRunner(Gene gene)
        {
            this.gene = gene;
            Size = Config.FieldSize;
            Size2 = Size * Size;
            Cells = new Cell[Size2];
            for (int i = 0; i < Cells.Length; i++)
            {
                Cells[i] = new Cell();
            }
            colors = new Color[Size2];
            buffer = new double[Config.ParamCount][];
            for (int i = 0; i < buffer.Length; i++)
            {
                buffer[i] = new double[Size2];
            }

            InitNoise();
            //cells[99].items[1] = 100;
        }

        void InitNoise()
        {
            var d = .1;
            var rand = new Random();
            for (int i = 0; i < Size2; i++)
            {
                for (int j = 0; j < Config.ParamCount; j++)
                {
                    Cells[i].items[j] = rand.NextDouble() * d;
                }
            }
        }

        public void Update()
        {
            Diffusion();
            Reaction();
        }

        

        int PointToIndex(int x, int y)
        {
            if(x < 0)
            {
                x += Size;
            }
            if(x >= Size)
            {
                x -= Size;
            }
            if(y < 0)
            {
                y += Size;
            }
            if(y >= Size)
            {
                y -= Size;
            }
            return x + y * Size;
        }

        Point IndexToPoint(int i)
        {
            if(i < 0)
            {
                i += Size2;
            }
            if(i >= Size2)
            {
                i -= Size2;
            }
            return new Point(i % Size, i / Size);
        }

        static readonly Point[] Around = new[]
        {
            new Point(-1, -1),
            new Point(-1, 0),
            new Point(-1, 1),
            new Point(1, 0),
            new Point(1, -1),
            new Point(1, 1),
            new Point(0, 1),
            new Point(0, -1)
        };

        void Reaction()
        {
            for (int i = 0; i < Size2; i++)
            {
                for (int j = 0; j < Config.ParamCount; j++)
                {
                    buffer[j][i] = CalcReaction(Cells[i], j);
                }
            }

            for (int i = 0; i < Config.ParamCount; i++)
            {
                for (int j = 0; j < Size2; j++)
                {
                    Cells[j].items[i] += buffer[i][j] * Config.Dt;
                }
            }
        }

        double CalcReaction(Cell cell, int j)
        {
            var k = 1.0;
            for (int i = 0; i < Config.ParamCount; i++)
            {
                k *= Sigmoid(cell, j, i, gene.GetParamMode(j, i));
            }

            return gene.GetCreationRate(j) * k
                - gene.GetDestroyRate(j) * cell.items[j]
                + gene.GetConstantCreate(j);
        }

        double Sigmoid(Cell cell, int j, int i, bool active)
        {
            var o = j * 7;
            var i0 = Math.Pow(cell.items[i], gene.GetPowerOfCurve(j, i));
            var k0 = Math.Pow(gene.GetHalfMass(j, i), gene.GetPowerOfCurve(j, i));
            return active
                ? (i0 / (i0 + k0))//act
                : (k0 / (k0 + i0));//inh
        }


        void Diffusion()
        {
            for (int i = 0; i < Config.ParamCount; i++)
            {
                for (int j = 0; j < Size2; j++)
                {
                    buffer[i][j] = Cells[j].items[i] * -1 * Around.Length;
                    var p = IndexToPoint(j);
                    for (int k = 0; k < Around.Length; k++)
                    {
                        buffer[i][j] += Cells[PointToIndex(p.X + Around[k].X, p.Y + Around[k].Y)].items[i];
                    }
                }
            }

            for (int i = 0; i < Config.ParamCount; i++)
            {
                for (int j = 0; j < Size2; j++)
                {
                    Cells[j].items[i] += buffer[i][j] * gene.GetDiffusion(i) / Around.Length * Config.Dt;
                }
            }
        }

        Color[] colors;

        public Color CellToColor(Cell cell)
        {
            //return new Color((float)cell.items[0], 0, (float)cell.items[1]);
            return new Color(
                (float)Sigmoid(cell, 0, 0, gene.GetParamMode(0, 0)),
                0,//(float)Sigmoid(cell, 2, 2, gene.GetParamMode(2, 2)),
                (float)Sigmoid(cell, 1, 1, gene.GetParamMode(1, 1)));
        }

        public Color[] GetVisual()
        {
            for (int i = 0; i < colors.Length; i++)
            {
                colors[i] = CellToColor(Cells[i]);
            }
            return colors;
        }
    }
}
