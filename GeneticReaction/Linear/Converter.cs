﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticReaction
{
    public class Converter
    {
        public static Gene Convert(double[] old)
        {
            var g = new Gene();

            var items = g.Items;

            items[0] = 1.0 / 512;
            items[1] = old[0];
            items[2] = old[5];
            items[3] = old[6];

            items[4] = 1;
            items[5] = old[1];
            items[6] = old[2];

            items[7] = 0;
            items[8] = old[3];
            items[9] = old[4];

            items[10] = 1.0 / 8;
            items[11] = old[7];
            items[12] = old[12];
            items[13] = old[13];

            items[14] = 1;
            items[15] = old[8];
            items[16] = old[9];

            items[17] = 0;
            items[18] = old[10];
            items[19] = old[11];

            for (int i = 0; i < items.Length; i++)
            {
                items[i] /= g.GetMaximum(i);
            }

            return g;
        }
    }
}
