﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticReaction
{
    public class GeneticManager
    {
        readonly int PoolSize;
        ReactionRunner[] runners;
        public Gene[] genes;
        public int Generation { get; private set; }
        public int StepCount { get; private set; }
        Random random = new Random();
        Gene[] lastGenes;

        public GeneticManager()
        {
            PoolSize = Config.PoolSize;
            runners = new ReactionRunner[PoolSize];
            genes = new Gene[PoolSize];
            lastGenes = new Gene[PoolSize];
            Adam();
            CreateRunner();
        }

        public void FromFiles(IEnumerable<string> files)
        {
            var g = files.Select(x => Gene.Load(x)).Where(x => x != null).Take(PoolSize).ToArray();
            for (int i = 0; i < g.Length; i++)
            {
                genes[i] = g[i];
            }
            CreateRunner();
            Generation = 0;
        }

        public void FromGene(Gene gene)
        {
            genes[0] = gene;
            for (int i = 1; i < PoolSize; i++)
            {
                genes[i] = gene.Mutate();
            }
            CreateRunner();
            Generation = 0;
        }

        void CreateRunner()
        {
            for (int i = 0; i < runners.Length; i++)
            {
                runners[i] = new ReactionRunner(genes[i]);
            }
            StepCount = 0;
        }

        public void Step(int n = 1)
        {
            Parallel.ForEach(runners, x =>
            {
                for (int t = 0; t < n; t++)
                {
                    x.Update();
                }
            });
            StepCount += n;
        }

        public void Update(IEnumerable<int> selected)
        {
            selected = selected.ToArray();
            
            Backup();
            if (!selected.Any())
            {
                Adam();
            }
            else if (selected.Count() == 1)
            {
                CloneAndMutate(selected.ElementAt(0));
            }
            else
            {
                CrossOver(selected.ToArray());
            }

            CreateRunner();
            Generation++;
            StepCount = 0;
        }

        void Backup()
        {
            for (int i = 0; i < PoolSize; i++)
            {
                lastGenes[i] = genes[i].Clone();
            }
        }
    
        void Adam()
        {
            for (int i = 0; i < PoolSize; i++)
            {
                genes[i] = Gene.Adam();
            }
        }

        public void Undo()
        {
            if(Generation == 0)
            {
                return;
            }
            Generation--;
            StepCount = 0;
            for (int i = 0; i < PoolSize; i++)
            {
                genes[i] = lastGenes[i].Clone();
            }
            CreateRunner();
        }

        void CloneAndMutate(int root)
        {
            for (int i = 0; i < PoolSize; i++)
            {
                if (root == i) continue;
                genes[i] = genes[root].Mutate();
            }
        }

        void CrossOver(int[] parents)
        {
            for (int i = 0; i < PoolSize; i++)
            {
                if (parents.Contains(i)) continue;
                var pair = SelectPair(parents);
                var c = genes[pair.Item1].CrossOver(genes[pair.Item2]);
                if(random.NextDouble() < Config.MutateRate)
                {
                    c = c.Mutate();
                }
                genes[i] = c;
            }
        }
       
        Tuple<int, int> SelectPair(int[] parents)
        {
            var p1 = parents[random.Next(parents.Length)];
            for (int i = 0; i < 100; i++)
            {
                var p2 = parents[random.Next(parents.Length)];
                if(p1 != p2)
                {
                    return Tuple.Create(p1, p2);
                }
            }
            return Tuple.Create(p1, p1);
        }

        public ReactionRunner GetRunner(int i)
        {
            return runners[i];
        }
    }
}
