﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticReaction.Expressions
{
    public class GeneticManager
    {
        Gene[] genes, old;
        ReactionRunner[] runners;

        public int StepCount
        {
            get; private set;
        }
        Random rand;
        public int Generation { get; private set; }

        public GeneticManager()
        {
            rand = new Random();
            genes = new Gene[Config.PoolSize];
            old = new Gene[Config.PoolSize];
            runners = new ReactionRunner[Config.PoolSize];
            Adam();
            CreateRunner();
        }

        Gene CreateValid(Func<Gene> f)
        {
            Gene g = null;
            for (int i = 0; i < 10; i++)
            {
                g = f();
                var runner = new ReactionRunner(g);
                for (int t = 0; t < 10; t++)
                {
                    runner.Update();
                }
                if (!runner.IsNan)
                {
                    return g;
                }
            }
            return g;
        }

        void Adam()
        {
            var t = (int)DateTime.Now.Ticks;
            Parallel.For(0, genes.Length, i =>
            //for (var i = 0; i < genes.Length; i++)
            {
                var r = new Random(i + t);
                genes[i] = CreateValid(() => Gene.Adam(r));
            //}
            });
            Generation = 0;
        }

        public void ResetGene(int j)
        {
            genes[j] = CreateValid(() => Gene.Adam(rand));
            runners[j] = new ReactionRunner(genes[j]);
        }

        public void CreateRunner()
        {
            for (int i = 0; i < runners.Length; i++)
            {
                runners[i] = new ReactionRunner(genes[i]);
            }
            StepCount = 0;
        }

        public void Step(int n = 1)
        {
            Parallel.ForEach(runners, x =>
            {
                for (int i = 0; i < n; i++)
                {
                    x.Update();
                }
            });
            StepCount += n;
        }

        public void Update(IEnumerable<int> selected)
        {
            selected = selected.ToArray();

            Backup();
            if (!selected.Any())
            {
                Adam();
            }
            else if (selected.Count() == 1)
            {
                CloneAndMutate(selected.ElementAt(0));
            }
            else
            {
                CrossOver(selected.ToArray());
            }

            CreateRunner();
            Generation++;
            StepCount = 0;
        }

        public void Undo()
        {
            if(old == null || old[0] == null)
            {
                return;
            }
            for (int i = 0; i < old.Length; i++)
            {
                genes[i] = old[i].Clone();
            }
            CreateRunner();
            Generation--;
            StepCount = 0;
        }

        void Backup()
        {
            for (int i = 0; i < old.Length; i++)
            {
                old[i] = genes[i].Clone();
            }
        }

        void CloneAndMutate(int selected)
        {
            var buf = new Gene[genes.Length - 1];
            var t = (int)DateTime.Now.Ticks;
            Parallel.For(0, buf.Length - 1, i =>
            {
                var rand = new Random(t + i);
                buf[i] = CreateValid(() => genes[selected].Mutate(rand));
            });
            buf[buf.Length - 1] = CreateValid(() => Gene.Adam(rand));
            int j = 0;
            for (int i = 0; i < genes.Length; i++)
            {
                if(i == selected)
                {
                    continue;
                }
                genes[i] = buf[j];
                j++;
            }
        }

        void CrossOver(int[] parents)
        {
            var buf = new Gene[genes.Length - parents.Length];
            var t = (int)DateTime.Now.Ticks;
            Parallel.For(0, buf.Length, i =>
            {
                var rand = new Random(t + i);
                buf[i] = CreateValid(() =>
                {
                    var pair = SelectPair(parents, rand);
                    var c = genes[pair.Item1].Crossover(genes[pair.Item2], rand);
                    if(rand.NextDouble() < Config.MutateRate)
                    {
                        c = c.Mutate(rand);
                    }
                    return c;
                });
            });
            int j = 0;
            for (int i = 0; i < genes.Length; i++)
            {
                if (parents.Contains(i))
                {
                    continue;
                }
                genes[i] = buf[j];
                j++;
            }
        }

        Tuple<int, int> SelectPair(int[] parents, Random r)
        {
            var p1 = parents[r.Next(parents.Length)];
            for (int i = 0; i < 100; i++)
            {
                var p2 = parents[r.Next(parents.Length)];
                if (p1 != p2)
                {
                    return Tuple.Create(p1, p2);
                }
            }
            return Tuple.Create(p1, p1);
        }

        public ReactionRunner GetRunner(int i)
        {
            return runners[i];
        }

        public void FromFiles(string[] files)
        {
            int i = 0;
            var t = (int)DateTime.Now.Ticks;
            foreach (var file in files)
            {
                genes[i] = Gene.Deserialize(file, new Random(t + i));
                i++;
                if(i >= genes.Length)
                {
                    break;
                }
            }
            if(i <= 1)
            {
                CloneAndMutate(0);
            }
            else
            {
                CrossOver(Enumerable.Range(0, i).ToArray());
            }
            Generation = 0;
            CreateRunner();
        }

    }
}
