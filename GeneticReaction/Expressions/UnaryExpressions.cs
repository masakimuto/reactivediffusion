﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Tree = System.Linq.Expressions;

namespace GeneticReaction.Expressions
{
    [Serializable]
    public class UnaryPowerExpression : UnaryExpression
    {
        public override Tree.Expression ToExpressionTree(ExpressionBuilder builder)
        {
            return Tree.Expression.Power(Value.ToExpressionTree(builder), Tree.Expression.Constant(2.0));
        }

        public override string ToString()
        {
            return $"pow({Value}, 2.0)";
          //  return $"({Value}^2)";
        }

        protected override Expression Clone()
        {
            return new UnaryPowerExpression();
        }
    }   

    [Serializable]
    public class UnaryNegativeExpression : UnaryExpression
    {
        public override Tree.Expression ToExpressionTree(ExpressionBuilder builder)
        {
            return Tree.Expression.Negate(Value.ToExpressionTree(builder));
        }

        public override string ToString()
        {
            return $"(-{Value.ToString()})";
        }

        protected override Expression Clone()
        {
            return new UnaryNegativeExpression();
        }
    }
}
