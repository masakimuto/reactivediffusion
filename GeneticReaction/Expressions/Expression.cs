﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tree = System.Linq.Expressions;

using ExpressionType = System.Func<double, double, double>;
using System.Diagnostics;

namespace GeneticReaction.Expressions
{
    

    //constructor->AddChild->Build->Compile
    [Serializable]
    public abstract class Expression
    {
        public abstract Tree.Expression ToExpressionTree(ExpressionBuilder builder);
        public static readonly Type ValueType = typeof(Double);
        public abstract int ChildCount { get; }
        public Expression Parent;
        public List<Expression> children = new List<Expression>();
        public int Level { get; private set; }
        public int BrotherIndex { get; set; }

        Expression AddChild(Expression child)
        {
            Debug.Assert(children.Count < ChildCount);
            children.Add(child);
            child.Parent = this;
            child.BrotherIndex = children.Count - 1;
            return this;
        }

        public Expression AddChildren(params Expression[] children)
        {
            foreach (var item in children)
            {
                AddChild(item);
            }
            return this;
        }

        /// <summary>
        /// this as root
        /// </summary>
        /// <returns></returns>
        public ExpressionBuilder Build()
        {
            var builder = new ExpressionBuilder(this);
            Build(builder);
            return builder;
        }

        public virtual void Build(ExpressionBuilder builder)
        {
            Debug.Assert(children.Count == ChildCount);
            Level = Parent == null ? 0 : Parent.Level + 1;
            builder.Add(this, Level);
            foreach (var item in children)
            {
                item.Build(builder);
            }
        }

        public static Expression CreateRandomTree(Random rand, int level = 0)
        {
            var node = CreateRandomNode(rand);
            if(level >= Config.MaxNodeLength)
            {
                node = CreateRandomValue(rand);
            }
            if(level == 0)
            {
                node = CreateRandomBinary(rand);
            }
            for (int i = 0; i < node.ChildCount; i++)
            {
                node.AddChild(CreateRandomTree(rand, level + 1));
            }
            return node;
        }

        public static Expression CreateRandomNode(Random rand)
        {
            var type = rand.Next(0, 3);
            switch (type)
            {
                case 0:
                    return CreateRandomBinary(rand);
                case 1:
                    return CreateRandomUnary(rand);
                case 2:
                    return CreateRandomValue(rand);
                default:
                    throw new Exception();
            }
        }

        public static BinaryExpression CreateRandomBinary(Random rand)
        {
            var t = rand.Next(0, 4);
            return new BinaryExpression((BinaryExpression.Operator)t);
        }

        public static UnaryExpression CreateRandomUnary(Random rand)
        {
            var t = rand.Next(0, 2);
            if(t == 0)
            {
                return new UnaryNegativeExpression();
            }
            else
            {
                return new UnaryPowerExpression();
            }
        }

        public static ValueExpression CreateRandomValue(Random rand)
        {
            var t = rand.Next(0, Config.ParamCount + 1);
            if(t == Config.ParamCount)
            {
                var c = new ConstantExpression();
                c.Value = rand.NextDouble();
                return c;
            }
            else
            {
                return new ParameterExpression(t);
            }
        }

        public Expression AtParent
        {
            set
            {
                Parent.children[BrotherIndex] = value;
            }
        }

        /// <summary>
        /// 部分木をクローン
        /// </summary>
        /// <returns></returns>
        public Expression CloneTree()
        {
            var c = Clone();
            foreach (var item in children)
            {
                c.AddChild(item.CloneTree());
            }
            return c;
        }

        protected abstract Expression Clone();

    }

    [Serializable]
    public class BinaryExpression : Expression
    {
        public enum Operator
        {
            Add,
            Sub,
            Mul,
            Div,
        }

        public override int ChildCount => 2;

        public Expression Left => children[0];
        public Expression Right => children[1];

        readonly Operator op;

        public BinaryExpression(Operator op)
        {
            this.op = op;
        }

        public BinaryExpression()
            : this(Operator.Add)
        {

        }

        public static BinaryExpression Add() => new BinaryExpression(Operator.Add);
        public static BinaryExpression Sub() => new BinaryExpression(Operator.Sub);
        public static BinaryExpression Mul() => new BinaryExpression(Operator.Mul);
        public static BinaryExpression Div() => new BinaryExpression(Operator.Div);
        public static Expression Add(Expression e1, Expression e2) => Add().AddChildren(e1, e2);
        public static Expression Sub(Expression e1, Expression e2) => Sub().AddChildren(e1, e2);
        public static Expression Mul(Expression e1, Expression e2) => Mul().AddChildren(e1, e2);
        public static Expression Div(Expression e1, Expression e2) => Div().AddChildren(e1, e2);

        static Expression Chain(Expression[] e, Func<Expression, Expression, Expression> f)
        {
            if (e.Length <= 1)
            {
                throw new Exception();
            }
            var cu = f(e[0], e[1]);
            for (int i = 2; i < e.Length; i++)
            {
                cu = f(cu, e[i]);
            }
            return cu;
        }
        public static Expression Add(params Expression[] e) => Chain(e, Add);
        public static Expression Sub(params Expression[] e) => Chain(e, Sub);
        public static Expression Mul(params Expression[] e) => Chain(e, Mul);
        

        public override Tree.Expression ToExpressionTree(ExpressionBuilder builder)
        {
            var left = Left.ToExpressionTree(builder);
            var right = Right.ToExpressionTree(builder);
            switch (op)
            {
                case Operator.Add:
                    return Tree.Expression.Add(left, right);
                case Operator.Sub:
                    return Tree.Expression.Subtract(left, right);
                case Operator.Mul:
                    return Tree.Expression.Multiply(left, right);
                case Operator.Div:
                    return Tree.Expression.Divide(left, right);
                default:
                    throw new ArgumentException();
            }
        }

        protected override Expression Clone()
        {
            return new BinaryExpression(op);
        }

        public override string ToString()
        {
            string o;
            switch (op)
            {
                case Operator.Add:
                    return $"({Left}+{Right})";
                case Operator.Sub:
                    return $"({Left}-{Right})";
                case Operator.Mul:
                    return $"{Left}*{Right}";
                case Operator.Div:
                    return $"{Left}/({Right})";
                default:
                    o = null;
                    break;
            }
            return $"({Left}){o}({Right})";
        }
    }

    [Serializable]
    public abstract class UnaryExpression : Expression
    {
        public override int ChildCount => 1;

        public Expression Value => children[0];

        public static Expression Power(Expression e) => new UnaryPowerExpression().AddChildren(e);
        public static Expression Negative(Expression e) => new UnaryNegativeExpression().AddChildren(e);
    }

    [Serializable]
    public abstract class ValueExpression : Expression
    {
        public override int ChildCount => 0;
    }
}
