﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticReaction.Expressions
{
   

    public class Gene
    {
        public Expression[] root
        {
            get; private set;
        }
        ExpressionBuilder[] builder;
        double[] Constants
        {
            get; set;
        }

        public Random rand
        {
            get; private set;
        }

        public double[] StablePoint;

        static readonly int ConstantPerParam = 4;
        static int DiffusionConstantLength = Config.ParamCount * ConstantPerParam;//diffusion rate, anisotrophic flag, anisotrophic direction, anisotorphic rate
       

        public Gene(Random rand, Expression[] root)
        {
            this.root = root;
            this.rand = rand;
            Build();
        }

        public static Gene Adam(Random rand)
        {
            Gene g;
            //do
            //{
            //    g = new Gene(rand, Enumerable.Range(0, Config.ParamCount).Select(x=>Expression.CreateRandomTree(rand)).ToArray());
            //} while (g.builder.Any(x => !x.IsValid()));
            //g = new Gene(rand, new[] { SampleExpressions.Schnakenberg(0), SampleExpressions.Schnakenberg(1) });
            //g = new Expressions.Gene(rand, new[] { SampleExpressions.GiererMeinhrdt(0), SampleExpressions.GiererMeinhrdt(1) });
            g = new Gene(rand, new[] { SampleExpressions.Turing(0), SampleExpressions.Turing(1) });
            g.RandomizeConstants();
            return g;
        }
        
        
        
        public bool EvalUnstable()
        {
            if(builder.Any(x=>!x.IsValid()))
            {
                return false;
            }
            return StableEval.EvalNan(Compile());
            //StablePoint = StableEval.EvalUnstable(Compile());
            //return StablePoint != null;
        }

        void TestTuring()
        {
            root = new[] {
                SampleExpressions.Turing(0),
                SampleExpressions.Turing(1)
            };
            Build();
            var param = new[]
            {
                0.05, 0.07, 0.003,
                0.08, 0.03, 0
            };
            for (int i = 2; i < param.Length; i++)
            {
                Constants[i] = param[i];
            }
            Constants[0] = 1f / 16f;//diffusion relative to dif[1]
            Constants[1] = 1f / 8f;
            ApplyConstants();
        }

        void Build()
        {
            var dif = new double[DiffusionConstantLength];
            if(Constants != null && Constants.Length >= dif.Length)
            {
                for (int i = 0; i < dif.Length; i++)
                {
                    dif[i] = Constants[i];
                }
            }
            builder = root.Select(x => x.Build()).ToArray();
            for (int i = 0; i < builder.Length; i++)
            {
                builder[i].Random = rand;
            }
            var cons = builder.SelectMany(x => Enumerable.Range(0, x.ConstantCount).Select(i => x.GetConstant(i)));
            Constants = dif.Concat(cons).ToArray();
        }

        void RandomizeConstants()
        {
            for (int i = 0; i < Constants.Length; i++)
            {
                Constants[i] = rand.NextDouble();
            }
            ApplyConstants();
        }
    
        public Func<double[], double>[] Compile()
        {
            //ApplyConstants();
            return builder.Select(x => x.Compile()).ToArray();
        }

        void ApplyConstants()
        {
            int i = 0;
            foreach (var item in builder)
            {
                item.SetConstants(Constants.Skip(DiffusionConstantLength).Skip(i).Take(item.ConstantCount));
                i += item.ConstantCount;
            }
        }

        public double GetDiffusion(int i)
        {
            if(i >= Config.ParamCount)
            {
                return 0.0;
            }
            if (i == 1)
            {
                return 1.0;
            }else
            {
                return Constants[i * ConstantPerParam] * 0.1;
            }
            if(i== 0)
            {
                return Constants[i * ConstantPerParam] * Constants[(i + 1) * ConstantPerParam];//diffusion of activator should be smaller than inhibitor
            }
            return Constants[i * ConstantPerParam];
        }

        public bool IsAnisotropicDiffusion(int i)
        {
            return Constants[i * ConstantPerParam + 1] > 0.5;
        }

        public double GetAnisotorophicRate(int i, double angle)
        {
            var a = Constants[i * ConstantPerParam + 2] * Math.PI * 2;
            var r = Constants[i * ConstantPerParam + 3];
            //return Math.Abs(Math.Cos(a - angle)) * r * 1.41421356 + 1 - r;
            return (Math.Pow(Math.Cos(a - angle), 2) - 0.5) * 2 * r + 1;
        }

        public Gene Clone()
        {
            var g = new Gene(rand, root.Select(x=>x.CloneTree()).ToArray());
            for (int i = 0; i < Constants.Length; i++)
            {
                g.Constants[i] = Constants[i];
            }
            g.ApplyConstants();

            return g;
        }

        public Gene Mutate(Random rnd)
        {
            var g = Clone();
            g.SetRandom(rnd);
            var r = rnd.Next(2);
            if(r == 1)
            {
                g.MutateConstants();
                g.ApplyConstants();
            }
            else
            {
                for (int i = 0; i < builder.Length; i++)
                {
                    g.builder[i].Mutate();
                    g.root[i] = g.builder[i].Root;
                }
                
            }
            g.Build();
            return g;
        }

        void SetRandom(Random rand)
        {
            this.rand = rand;
            foreach (var item in builder)
            {
                item.Random = rand;
            }
        }

        public Gene Crossover(Gene o, Random rnd)
        {
            var g = Clone();
            g.SetRandom(rnd);
            for (int i = 0; i < g.builder.Length; i++)
            {
                g.builder[i].Crossover(o.builder[i]);
                g.root[i] = g.builder[i].Root;
            }
            g.Build();
            return g;
        }

        void MutateConstants()
        {
            MutateValue(rand.Next(Constants.Length));
            for (int i = 0; i < Constants.Length; i++)
            {
                if(rand.NextDouble() < Config.MutateRate)
                {
                    MutateValue(i);
                }
            }
        }

        void MutateValue(int i)
        {
            var d = (rand.NextDouble() - 0.5) * 2 * Config.MutateRange;
            if(Constants[i] <= 0)
            {
                Constants[i] += Math.Abs(d);
            }
            else if (Constants[i] >= 1)
            {
                Constants[i] -= Math.Abs(d);
            }
            else
            {
                Constants[i] += d;
            }
            Constants[i] = Math.Max(0, Math.Min(1, Constants[i]));
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            root.Aggregate(sb, (b, x) => b.AppendLine(x.ToString()));
            sb.Append("Diffusion:");
            Enumerable.Range(0, root.Length).Aggregate(sb, (b, i) => sb.Append(GetDiffusion(i).ToString("0.000")).Append(", "));
            return sb.ToString();
            //return $"{root[0]}\n{root[1]}\ndif={Constants[0].ToString("0.000")},{Constants[1].ToString("0.000")}";
        }

        public void Serialize(string fn)
        {
            Serializer.Serialize(root, Constants.Take(DiffusionConstantLength).ToArray(), fn);
        }

        public static Gene Deserialize(string fn, Random rand)
        {
            double[] dif = new double[DiffusionConstantLength];
            var e = Serializer.Deserialize(fn, dif);
            var g = new Gene(rand, e);
            for (int i = 0; i < dif.Length; i++)
            {
                g.Constants[i] = dif[i];
            }
            return g;
        }

    }
}
