﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Tree = System.Linq.Expressions;

namespace GeneticReaction.Expressions
{
    [Serializable]
    public class ParameterExpression : ValueExpression
    {

        public int Index { get; private set; }
        public ParameterExpression(int index)
        {
            this.Index = index;
        }

        public ParameterExpression()
            : this(0)
        {

        }

        public override Tree.Expression ToExpressionTree(ExpressionBuilder builder)
        {
            return Tree.Expression.ArrayAccess(ExpressionBuilder.Params, Tree.Expression.Constant(Index));
            //return ExpressionBuilder.Params[Index];
        }

        public override string ToString()
        {
            return $"p{Index}";
        }

        protected override Expression Clone()
        {
            return new ParameterExpression(Index);
        }
    }

    /// <summary>
    /// 0..1 random double constant
    /// </summary>
    [Serializable]
    public class ConstantExpression : ValueExpression
    {
        int index = -1;
        public double Value { get; set; }

        public override void Build(ExpressionBuilder builder)
        {
            base.Build(builder);
            index = builder.AddConstant(this);
        }

        public override Tree.Expression ToExpressionTree(ExpressionBuilder builder)
        {
            return Tree.Expression.Constant(Value, ValueType);
        }

        public override string ToString()
        {
            return Value.ToString("0.00000");
        }

        protected override Expression Clone()
        {
            return new ConstantExpression();
        }
    }

    [Serializable]
    public class ImmutableExpression : ValueExpression
    {
        public double Value { get; private set; }

        public ImmutableExpression(double value)
        {
            Value = value;
        }

        public override string ToString()
        {
            return Value.ToString("0.00000");
        }

        protected override Expression Clone()
        {
            return new ImmutableExpression(Value);
        }

        public override Tree.Expression ToExpressionTree(ExpressionBuilder builder)
        {
            return Tree.Expression.Constant(Value, ValueType);
        }
    }
}
