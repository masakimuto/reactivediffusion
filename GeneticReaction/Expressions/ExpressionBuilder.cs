﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tree = System.Linq.Expressions;
using ExpressionType = System.Func<double[], double>;
using System.Diagnostics;

namespace GeneticReaction.Expressions
{
    //use to crossover
    public class ExpressionBuilder
    {
        public static Tree.ParameterExpression Params;

        List<ConstantExpression> Constants = new List<ConstantExpression>();

        public int ConstantCount { get { return Constants.Count; } }

        List<Expression> nodes = new List<Expression>();
        public Expression Root { get; private set; }
        int maxDepth = -1;


        public Random Random;

        public void Add(Expression node, int level)
        {
            nodes.Add(node);
            maxDepth = Math.Max(level, maxDepth);
        }

        static ExpressionBuilder()
        {
            //Params = Enumerable.Range(0, Config.ParamCount).Select(x => Tree.Expression.Parameter(Expression.ValueType, "a" + x.ToString())).ToArray();

            Params = Tree.Expression.Parameter(typeof(double[]), "a");
        }

        public ExpressionBuilder(Expression root)
        {
            this.Root = root;
            
        }

        /// <summary>
        /// 両方のパラメタを含むか
        /// </summary>
        /// <returns></returns>
        public bool IsValid()
        {
            return nodes.OfType<ParameterExpression>().Select(x => x.Index).Distinct().Count() >= 2;
        }

        public ExpressionType Compile()
        {
            var r = Root.ToExpressionTree(this);
            var lambda = Tree.Expression.Lambda<ExpressionType>(r, Params);
            return lambda.Compile();
        }

        public int AddConstant(ConstantExpression c)
        {
            Constants.Add(c);
            return Constants.Count - 1;
        }

        public ExpressionBuilder SetConstants(IEnumerable<double> constants)
        {
            int i = 0;
            foreach (var item in constants)
            {
                Constants[i].Value = item;
                i++;
            }
            return this;
        }

        public double GetConstant(int index)
        {
            return Constants[index].Value;
        }


        public void Crossover(ExpressionBuilder other)
        {
            if(maxDepth == 0 || other.maxDepth == 0)
            {
                return;
            }
            var p1 = SelectCrossOverPoint();
            var p2 = other.SelectCrossOverPoint().CloneTree();
            p1.AtParent = p2;
            p2.Parent = p1.Parent;

            Rebuild();
        }

        void Rebuild()
        {
            nodes.Clear();
            Constants.Clear();
            maxDepth = -1;
            Root.Build(this);
        }

        Expression SelectCrossOverPoint()
        {
            var d = SelectCrossOverDepth();
            var tgt = nodes.Where(x => x.Level == d).ToArray();
            return tgt[Random.Next(0, tgt.Length)];
        }

        int SelectCrossOverDepth()
        {
            int m = (int) Math.Pow(2, maxDepth) - 1;
            var r = Random.Next(0, m);
            int step = 1;
            int sum = 0;
            for (int i = 0; i < maxDepth; i++)
            {
                sum += step;
                step *= 2;
                if (r < sum)
                {
                    return maxDepth - i;
                }
            }
            return 1;
        }


        public void Mutate()
        {
            if(maxDepth == 0)
            {
                return;
            }
            switch (Random.Next(6))
            {
                case 0:
                    Insert(SelectTarget(null));
                    break;
                case 1:
                    Remove(SelectTarget(x => x.ChildCount > 0));
                    break;
                case 2:
                    Tour(SelectTarget(null));
                    break;
                case 3:
                    Swap(SelectTarget(x => x.ChildCount == 2));
                    break;
                case 4:
                    RemoveSubTree(SelectTarget(null));
                    break;
                case 5:
                    ChangeSubTree(SelectTarget(null));
                    break;
                default:
                    break;
            }
            Rebuild();
        }

        Expression SelectTarget(Func<Expression, bool> cond)
        {
            if (cond == null)
            {
                var i = Random.Next(1, nodes.Count);
                return nodes[i];
            }
            else
            {
                var a = nodes.Skip(1).Where(x => cond(x)).ToArray();
                if(a.Length == 0)
                {
                    return null;
                }
                var i = Random.Next(a.Length);
                return a[i];
            }
        }

        void Insert(Expression target)
        {
            var next = Expression.CreateRandomBinary(Random);
            var other = Expression.CreateRandomValue(Random);
            next.Parent = target.Parent;
            target.AtParent = next;
            if (Random.Next(2) == 0)
            {
                next.AddChildren(target, other);
            }
            else
            {
                next.AddChildren(other, target);
            }
        }

        void Remove(Expression target)
        {
            if(target == null)
            {
                return;
            }
            Debug.Assert(target.ChildCount > 0);
            var ti = target.ChildCount == 2 ? Random.Next(0, 2) : 0;

            target.Parent.children[target.BrotherIndex] = target.children[ti];
            target.children[ti].Parent = target.Parent;
        }

        /// <summary>
        /// 要素の巡回、Operatorの交換
        /// </summary>
        /// <param name="target"></param>
        void Tour(Expression target)
        {
            var another = nodes.Skip(1).Where(x => x.ChildCount == target.ChildCount && x != target).FirstOrDefault();
            if(another == null)
            {
                return;
            }

            for (int i = 0; i < target.ChildCount; i++)
            {
                var tmp = target.children[i];
                target.children[i] = another.children[i];
                another.children[i] = tmp;
                target.children[i].Parent = target;
                another.children[i].Parent = another;
            }

            var ap = another.Parent;
            target.Parent.children[target.BrotherIndex] = another;
            another.Parent.children[another.BrotherIndex] = target;

            another.Parent = target.Parent;
            target.Parent = ap;

            //target.Parent.children[target.Index] = another;
            //another.Parent.children[another.Index] = target;
            //var dummy = target.Parent;
            //target.Parent = another.Parent;
            //another.Parent = dummy;

            //var buf = target.children;
            //target.children = another.children;
            //another.children = buf;

        }

        /// <summary>
        /// 引数順の入れ替え
        /// </summary>
        /// <param name="target"></param>
        void Swap(Expression target)
        {
            if(target == null)
            {
                return;
            }
            Debug.Assert(target.ChildCount == 2);
            target.children[0].BrotherIndex = 1;
            target.children[1].BrotherIndex = 0;
            var tmp = target.children[0];
            target.children[0] = target.children[1];
            target.children[1] = tmp;
        }

        void ChangeSubTree(Expression target)
        {
            var next = Expression.CreateRandomTree(Random);
            target.AtParent = next;
            next.Parent = target.Parent;
        }

        void RemoveSubTree(Expression target)
        {
            var next = Expression.CreateRandomValue(Random);
            target.AtParent = next;
            next.Parent = target.Parent;
        }

        void SetAsParent(Expression target, Expression child)
        {
            child.Parent.children[child.BrotherIndex] = target;
            child.Parent = target;
            for (int i = 0; i < child.ChildCount; i++)
            {
                child.children[i].Parent = target;
            }

        }

        
    }
}
