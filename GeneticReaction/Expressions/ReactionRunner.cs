﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticReaction.Expressions
{
    public class ReactionRunner
    {
        public readonly Cell[] Cells;
        readonly int Size;
        readonly int Size2;
        public readonly Gene gene;

        double[][] buffer;

        Func<double[], double>[] reactions;

        public bool IsNan { get; private set; }

        double[] stablePoint;

        public ReactionRunner(Gene gene)
        {
            this.gene = gene;
            stablePoint = gene.StablePoint == null ? Enumerable.Repeat(0.0, Config.ParamCount).ToArray() : gene.StablePoint;
            reactions = gene.Compile();
            Size = Config.FieldSize;
            Size2 = Size * Size;
            Cells = new Cell[Size2];
            for (int i = 0; i < Cells.Length; i++)
            {
                Cells[i] = new Cell();
            }
            SetCellNeighbors();
            colors = new Color[Size2];
            buffer = new double[Config.ParamCount][];
            for (int i = 0; i < buffer.Length; i++)
            {
                buffer[i] = new double[Size2];
            }

            InitNoise(gene.rand);
            average = stablePoint;
            distribution = stablePoint.Select(x => x <= 0 ? Math.E : x).ToArray();
            //cells[99].items[1] = 100;
        }

        void SetCellNeighbors()
        {
            for (int i = 0; i < Cells.Length; i++)
            {
                var p = IndexToPoint(i);
                Cells[i].Neighbors = Around.Select(x => PointToIndex(p.X + x.X, p.Y + x.Y)).Select(j => Cells[j]).ToArray();
                Cells[i].NeighborRate = Enumerable.Range(0, Config.ParamCount)
                    .SelectMany(x => Around.Select(a => !gene.IsAnisotropicDiffusion(x) ? 1.0 : gene.GetAnisotorophicRate(x, Math.Atan2(a.Y, a.X)))).ToArray();
            }
        }

        void InitNoise(Random rand)
        {
            var d = .2;
            for (int i = 0; i < Size2; i++)
            {
                for (int j = 0; j < Config.ParamCount; j++)
                {
                    Cells[i].items[j] = stablePoint[j] + rand.NextDouble() * d; 
                }
            }
        }

        public void Update()
        {
            if (IsNan)
            {
                return;
            }
            Diffusion();
            Reaction();
        }



        int PointToIndex(int x, int y)
        {
            if (x < 0)
            {
                x += Size;
            }
            if (x >= Size)
            {
                x -= Size;
            }
            if (y < 0)
            {
                y += Size;
            }
            if (y >= Size)
            {
                y -= Size;
            }
            return x + y * Size;
        }

        Point IndexToPoint(int i)
        {
            if (i < 0)
            {
                i += Size2;
            }
            if (i >= Size2)
            {
                i -= Size2;
            }
            return new Point(i % Size, i / Size);
        }

        static readonly Point[] Around = new[]
        {
            //new Point(-1, -1),
            new Point(-1, 0),
            //new Point(-1, 1),
            new Point(1, 0),
            //new Point(1, -1),
            //new Point(1, 1),
            new Point(0, 1),
            new Point(0, -1)
        };

        void Reaction()
        {
            for (int i = 0; i < Size2; i++)
            {
                for (int j = 0; j < Config.ParamCount; j++)
                {
                    buffer[j][i] = CalcReaction(Cells[i], j);
                }
            }

            for (int i = 0; i < Config.ParamCount; i++)
            {
                for (int j = 0; j < Size2; j++)
                {
                    Cells[j].items[i] += buffer[i][j] * Config.Dt;
                }
            }
        }

        double CalcReaction(Cell cell, int j)
        {
            var val = reactions[j](cell.items);
            const double Range = 10.0;
            if(!(val < Range))
            {
                val = Range;
            }
            if(!(val > -Range))
            {
                val = -Range;
            }
            return val;
        }

        


        void Diffusion()
        {
            for (int i = 0; i < Config.ParamCount; i++)
            {
                for (int j = 0; j < Size2; j++)
                {
                    buffer[i][j] = Cells[j].items[i] * -1 * Around.Length;
                    for (int a = 0; a < Around.Length; a++)
                    {
                        buffer[i][j] += Cells[j].Neighbors[a].items[i] * Cells[j].NeighborRate[a];
                    }
                }
            }

            for (int i = 0; i < Config.ParamCount; i++)
            {
                for (int j = 0; j < Size2; j++)
                {
                    Cells[j].items[i] += buffer[i][j] * gene.GetDiffusion(i) / Around.Length * Config.Dt;
                }
            }
        }

        Color[] colors;

        public Color CellToColor(Cell cell)
        {
            if (cell.IsNan())
            {
                IsNan = true;
            }
            return new Color(ValueToColor(cell.items[0], 0), ValueToColor(cell.items[2], 2), ValueToColor( cell.items[1], 1));
            //return new Color(
            //    (float)Sigmoid(cell, 0, 0, gene.GetParamMode(0, 0)),
            //    0,//(float)Sigmoid(cell, 2, 2, gene.GetParamMode(2, 2)),
            //    (float)Sigmoid(cell, 1, 1, gene.GetParamMode(1, 1)));
        }

        double[] average, distribution;

        public void UpdateStatics()
        {
            for (int k = 0; k < Config.ParamCount; k++)
            {
                average[k] = 0;
                for (int i = 0; i < Size2; i++)
                {
                    average[k] += Cells[i].items[k];
                }
                average[k] /= Size2;
                distribution[k] = 0;
                for (int i = 0; i < Size2; i++)
                {
                    var t = Cells[i].items[k] - average[k];
                    distribution[k] += Math.Sqrt( t * t);
                }
                distribution[k] /= Size2;
                if(distribution[k] < .1)
                {
                    distribution[k] = .1;
                }
            }
            //Console.WriteLine("update{0},{1}", average[0], distribution[0]);
        }

        float ValueToColor(double value, int i)
        {
            if(i >= Config.ParamCount)
            {
                return 0;
            }
            value = value - average[i];
            return MathHelper.Clamp( (float) (value / distribution[i]) * 0.5f + 0.5f, 0, 1);
        }

        static Color[] nanColor;
        bool isFlat = false;

        public Color[] GetVisual()
        {
            if (IsNan)
            {
                if(nanColor == null)
                {
                    nanColor = Enumerable.Repeat(Color.Black, colors.Length).ToArray();
                }
                return nanColor;
            }
            var col = CellToColor(Cells[0]);
            isFlat = true;
            for (int i = 0; i < colors.Length; i++)
            {
                colors[i] = CellToColor(Cells[i]);
                if(col != colors[i])
                {
                    isFlat = false;
                }
            }
            if (isFlat)
            {
                UpdateStatics();    
            }
            return colors;
        }
    }
}


