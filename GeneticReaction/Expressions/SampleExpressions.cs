﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticReaction.Expressions
{
    public static class SampleExpressions
    {
        //「生物に見られるパターンとその起源」 p113
        public static Expression GiererMeinhrdt(int index)
        {
            if (index == 0)
            {
                return BinaryExpression.Add().AddChildren(
                    BinaryExpression.Sub().AddChildren(
                        new ConstantExpression(),
                        BinaryExpression.Mul().AddChildren(
                            new ConstantExpression(),
                            new ParameterExpression(0)
                        )
                    ),
                    BinaryExpression.Div().AddChildren(
                        BinaryExpression.Mul().AddChildren(
                            new UnaryPowerExpression().AddChildren(new ParameterExpression(0)),
                            new ConstantExpression()
                        ),
                        new ParameterExpression(1))
                );
            }

            if (index == 1)
            {
                return BinaryExpression.Sub().AddChildren(
                    BinaryExpression.Mul().AddChildren(
                        new UnaryPowerExpression().AddChildren(new ParameterExpression(0)),
                        new ConstantExpression()
                    ),
                    BinaryExpression.Mul().AddChildren(
                        new ParameterExpression(1),
                        new ConstantExpression()
                    )
                );
            }

            throw new IndexOutOfRangeException();
        }

        public static Expression Turing(int index)
        {
            return BinaryExpression.Add().AddChildren(
                BinaryExpression.Sub().AddChildren(
                    BinaryExpression.Mul().AddChildren(
                        new ParameterExpression(0),
                        new ConstantExpression()
                    ),
                    BinaryExpression.Mul().AddChildren(
                        new ParameterExpression(1),
                        new ConstantExpression()
                    )
                ),
                new ConstantExpression());
        }

        public static Expression Butterfly(int index)
        {
            if(index == 0)
            {
                return BinaryExpression.Sub().AddChildren(
                    BinaryExpression.Div().AddChildren(
                        BinaryExpression.Mul().AddChildren(
                            new ConstantExpression(),
                            new UnaryPowerExpression().AddChildren(new ParameterExpression(0))
                        ),
                        new ParameterExpression(1)
                    ),
                    BinaryExpression.Mul().AddChildren(
                        new ConstantExpression(),
                        new ParameterExpression(0)
                    )   
                );
            }

            if(index == 1)
            {
                return BinaryExpression.Sub().AddChildren(
                    BinaryExpression.Mul().AddChildren(
                        new ConstantExpression(),
                        new UnaryPowerExpression().AddChildren(new ParameterExpression(0))
                    ),
                    BinaryExpression.Mul().AddChildren(
                        new ConstantExpression(),
                        new ParameterExpression(1)
                    )
                );
            }
            throw new IndexOutOfRangeException();
        }

        //Murray2, p76
        public static Expression Schnakenberg(int i)
        {
            if(i == 0)
            {
                //k1-k2A+k3A^2*B
                return BinaryExpression.Add().AddChildren(
                    BinaryExpression.Sub().AddChildren(
                        new ConstantExpression(),
                        BinaryExpression.Mul().AddChildren(
                            new ParameterExpression(0), 
                            new ConstantExpression()
                        )
                    ),
                    BinaryExpression.Mul().AddChildren(
                        new ConstantExpression(),
                        BinaryExpression.Mul().AddChildren(
                            new UnaryPowerExpression().AddChildren(new ParameterExpression(0)), 
                            new ParameterExpression(1)
                        )
                    )
                );
            }
            if(i == 1)
            {
                //k4-k3A^2*B
                return BinaryExpression.Sub().AddChildren(
                    new ConstantExpression(),
                    BinaryExpression.Mul().AddChildren(
                        BinaryExpression.Mul().AddChildren(
                            new ConstantExpression(),
                            new UnaryPowerExpression().AddChildren(new ParameterExpression(0))
                        ),
                        new ParameterExpression(1)
                    )
                );
            }
            throw new IndexOutOfRangeException();
        }

        public static Expression GinzburgLandau(int i)
        {
            return BinaryExpression.Mul().AddChildren(
                BinaryExpression.Mul().AddChildren(new ParameterExpression(0), new ParameterExpression(1)),
                BinaryExpression.Sub().AddChildren(
                    BinaryExpression.Sub().AddChildren(new ConstantExpression(), new UnaryPowerExpression().AddChildren(new ParameterExpression(0))),
                    new UnaryPowerExpression().AddChildren(new ParameterExpression(1))
                )
            );
        }

        //Meinhardt Biological pattern formation, formula 1
        public static Expression Meinhardt2(int i)
        {
            if(i == 0)
            {
                return BinaryExpression.Add(
                    BinaryExpression.Div(
                        BinaryExpression.Mul(
                            new ConstantExpression(),
                            UnaryExpression.Power(new ParameterExpression(0))
                        ),
                        BinaryExpression.Mul(
                            BinaryExpression.Add(
                                new ImmutableExpression(1.0),
                                BinaryExpression.Mul(
                                    new ConstantExpression(),
                                    UnaryExpression.Power(new ParameterExpression(0))
                                )
                            ),
                            new ParameterExpression(1)
                        )
                    ),
                    UnaryExpression.Negative(
                        BinaryExpression.Mul(
                            new ConstantExpression(),
                            new ParameterExpression(0)
                            )
                    ),
                    new ConstantExpression()
                );
            }
            else
            {
                return BinaryExpression.Add(
                    BinaryExpression.Mul(
                        new ConstantExpression(),
                        UnaryExpression.Power(new ParameterExpression(0))
                    ),
                    UnaryExpression.Negative(
                        BinaryExpression.Mul(
                            new ConstantExpression(),
                            new ParameterExpression(1)
                        )
                    ),
                    new ConstantExpression()
                );
            }
        }

        //Meinhardt Biological pattern formation, formula 2
        public static Expression Meinhardt3(int i)
        {
            if (i == 0)
            {
                return BinaryExpression.Add(
                    BinaryExpression.Div(
                        BinaryExpression.Mul(
                            new ConstantExpression(),
                            UnaryExpression.Power(new ParameterExpression(0)),
                            new ParameterExpression(1)
                        ),
                        BinaryExpression.Add(
                            new ImmutableExpression(1.0),
                            BinaryExpression.Mul(
                                new ConstantExpression(),
                                UnaryExpression.Power(new ParameterExpression(0))
                            )
                        )                        
                    ),
                    UnaryExpression.Negative(
                        BinaryExpression.Mul(
                            new ConstantExpression(),
                            new ParameterExpression(0)
                            )
                    ),
                    new ConstantExpression()
                );
            }
            else
            {
                return BinaryExpression.Add(
                    UnaryExpression.Negative(
                        BinaryExpression.Div(
                            BinaryExpression.Mul(
                                new ConstantExpression(),
                                UnaryExpression.Power(new ParameterExpression(0)),
                                new ParameterExpression(1)
                            ),
                            BinaryExpression.Add(
                                new ImmutableExpression(1.0),
                                BinaryExpression.Mul(
                                    new ConstantExpression(),
                                    UnaryExpression.Power(new ParameterExpression(0))
                                )
                            )
                        )
                    ),
                    new ConstantExpression()
                );
            }
        }



    }
}
