﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Func = System.Func<double[], double>;

namespace GeneticReaction.Expressions
{
    public static class StableEval
    {
        static readonly double delta = 0.01;
        static readonly double epsilon = 0.001;
        static readonly int N = 10;

        /// <summary>
        /// 拡散不安定性を示すか
        /// </summary>
        /// <param name="f"></param>
        /// <returns></returns>
        public static double[] EvalUnstable(Func[] f)
        {
            return Enumerable.Repeat(0.0, Config.ParamCount).ToArray();
            var p = CalcSolution(f, new[] { 2.0, 2.0 });
            if(p == null)
            {
                return null;
            }
            var f0 = f[0](p);
            var g0 = f[1](p);
            var fu = f[0](new[] { p[0] + delta, p[1] }) - f0;
            var fv = f[0](new[] { p[0], p[1] + delta }) - f0;
            var gu = f[1](new[] { p[0] + delta, p[1] }) - g0;
            var gv = f[1](new[] { p[0], p[1] + delta }) - g0;


            var res = (fu > 0 && gu > 0 && fv < 0 && gv < 0)//activator/inhibitor model
                || (fu < 0 && gu < 0 && fv > 0 && gv > 0)
                || (fu > 0 && fv > 0 && gu < 0 && gv < 0)//resouce consume model
                || (fu < 0 && fv < 0 && gu > 0 && gv > 0);
            if (res) return p;
            else return null;
        }

        static double[] ZeroPt;

        static StableEval()
        {
            ZeroPt = Enumerable.Repeat(0.0, Config.ParamCount).ToArray();
        }

        /// <summary>
        /// 規程ステップ後に発散していたらFalse
        /// </summary>
        /// <param name="f"></param>
        /// <returns></returns>
        public static bool EvalNan(Func[] f)
        {
            return true;
            var sol = CalcSolution(f, ZeroPt);
            return !sol.Any(x => double.IsInfinity(x) || double.IsNaN(x));
        }

        /// <summary>
        /// 安定点の探索
        /// </summary>
        /// <param name="f"></param>
        /// <returns></returns>
        static double[] CalcSolution(Func[] f, double[] init )
        {
            var state = init.Clone() as double[];
            double step = 1;
            double[] d = new double[state.Length];
            for (int i = 0; i < N; i++)
            {
                for (int k = 0; k < f.Length; k++)
                {
                    d[k] = f[k](state);
                }
                step = Math.Pow(0.98, i) * 0.1;
                for (int k = 0; k < f.Length; k++)
                {
                    state[k] += d[k] * step;
                }
            }
            return state;
        }

    }
}
