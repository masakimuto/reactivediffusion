﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace GeneticReaction.Expressions
{
    public static class Serializer
    {
        public static void Serialize(Expression[] root, double[] dif, string fileName)
        {
            var fmt = new BinaryFormatter();
            using (var file = File.Open(fileName, FileMode.Create, FileAccess.Write))
            {
                fmt.Serialize(file, root);
                foreach (var item in dif)
                {
                    file.Write(BitConverter.GetBytes(item), 0, sizeof(double));
                }
            }
            
        }

        public static Expression[] Deserialize(string fileName, double[] dif)
        {
            var fmt = new BinaryFormatter();
            try
            {
                using (var file = File.Open(fileName, FileMode.Open, FileAccess.Read))
                {
                    var exp = fmt.Deserialize(file) as Expression[];
                    for (int i = 0; i < dif.Length; i++)
                    {
                        var buf = new byte[sizeof(double)];
                        file.Read(buf, 0, sizeof(double));
                        dif[i] = BitConverter.ToDouble(buf, 0);
                    }
                    return exp;
                }
            }
            catch(Exception e)
            {
                throw new Exception("failed to load gene from " + fileName, e);
            }
        }
    }
}
