﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticReaction
{
    public class Cell
    {
        public static int ParamCount;
        public double[] items;

        static Cell()
        {
            ParamCount = Config.ParamCount;
        }

        public Cell()
        {
            items = new double[Math.Max(ParamCount, 3)];
        }

        public override string ToString()
        {
            return $"{items[0]}, {items[1]}";
        }

        public bool IsNan()
        {
            return items.Any(x => double.IsNaN(x) || double.IsInfinity(x));
        }

        public Cell[] Neighbors;
        public double[] NeighborRate;
    }
}
