﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace GeneticReaction
{
    class Perf
    {
        public Perf()
        {
            const int Items = 36;
            const int Steps = 100;

            Random rand = new Random(0);
            var runners = new ReactionRunner[Items];
            for (int i = 0; i < runners.Length; i++)
            {
                runners[i] = new ReactionRunner(Gene.Adam(rand));
            }
            var watch = new Stopwatch();
            watch.Start();
            Parallel.ForEach(runners, r =>
            {
                for (int i = 0; i < Steps; i++)
                {
                    r.Update();
                }
            });
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds);
        }
    }
}
