﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.IO;

namespace GeneticReaction
{
    public partial class RendererControl : UserControl
    {

        public RendererControl()
        {
            InitializeComponent();
        }

        protected override void OnCreateControl()
        {
            if (!DesignMode)
            {
                CreateDevice();
            }
            base.OnCreateControl();
        }

        Texture2D target;

        GraphicsDevice device;
        BasicEffect effect;
        VertexBuffer vertex;
        IndexBuffer index;

        private void CreateDevice()
        {
            var pp = new PresentationParameters()
            {
                BackBufferFormat = SurfaceFormat.Color,
                DepthStencilFormat = DepthFormat.None,
                BackBufferWidth = Config.FieldSize,
                BackBufferHeight = Config.FieldSize,
                DeviceWindowHandle = Handle,
                MultiSampleCount = 4,
                IsFullScreen = false,
                PresentationInterval = PresentInterval.Immediate,
            };
            
            device = new GraphicsDevice(GraphicsAdapter.DefaultAdapter, GraphicsProfile.HiDef, pp);
            target = new Texture2D(device, Config.FieldSize, Config.FieldSize, false, SurfaceFormat.Color);
            effect = new BasicEffect(device);
            effect.TextureEnabled = true;
            effect.LightingEnabled = false;
            effect.VertexColorEnabled = false;
            effect.World = Matrix.Identity;
            effect.View = Matrix.Identity;
            effect.Projection = Matrix.Identity;
            vertex = new VertexBuffer(device, typeof(VertexPositionTexture), 4, BufferUsage.WriteOnly);
            vertex.SetData(new VertexPositionTexture[]
            {
                new VertexPositionTexture(new Vector3(-1, 1, 0), new Vector2(0, 0)),
                new VertexPositionTexture(new Vector3(-1, -1, 0), new Vector2(0, 1)),
                new VertexPositionTexture(new Vector3(1, -1, 0), new Vector2(1, 1)),
                new VertexPositionTexture(new Vector3(1, 1, 0), new Vector2(1, 0))
            });
            index = new IndexBuffer(device, IndexElementSize.SixteenBits, 6, BufferUsage.WriteOnly);
            index.SetData(new short[] { 2, 1, 0, 3, 2, 0 });
        }

        public void Draw(Color[] data)
        {
            target.SetData(data);
            effect.Texture = target;
            effect.CurrentTechnique.Passes[0].Apply();
            device.SetVertexBuffer(vertex);
            device.Indices = index;
            device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, 4, 0, 2);
            device.Present();
            //device.Clear(Color.LightCoral);
            //sprite.Begin();
            //for (int i = 0; i < data.Length; i++)
            //{
            //    sprite.Draw(texture, new Vector2(i % Config.FieldSize, i / Config.FieldSize), data[i]);
            //}
            //sprite.End();
            //device.Present();
            
        }

        public void SaveRender(string fn)
        {
            using (var f = File.Open(fn, FileMode.Create, FileAccess.Write))
            {
                target.SaveAsPng(f, target.Width, target.Height);
            }
        }

        protected override void DestroyHandle()
        {
            base.DestroyHandle();
            effect.Dispose();
            index.Dispose();
            target.Dispose();
            vertex.Dispose();
            device.Dispose();
        }
    }
}
