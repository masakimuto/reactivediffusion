﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenTK;
using OpenTK.Graphics;

namespace RenderingConsole
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        GraphicsContext context;
        GLHost.TileRenderer tile;

        string src, dst;
        int res, time;

        private void Form1_Load(object sender, EventArgs e)
        {
            var window = OpenTK.Platform.Utilities.CreateWindowsWindowInfo( this.Handle);
            context = new GraphicsContext(GraphicsMode.Default, window);
            context.LoadAll();

            var args = Environment.GetCommandLineArgs();
            if(args.Length < 4)
            {
                Console.WriteLine($"Usage:{args[0]} src dst resolution (time:1000)");
                Close();
                return;
            }
            src = args[1];
            dst = args[2];
            res = int.Parse(args[3]);
            if(args.Length >= 5)
            {
                time = int.Parse(args[4]);
            }else
            {
                time = 1000;
            }

            tile = new GLHost.TileRenderer(res, 1, 1);
            tile.LoadContent();
            var gene = GLHost.ExtentGene.Deserialize(src, new Random());
            tile.SetGenes(new[] { gene });

            tile.Update(time);

            tile.Output(0, dst);

            Console.WriteLine("complete");

            Close();

        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            if(tile != null)
            {
                tile.Dispose();
                tile = null;
            }
            if(context != null)
            {
                context.Dispose();
                context = null;
            }
        }
    }
}
