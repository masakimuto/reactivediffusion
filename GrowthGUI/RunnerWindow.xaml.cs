﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using GrowGrowth;

namespace GrowthGUI
{
    /// <summary>
    /// Interaction logic for RunnerWindow.xaml
    /// </summary>
    public partial class RunnerWindow : Window
    {
        DispatcherTimer timer;

        GraphicsDevice device;
        Manager growth;

        int stepSize;

        public RunnerWindow(int stepSize = 1)
        {
            this.stepSize = stepSize;
            InitializeComponent();
        }


        protected override void OnActivated(EventArgs e)
        {
            if (device == null)
            {
                var src = (HwndSource)PresentationSource.FromVisual(this);
                CreateDevice(src.Handle);
                CreateGrowth();
                timer = new DispatcherTimer();
                timer.Interval = new TimeSpan(1);
                timer.Tick += Timer_Tick;
                timer.Start();
            }
            base.OnActivated(e);
        }

        void CreateDevice(IntPtr handle)
        {
            var pp = new PresentationParameters()
            {
                BackBufferFormat = SurfaceFormat.Color,
                DepthStencilFormat = DepthFormat.Depth24,
                BackBufferWidth = (int)Width,
                BackBufferHeight = (int)Height,
                DeviceWindowHandle = handle,
                IsFullScreen = false,
                MultiSampleCount = 1,
                PresentationInterval = PresentInterval.Immediate,
            };

            device = new GraphicsDevice(GraphicsAdapter.DefaultAdapter, GraphicsProfile.HiDef, pp);
        }

        void CreateGrowth()
        {
            //SaveParams();
            LoadParams();
            growth.LoadContent();
        }

        void SaveParams()
        {
            var ser = new System.Xml.Serialization.XmlSerializer(typeof(GrowthParam));
            using (var file = File.OpenWrite("growth.xml"))
            {
                growth = new SerializeGrowth(device);
                ser.Serialize(file, ((SerializeGrowth)growth).data);
            }
        }

        void LoadParams()
        {
            var ser = new System.Xml.Serialization.XmlSerializer(typeof(GrowthParam));
            using (var file = File.OpenRead("growth.xml"))
            {
                var p = ser.Deserialize(file) as GrowthParam;
                growth = new SerializeGrowth(device, p);
            }
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            device.Clear(Microsoft.Xna.Framework.Color.CornflowerBlue);
            var time = new Microsoft.Xna.Framework.GameTime();
            for (int i = 0; i < stepSize; i++)
            {
                growth.Update(time);

            }
            growth.Draw(time);
            device.Present();
        }

        protected override void OnClosed(EventArgs e)
        {
            timer.Stop();
            growth.Dispose(true);
            device.Dispose();
            base.OnClosed(e);
        }
    }
}
