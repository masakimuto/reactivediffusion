﻿using System;
using System.IO;
using System.Windows;
using System.Linq;
using System.Xml.Linq;
using System.Windows.Controls;

namespace GrowthGUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            eqs = new[] { eq0, eq1 };
            SetupText();
        }

        XElement rootNode;

        TextBox[] eqs;

        public int StepSize { get; set; }

        void SetupText()
        {
            var file = File.ReadAllText(fileName);
            var doc = XDocument.Parse(file);
            rootNode = doc.Root;
            var dif = doc.Root.Element("Reactions");
            for (int i = 0; i < eqs.Length; i++)
            {
                eqs[i].Text = dif.Elements().ElementAt(i).Value;
            }
            StepSize = 1;
        }
        

        void ApplyEquatations()
        {
            var nodes = rootNode.Element("Reactions").Elements().ToArray();
            for (int i = 0; i < eqs.Length; i++)
            {
                nodes[i].Value = eqs[i].Text;
            }
        }

        private void ApplyButtonClick(object sender, RoutedEventArgs e)
        {
            var texts = eqs.Select(x => x.Text).ToArray();
            SetupText();
            for (int i = 0; i < texts.Length; i++)
            {
                eqs[i].Text = texts[i];
            }
            ApplyEquatations();
            
            using (var file = File.Open(fileName, FileMode.Create, FileAccess.Write))
            {
                rootNode.Document.Save(file);
                file.Flush();
                file.Close();
            }
            if (runner != null)
            {
                runner.Close();
            }
            runner = new RunnerWindow(10);
            runner.Show();
        }

        string fileName = "growth.xml";

        void OpenFileButtonClick(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start(fileName);
        }

        RunnerWindow runner;

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            new RunnerWindow(10).Show();
        }
    }
}
