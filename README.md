https://bitbucket.org/masakimuto/reactivediffusion

# Projects
* ReactiveDiffusion : Simple 2D implementation
* Cell1D : 1Dimensional fractal 
* GrowGrowth : 2Dimensional fractal
* GrowthGUI : GUI for GrowGrowth
* GeneticReaction (RDIEC) : Interactive Texture generator by reactive-diffusion system
* SeedCreator : out-of-date project

# RDIEC Usage
Select Favorite textures, and click "Next" Button to evolve textures.
* You select no texture, all item will reset randomly.
* You select just 1 texture, next generation textures are mutant of the texture.
* You select 2 or more texture, next generation textures are crossover child of 2 selected texture.

Click "Step" button and step textures' time by 50 steps.

Click "Save" button and a selected texture's gene will be saved to a file. You can drag-drop the gene file to the window to load the gene.

Click "Render" button and a selected texture(not gene) will be saved to a PNG file.

Clicking "Undo" button allows you restore the last generation textures.

"AutoStep" toggle button controls texture time sequence.

## RDIEC Setting
RDIEC can load custom setting file from command line arguments.
` .\GeneticReaction.exe config.txt `

config.txt

    0.3
    0.2
    36
    6
    2
    512
    3
    0.5

1. Mutation rate after crossover
2. Value Mutation Range 
3. Execution Pool size
4. Execution Pool Square Size
5. Texture Display scale
6. Model Parameter Number
7. Step Time Length
