﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GrowGrowth
{
    public class Growth : DrawableGameComponent
    {
        VertexBuffer vbuf;
        IndexBuffer ibuf;
        BasicEffect effect;
        Texture2D texture;

        public Growth(MyGame game)
            : base(game)
        {
        }

        RasterizerState rasterWire;

        protected override void LoadContent()
        {
            base.LoadContent();
            texture = new Texture2D(GraphicsDevice, 1, 1);
            texture.SetData(new[] { Color.White });
            rasterWire = new RasterizerState()
            {
                //CullMode = CullMode.None,
                FillMode = FillMode.Solid
            };
            vbuf = new VertexBuffer(GraphicsDevice, typeof(VertexPositionNormalTexture), 12, BufferUsage.WriteOnly);
            ibuf = new IndexBuffer(GraphicsDevice, IndexElementSize.SixteenBits, 60, BufferUsage.WriteOnly);
            effect = new BasicEffect(GraphicsDevice);
            CreateShape();
            effect.World = Matrix.Identity;
            effect.Projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, 1, .1f, 100f);
            effect.View = Matrix.CreateLookAt(new Vector3(0, 0, -10), Vector3.Zero, Vector3.Up);
            //effect.VertexColorEnabled = true;
            effect.EnableDefaultLighting();
        }

        void CreateShape()
        {
            float rad = 1f;
            float length = 2f;
            int divide = 8;

            var r = 1f;
            var pos = new Vector3[12];
            pos[0] = new Vector3(0, r, 0);
            pos[11] = new Vector3(0, -r, 0);
            var a = (float)(r / (Math.Sqrt(3) / 12d * (3 + Math.Sqrt(5))));

            for (int i = 0; i < 5; i++)
            {
                var b = (Math.Sqrt(5) / 5);
                var y = b * r;
                var rc = Math.Sqrt(r * r - y * y);
                var ang = Math.PI * 2 / 5d * i;
                pos[i + 1] = new Vector3((float)(Math.Cos(ang) * rc), (float)y, (float)(Math.Sin(ang) * rc));
                ang += Math.PI / 5f;
                pos[i + 6] = new Vector3((float)(Math.Cos(ang) * rc), (float)-y, (float)(Math.Sin(ang) * rc));
                //pos[i + 1] = 
            }

            var ind = new short[]
            {
                0, 1, 2, 0, 2, 3, 0, 3, 4, 0, 4, 5, 0, 5, 1,//upper 5
                11, 10, 9, 11, 9, 8, 11, 8, 7, 11, 7, 6, 11, 6, 10,//downer 5
                2, 1, 6, 3, 2, 7, 4, 3, 8, 5, 4, 9, 1, 5, 10,
                //1, 2, 6, 2, 3, 7, 3, 4, 8, 4, 5, 9, 5, 1, 10,
                6, 7, 2, 7, 8, 3, 8, 9, 4, 9, 10, 5, 10, 6, 1,
                //7, 6, 2, 8, 7, 3, 9, 8, 4, 10, 9, 5, 6, 10, 1,
            };

            vbuf.SetData(pos.Select(x => new VertexPositionNormalTexture(x, Vector3.Normalize(x), Vector2.Zero)).ToArray());
            ibuf.SetData(ind);

        }



        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            var r = 10f;
            var a = gameTime.TotalGameTime.TotalSeconds * Math.PI * .5;
            var pos = new Vector3((float)Math.Cos(a) * r, 0, (float)Math.Sin(a) * r);
            effect.View = Matrix.CreateLookAt(pos, Vector3.Zero, Vector3.Up);
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
            GraphicsDevice.RasterizerState = rasterWire;
            GraphicsDevice.DepthStencilState = DepthStencilState.None;

            GraphicsDevice.SetVertexBuffer(vbuf);
            GraphicsDevice.Indices = ibuf;
            effect.Texture = texture;
            effect.CurrentTechnique.Passes[0].Apply();
            GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, 12, 0, 20);

            //GraphicsDevice.DrawUserPrimitives(PrimitiveType.TriangleList, new VertexPositionColor[]
            //{
            //    new VertexPositionColor(Vector3.Zero, Color.White),
            //    new VertexPositionColor(new Vector3(1, 1, 0), Color.Red),
            //    new VertexPositionColor(new Vector3(2, 5, 0), Color.Blue)
            //}, 0, 1);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            effect.Dispose();
            vbuf.Dispose();
            ibuf.Dispose();

        }


    }
}
