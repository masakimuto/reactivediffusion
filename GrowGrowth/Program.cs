﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace GrowGrowth
{
    class Program
    {
        public static void Main(string[] args)
        {
            using (var game = new MyGame(IntPtr.Zero))
            {
                game.Run();
            }
        }
    }

    internal class MyGame : Game
    {
        GraphicsDeviceManager manager;
        IntPtr windowHandle;

        Manager growth;

        public MyGame(IntPtr windowHandle)
        {
            this.windowHandle = windowHandle;
            manager = new GraphicsDeviceManager(this)
            {
                PreferredBackBufferWidth = 1024,
                PreferredBackBufferHeight = 1024,
            };
            
            manager.PreparingDeviceSettings += Manager_PreparingDeviceSettings;
        }

        private void Manager_PreparingDeviceSettings(object sender, PreparingDeviceSettingsEventArgs e)
        {
            if(windowHandle != IntPtr.Zero)
            {
                e.GraphicsDeviceInformation.PresentationParameters.DeviceWindowHandle = windowHandle;
            }
        }

        void CreateGrowth()
        {
            growth = new DirectedGrowth(GraphicsDevice);

            growth.LoadContent();

        }

        protected override void LoadContent()
        {
            base.LoadContent();
            CreateGrowth();
        }


        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            if (Keyboard.GetState().IsKeyDown(Keys.Space))
            {
                CreateGrowth();
            }
            growth.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            base.Draw(gameTime);
            growth.Draw(gameTime);
        }
    }
}
