﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Diagnostics;

namespace GrowGrowth
{
    struct Edge
    {
        public readonly int P1, P2;

        public Edge(int p1, int p2)
        {
            Debug.Assert(p1 != p2);
            P1 = p1;
            P2 = p2;
        }

        public override bool Equals(object obj)
        {
            if (obj is Edge)
            {
                var e = (Edge)obj;
                return (e.P1 == this.P1 && e.P2 == this.P2) || (e.P1 == this.P2 && e.P2 == this.P1);
            }
            return false;
        }

        public override int GetHashCode()
        {
            return P1 * P2 + P1 + P2;
        }

        public override string ToString()
        {
            return base.ToString() + $"({P1},{P2})";
        }

    }

    struct VertexPositionNormalColor : IVertexType
    {
        public Vector3 Position, Normal;
        public Color Color;

        public VertexPositionNormalColor(Vector3 pos, Vector3 normal, Color color)
        {
            Position = pos;
            Normal = normal;
            Color = color;
        }

        public VertexDeclaration VertexDeclaration
        {
            get
            {
                return new VertexDeclaration(
                    new VertexElement(0, VertexElementFormat.Vector3, VertexElementUsage.Position, 0),
                    new VertexElement(12, VertexElementFormat.Vector3, VertexElementUsage.Normal, 0),
                    new VertexElement(24, VertexElementFormat.Color, VertexElementUsage.Color, 0)
                );
            }
        }
    }

    public class Cell
    {
        public int[] Index = new int[3];
        public float[] Params;

        public int ID;
        public List<int> neighbors= new List<int>(3);

        public Vector3 GetNormal(List<Vector3> v) => Vector3.Normalize(Vector3.Cross(v[Index[1]] - v[Index[2]], v[Index[0]] - v[Index[2]]));
        public Vector3 GetCenter(List<Vector3> v) => (v[Index[0]] + v[Index[1]] + v[Index[2]]) / 3f;


        public virtual Color GetColor() => new Color(Params[0], 0, Params[1], 1);

        protected virtual int ParamCount { get; } = 2;

        public int Generation = 0;

        public Cell()
        {
            Params = new float[ParamCount];
        }

        internal IEnumerable<Edge> GetEdges()
        {
            yield return new Edge(Index[0], Index[1]);
            yield return new Edge(Index[1], Index[2]);
            yield return new Edge(Index[2], Index[0]);
        }

        public bool IsDirty = true;

        public override string ToString()
        {
            return base.ToString() + $"{ID}:({Index[0]}, {Index[1]}, {Index[2]})";
        }

        public void SetNeighbors(IEnumerable<Cell> candidate)
        {
            foreach (var edge in GetEdges())
            {
                var neighbor = GetNeighbors(edge, candidate.Where(x => ID != x.ID));
                neighbors.Add(neighbor);
            }
        }

        internal int GetNeighbors(Edge edge, IEnumerable<Cell> candidate)
        {
            return candidate.SelectMany(x => x.GetEdges().Select(e => Tuple.Create(x.ID, e))).Single(x => x.Item2.Equals(edge)).Item1;
        }

    }

    public class Manager
    {
        DynamicVertexBuffer vbuffer;
        VertexPositionNormalColor[] vertices;
        BasicEffect effect;

        public List<Cell> Items { get; }

        protected virtual int Max { get; } = 1024;

        protected virtual float Dt { get; } = (1f / 32f);
        protected virtual float[] Diffusion { get; } = new[] { 1f / 256f, 1f / 8f };

        public List<Vector3> Vertex { get; }

        readonly float Threshold = 4f;
        readonly float ChildHeight = .3f;

        float[] diffusionBuffer;
        float[][] reactionBuffer;

        GraphicsDevice GraphicsDevice { get; }

        protected virtual int ParamCount { get; } = 2;

        public Manager(GraphicsDevice device)
        {
            GraphicsDevice = device;
            Items = new List<Cell>(Max);
            Vertex = new List<Vector3>(Max * 3);
            diffusionBuffer = new float[Max];
            reactionBuffer = new float[ParamCount][];
            for (int i = 0; i < reactionBuffer.Length; i++)
            {
                reactionBuffer[i] = new float[Max];
            }

            InitShape();
            InitValue(true);

        }


        void InitValue(bool noise)
        {
            var rand = new Random();
            float max = .01f;
            for (int i = 0; i < Items.Count; i++)
            {
                for (int k = 0; k < ParamCount; k++)
                {
                    Items[i].Params[k] = noise ? ((float)rand.NextDouble() * max) : 0;
                }
            }
        }

        protected virtual Cell CreateCell()
        {
            return new Cell();
        }

        static readonly protected float EdgeLength = (float)(1f / (Math.Sqrt(3) / 12d * (3 + Math.Sqrt(5))));

        void InitShape()
        {
            var r = 1f;
            var pos = new Vector3[12];
            pos[0] = new Vector3(0, r, 0);
            pos[11] = new Vector3(0, -r, 0);
            var a = (float)(r / (Math.Sqrt(3) / 12d * (3 + Math.Sqrt(5))));
            //EdgeLength = a;
            for (int i = 0; i < 5; i++)
            {
                var b = (float)Math.Sqrt(5f) / 5f;
                var y = b * r;
                var rc = Math.Sqrt(r * r - y * y);
                var ang = Math.PI * 2 / 5d * i;
                pos[i + 1] = new Vector3((float)(Math.Cos(ang) * rc), (float)y, (float)(Math.Sin(ang) * rc));
                ang += Math.PI / 5f;
                pos[i + 6] = new Vector3((float)(Math.Cos(ang) * rc), (float)-y, (float)(Math.Sin(ang) * rc));
            }

            Vertex.AddRange(pos);

            var ind = new short[]
            {
                0, 1, 2, 0, 2, 3, 0, 3, 4, 0, 4, 5, 0, 5, 1,//upper 5
                11, 10, 9, 11, 9, 8, 11, 8, 7, 11, 7, 6, 11, 6, 10,//downer 5
                2, 1, 6, 3, 2, 7, 4, 3, 8, 5, 4, 9, 1, 5, 10,
                6, 7, 2, 7, 8, 3, 8, 9, 4, 9, 10, 5, 10, 6, 1,
            };

            for (int i = 0; i < 20; i++)
            {
                var cell = CreateCell();
                cell.ID = i;
                for (int j = 0; j < 3; j++)
                {
                    cell.Index[j] = ind[i * 3 + j];
                }
                Items.Add(cell);
            }

            foreach (var item in Items)
            {
                item.SetNeighbors(Items);
            }
        }

        void SetData()
        {
            int i = 0;
            foreach (var item in Items)
            {
                var col = item.GetColor();
                if (item.IsDirty)
                {
                    item.IsDirty = false;
                    var normal = item.GetNormal(Vertex);
                    for (int k = 0; k < 3; k++)
                    {
                        vertices[i + k] = new VertexPositionNormalColor(Vertex[item.Index[k]], normal, col);
                    }
                }
                for (int k = 0; k < 3; k++)
                {
                    vertices[i + k].Color = col;
                }
                i += 3;
            }
            vbuffer.SetData(vertices);
        }

        public void LoadContent()
        {
            vertices = new VertexPositionNormalColor[Max * 3];
            vbuffer = new DynamicVertexBuffer(GraphicsDevice, typeof(VertexPositionNormalColor), Max * 3, BufferUsage.WriteOnly);
            effect = new BasicEffect(GraphicsDevice);
            SetupLighting();
            effect.World = Matrix.Identity;
            effect.Projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, GraphicsDevice.Viewport.AspectRatio, .1f, 100f);
            effect.VertexColorEnabled = true;
            SetData();
        }

        void SetupLighting()
        {
            effect.LightingEnabled = true;
            effect.AmbientLightColor = new Vector3(.2f);
            effect.PreferPerPixelLighting = false;
            var light = effect.DirectionalLight0;
            light.Enabled = true;
            light.SpecularColor = Vector3.Zero;
            light.Direction = -Vector3.One;
            light.DiffuseColor = new Vector3(.5f);

            light = effect.DirectionalLight1;
            light.Enabled = true;
            light.SpecularColor = Vector3.Zero;
            light.Direction = Vector3.Up;
            light.DiffuseColor = new Vector3(.3f);
        }

        public void Update(GameTime gameTime)
        {

            effect.View = UpdateCamera(gameTime);
            UpdateOther();
            UpdateDiffusion();
            UpdateReaction();
            UpdateGeneration();
        }

        protected virtual void UpdateOther() { }

        protected virtual Matrix UpdateCamera(GameTime gameTime)
        {
            var a = gameTime.TotalGameTime.TotalSeconds * MathHelper.TwoPi * .1f;
            var r = 10f;
            return Matrix.CreateLookAt(new Vector3((float)Math.Cos(a) * r, 0, (float)Math.Sin(a) * r), Vector3.Zero, Vector3.Up);

        }

        public Cell[] GenerateChildren(Cell item)
        {
            item.IsDirty = true;
            Vertex.Add(GetChildVertex(item));
            var next = Vertex.Count - 1;

            var child = new[] { new Cell(), new Cell() };

            child[0].ID = Items.Count;
            child[1].ID = Items.Count + 1;

            child[0].Index[0] = item.Index[1];
            child[0].Index[1] = item.Index[2];
            child[0].Index[2] = next;

            child[1].Index[0] = item.Index[2];
            child[1].Index[1] = item.Index[0];
            child[1].Index[2] = next;

            item.Index[2] = next;

            var all = new[] { child[0], child[1], item };

            var cand = new List<Cell>(6);
            cand.AddRange(all);
            cand.AddRange(item.neighbors.Select(x => Items[x]));

            foreach (var c in item.neighbors.Select(x => Items[x]))
            {
                var ind = c.neighbors.IndexOf(item.ID);
                var edge = c.GetEdges().ElementAt(ind);
                c.neighbors[ind] = c.GetNeighbors(edge, all);
            }

            item.neighbors.Clear();
            foreach (var c in all)
            {
                c.SetNeighbors(cand);
            }

            for (int i = 0; i < ParamCount; i++)
            {
                var par = item.Params[i] / 3f;
                for (int k = 0; k < all.Length; k++)
                {
                    all[k].Params[i] = par;
                }
            }

            item.Generation++;
            child[0].Generation = item.Generation;
            child[1].Generation = item.Generation;

            return child;
        }

        protected virtual Vector3 GetChildVertex(Cell cell)
        {
            return cell.GetNormal(Vertex) * ChildHeight + cell.GetCenter(Vertex);
        }

        void UpdateDiffusion()
        {
            for (int i = 0; i < ParamCount; i++)
            {
                for (int j = 0; j < Items.Count; j++)
                {
                    diffusionBuffer[j] = CalcDiffusion(Items[j], Items, i);       
                }
                for (int j = 0; j < Items.Count; j++)
                {
                    Items[j].Params[i] += diffusionBuffer[j] * Dt;
                }
            }
        }

        protected virtual float CalcDiffusion(Cell item, List<Cell> items, int param)
        {
            var s = item.Params[param] * (-Diffusion[param] * 3);
            for (int k = 0; k < 3; k++)
            {
                s += items[item.neighbors[k]].Params[param] * Diffusion[param];
            }
            return s;
        }

        void UpdateReaction()
        {
            for (int i = 0; i < Items.Count; i++)
            {
                var item = Items[i];
                for (int k = 0; k < ParamCount; k++)
                {
                    reactionBuffer[k][i] = CalcReaction(Items[i], k);
                }
            }

            for (int i = 0; i < Items.Count; i++)
            {
                for (int k = 0; k < ParamCount; k++)
                {
                    Items[i].Params[k] += reactionBuffer[k][i] * Dt;
                }
            }
        }

        protected virtual float CalcReaction(Cell item, int param)
        {
            var a = item.Params[0];
            var i = item.Params[1];
            if(param == 0)
            {
                return CalcReactionActivator(a, i);
            }
            else if(param == 1)
            {
                return CalcReactionInhibitor(a, i);
            }
            throw new ArgumentException("", nameof(param));
        }

        void UpdateGeneration()
        {
            var length = Items.Count;
            if (length >= Max)
            {
                return;
            }
            for (int i = 0; i < length; i++)
            {
                if (CanCreateChildren(Items[i]))
                {
                    Items.AddRange(GenerateChildren(Items[i]));
                    //Game.Window.Title = Items.Count.ToString();
                    if (Items.Count >= Max)
                    {
                        return;
                    }
                }
            }
        }

        protected virtual bool CanCreateChildren(Cell item)
        {
            return item.Params[0] > Threshold;
        }

        float CalcReactionActivator(float act, float inh)
        {
            return (.3f - .3f * inh + act * act / inh) * .1f;
        }

        float CalcReactionInhibitor(float act, float inh)
        {
            return (act * act - inh) * .1f;
        }

        public void Draw(GameTime gameTime)
        {
            SetData();
            effect.CurrentTechnique.Passes[0].Apply();
            GraphicsDevice.SetVertexBuffer(vbuffer);
            GraphicsDevice.DrawPrimitives(PrimitiveType.TriangleList, 0, Items.Count);
        }


        public void Dispose(bool disposing)
        {
            vbuffer.Dispose();
            effect.Dispose();
        }
    }
}
