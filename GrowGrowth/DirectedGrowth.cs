﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace GrowGrowth
{
    public class DirectedGrowth : Manager
    {
        Vector3 Direction;
        float Effect = .8f;

        public DirectedGrowth(GraphicsDevice device)
            : base(device)
        {
            Direction = new Vector3(0, 1, 0);
            //Items[0].Params[0] = 1;
        }

        protected override bool CanCreateChildren(Cell item)
        {
            return false;
            //return item.Params[0] > 5f;
        }

        protected override Vector3 GetChildVertex(Cell cell)
        {
            return cell.GetCenter(Vertex) + cell.GetNormal(Vertex) * EdgeLength * (float)Math.Sqrt(2f/3f);
        }

        protected override float CalcDiffusion(Cell item, List<Cell> items, int param)
        {
            var s = 0f;
            //s -= item.Params[param] * 3;
            var ef = param != 0 ? Effect : 0;
            for (int i = 0; i < item.neighbors.Count; i++)
            {
                var n = items[item.neighbors[i]];
                var dir = n.GetCenter(Vertex) - item.GetCenter(Vertex);
                dir.Normalize();
                var f = Math.Abs(Vector3.Dot(Direction, dir));
                //f = Math.Max(Vector3.Dot(Direction, dir), 0f);
                f = f * ef + (1 - ef);
                s += n.Params[param] * f - item.Params[param] * f;
            }
            return s * Diffusion[param];
        }

        
    }
}
