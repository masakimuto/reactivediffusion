﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrowGrowth
{
    public class StandardManager : Manager
    {
        protected override int Max { get; } = 1024;
        protected override float Dt { get; } = (1f / 32f);
        protected override float[] Diffusion { get; } = new[] { 1f / 256f, 1f / 8f };
        protected override int ParamCount { get; } = 2;
        

        public StandardManager(Microsoft.Xna.Framework.Graphics.GraphicsDevice device)
            : base(device)
        {

        }

        protected override float CalcDiffusion(Cell item, List<Cell> items, int param)
        {
            var s = item.Params[param] * -3;
            for (int i = 0; i < item.neighbors.Count; i++)
            {
                s += items[item.neighbors[i]].Params[param];
            }
            return s * Diffusion[param];
        }

        protected override bool CanCreateChildren(Cell item)
        {
            return item.Params[0] > 4f;
        }

        protected override Cell CreateCell()
        {
            return new Cell();
        }

        protected override Vector3 GetChildVertex(Cell cell)
        {
            return cell.GetCenter(Vertex) + cell.GetNormal(Vertex) / (cell.Generation + 1) * .5f;
        }

        protected override void UpdateOther()
        {
            base.UpdateOther();
            Items[0].Params[0] += 1f * Dt;
        }

        protected override float CalcReaction(Cell item, int param)
        {
            var a = item.Params[0];
            var i = item.Params[1];
            switch (param)
            {
                case 0:
                    return (.3f - .3f * i + a * a / i) * .1f;
                case 1:
                    return (a * a - i) * .1f;
                default:
                    throw new ArgumentException("", nameof(param));
            }
        }

        protected override Matrix UpdateCamera(GameTime gameTime)
        {
            var a = gameTime.TotalGameTime.TotalSeconds * .1;
            var r = 5f;

            return Matrix.CreateLookAt(new Vector3((float)Math.Cos(a) * r, 0, (float)Math.Sin(a) * r),
                Vector3.Zero,
                Vector3.Up);
        }

    }
}
