﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using System.Linq.Expressions;
using System.Linq.Dynamic;

namespace GrowGrowth
{
    public class GrowthParam
    {
        public float Dt = 1f / 32f;
        public float[] Diffusion = new[] { 1f / 256f, 1f / 8f };
        public float Threshold = 4f;
        public float ChildHeight = (float)Math.Sqrt(2d / 3d);
        public float WaitTime = 100;

        public string[] Reactions = new[]
        {
            "(0.3f - 0.3f * p[1] + p[0] * p[0] / p[1]) * 0.1f",
            "(p[0] * p[0] - p[1]) * 0.1f" 
        };


        public GrowthParam()
        {

        }
    }

    public class SerializeGrowth : Manager
    {
        public GrowthParam data;

        Func<float[], double>[] reactions;

        protected override float[] Diffusion
        {
            get { return data.Diffusion; }
        }

        protected override float Dt
        {
            get
            {
                return data.Dt;
            }
        }

        protected override int Max
        {
            get
            {
                return 4096;
            }
        }


        public SerializeGrowth(GraphicsDevice device)
            : this(device, new GrowthParam())
        {

        }

        public SerializeGrowth(GraphicsDevice device, GrowthParam param)
            : base(device)
        {
            this.data = param;
            reactions = new Func<float[], double>[data.Reactions.Length];
            for (int i = 0; i < reactions.Length; i++)
            {
                var p = Expression.Parameter(typeof(float[]), "p");
                var exp = System.Linq.Dynamic.DynamicExpression.ParseLambda(new[] { p }, typeof(double), data.Reactions[i]);
                reactions[i] = (Func<float[], double>)(exp.Compile());
            }
        }

        float time = 0;

        protected override void UpdateOther()
        {
            time += Dt;
            base.UpdateOther();
        }

        protected override Vector3 GetChildVertex(Cell cell)
        {
            return cell.GetCenter(Vertex) + cell.GetNormal(Vertex) * data.ChildHeight * EdgeLength /*/ (cell.Generation + 1)*/;
        }

        protected override bool CanCreateChildren(Cell item)
        {
            return time > data.WaitTime && item.Params[0] > data.Threshold;
        }

        protected override float CalcReaction(Cell item, int param)
        {
            return (float)reactions[param](item.Params);
        }

    }
}
