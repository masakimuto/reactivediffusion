﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GeneticReaction.Expressions;
using Tree = System.Linq.Expressions;
using System.Diagnostics;
using System.Xml.Serialization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;

namespace Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestSer()
        {
            var rand = new Random();
            for (int i = 0; i < 10; i++)
            {
                var node = Expression.CreateRandomTree(rand);
                Debug.WriteLine(node.ToString());
                var fmt = new BinaryFormatter();
                var str = new MemoryStream();
                var n2 = Expression.CreateRandomTree(rand);
                Debug.WriteLine(n2);
                fmt.Serialize(str, new[] { node, n2 });
                str.Seek(0, SeekOrigin.Begin);
                var de = fmt.Deserialize(str) as Expression[];
                Debug.WriteLine(de[0]);
                Debug.WriteLine(de[1]);
                //Debug.WriteLine(fmt.Deserialize(str) as Expression);
                Debug.WriteLine("");
            }

        }

    }
}
