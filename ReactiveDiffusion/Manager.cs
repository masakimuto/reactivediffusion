﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.IO;

namespace ReactiveDiffusion
{
    class Manager
    {
        SpriteBatch batch;
        Texture2D texture;

        readonly int UnitSize = 4;
        readonly int Length = 128 * 2;
        readonly int BufferLength;

        readonly float DiffuseA = (1f / 256);
        readonly float DiffuseB = (1f / 16f);

        float[][] bufferA, bufferB;
        int current, last;

        readonly float[] ParamA = new[] { 0.05f, -0.07f, 0.003f, 0f, 0f, 0f };//a, b, 1, Max, -a, -b
        readonly float[] ParamB = new[] { 0.08f, -0.03f, 0f, 0f, 0f, 0f };

        readonly float Dt = (1f / 1f);

        readonly float Max = 2f;

        bool start = true;

        public Manager()
        {
            batch = new SpriteBatch(MyGame.Instance.GraphicsDevice);
            texture = new Texture2D(MyGame.Instance.GraphicsDevice, 1, 1);
            texture.SetData(new[] { Color.White });
            BufferLength = Length * Length;
            bufferA = CreateBuffer<float>();
            bufferB = CreateBuffer<float>();
            InitBuffer();
            current = 1;
            last = 0;
        }

        void InitBuffer()
        {
            var fileName = @"D:\projects\ReactiveDiffusion\img.png";
            var tex = new MonoGame.Utilities.Png.PngReader().Read(File.OpenRead(fileName), MyGame.Instance.GraphicsDevice);
            Color[] values = new Color[BufferLength];
            tex.GetData(0, new Rectangle(0, 0, Length, Length), values, 0, BufferLength);

            var rand = new Random();
            //for (int i = 0; i < 25; i++)
            //{
            //    bufferA[0][CellToIndex(rand.Next(Length), rand.Next(Length))] = (float)rand.NextDouble() * 10f * 0 + 1;
            //    bufferB[0][CellToIndex(rand.Next(Length), rand.Next(Length))] = (float)rand.NextDouble() * 10f * 0 + 1;
            //}
            var m = .1f;
            

            for (int i = 0; i < BufferLength; i++)
            {
                //bufferA[0][i] = values[i].R > 64 ? values[i].R / 512f : 0;
                //bufferB[0][i] = values[i].B > 64 ? values[i].B / 512f : 0;
            }
            bufferA[0] = DetectEdge(0, values);
            bufferB[0] = DetectEdge(2, values);
            for (int i = 0; i < BufferLength; i++)
            {
                bufferA[0][i] += (float)rand.NextDouble() * m;
                bufferB[0][i] += (float)rand.NextDouble() * m;
            }
        }

        float[] DetectEdge(int channel, Color[] data)
        {
            var buf = new float[BufferLength];
            for (int i = 0; i < BufferLength; i++)
            {
                var x = i % Length;
                var y = i / Length;
                var s = data[i].ToVector4() * 4 - data[CellToIndex(x - 1, y)].ToVector4()
                    - data[CellToIndex(x + 1, y)].ToVector4() - data[CellToIndex(x, y - 1)].ToVector4() - data[CellToIndex(x, y + 1)].ToVector4();
                buf[i] = Math.Abs(channel == 0 ? s.X : s.Z);
                if(Math.Abs(buf[i]) < .2f)
                {
                    buf[i] = 0;
                }
            }
            return buf;
        }

        T[][] CreateBuffer<T>()
        {
            var len = BufferLength;
            var buf = new T[2][];
            buf[0] = new T[len];
            buf[1] = new T[len];
            return buf;
        }

        public void Update()
        {
            if (!start)
            {
                if (Keyboard.GetState().IsKeyDown(Keys.Space))
                {
                    start = true;
                    
                }
                return;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Z))
            {
                swap = true;
            }
            Parallel.For(0, BufferLength, UpdateCell);
            Input();
            last = current;
            current = 1 - current;
        }

        void Input()
        {
            if (!MyGame.Instance.IsActive)
            {
                return;
            }
            float Amount = 100f;
            var state = Mouse.GetState();
            int i = CellToIndex(state.X / UnitSize, state.Y / UnitSize);
            if (state.LeftButton == ButtonState.Pressed)
            {
                
                bufferA[current][i] += Amount * Dt;
            }
            if (state.RightButton == ButtonState.Pressed)
            {
                bufferB[current][i] += Amount * Dt;
            }
        }

        bool swap = false;

        void UpdateCell(int i)
        {
            var a = bufferA[last][i];
            var b = bufferB[last][i];
            bufferA[current][i] = a + Dt * (Diffuse(bufferA[last], i, DiffuseA, swap ^ true) + React(a, b, ParamA));
            bufferB[current][i] = b + Dt * (Diffuse(bufferB[last], i, DiffuseB, swap ^ true) + React(a, b, ParamB));

        }


        float React(float a, float b, float[] param)
        {
            return a * param[0] + b * param[1] + param[2];
       //     return MathHelper.Clamp(a * param[0] + b * param[1] + param[2], 0, param[3]) + a * param[4] + b * param[5];
        }

        float Diffuse(float[] buffer, int i, float diffuse, bool dir)
        {
            int x = i % Length;
            int y = i / Length;

            if (dir)
            {
                var s = -.5f;
                return buffer[i] * (diffuse * -4)
                    + diffuse * (1 + s) * (buffer[CellToIndex(x - 1, y)] + buffer[CellToIndex(x + 1, y)])
                    + diffuse * (1 - s) * (buffer[CellToIndex(x, y - 1)] + buffer[CellToIndex(x, y + 1)]);
            }
            else
            {
                return buffer[i] * (diffuse * -4)
                + diffuse * (buffer[CellToIndex(x - 1, y)] + buffer[CellToIndex(x + 1, y)]
                + buffer[CellToIndex(x, y - 1)] + buffer[CellToIndex(x, y + 1)]);
            }
        }

        int CellToIndex(int x, int y)
        {
            if (x < 0) x += Length;
            if (x >= Length) x -= Length;
            if (y < 0) y += Length;
            if (y >= Length) y -= Length;
            return x + y * Length;
        }


        Color GetCellColor(int i)
        {

            var a = bufferA[current][i];
            var b = bufferB[current][i];
            var ca = a > 0 ? Math.Log(a, Max) : 0;
            var cb = b > 0 ? Math.Log(b, Max) : 0;
            var cd = b < 0 ? -b : 0;
            return new Color((float)a, 0, (float)b);
        }

        public void Draw()
        {
            batch.Begin();
            for (int i = 0; i < BufferLength; i++)
            {
                int x = i % Length;
                int y = i / Length;
                batch.Draw(texture, new Rectangle(x * UnitSize, y * UnitSize, UnitSize, UnitSize), GetCellColor(i));
            }
            batch.End();
        }

    }
}
