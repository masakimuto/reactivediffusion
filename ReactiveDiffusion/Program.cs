﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace ReactiveDiffusion
{
    class Program
    {
        public static void Main(string[] args)
        {
            using (var game = new MyGame())
            {
                game.Run();
            }
        }
    }

    public class MyGame : Game
    {
        public static MyGame Instance { get; private set; }

        GraphicsDeviceManager graphicsDeviceManager;

        Manager manager;

        public MyGame()
        {
            graphicsDeviceManager = new GraphicsDeviceManager(this)
            {
                IsFullScreen = false,
                PreferredBackBufferWidth = 128*8,
                PreferredBackBufferHeight = 128*8,
                SynchronizeWithVerticalRetrace = false,
            };
            Window.Position = new Point(200, 200);
            Window.IsBorderless = true;
            IsFixedTimeStep = false;
            Instance = this;
            IsMouseVisible = true;
        }

        protected override void LoadContent()
        {
            base.LoadContent();
            manager = new Manager();
        }

        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            var input = Keyboard.GetState();
            if (input.IsKeyDown(Keys.Escape))
            {
                manager = new Manager();
            }

            manager.Update();
        }

        protected override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
            GraphicsDevice.Clear(Color.CornflowerBlue);
            manager.Draw();
        }

    }
}
