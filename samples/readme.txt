These are RDIEC(Genetic Reaction) sample genes and output textures.
Drag-drop a gene files(.gen file) to RDIEC window to load it.

config.txt is a sample config file. Pass the config file to RDIEC as command line arguments.

*Texture initial state is generated randomly and textures develop by time, RDIEC does'nt always generate exactly same texture from same gene.

